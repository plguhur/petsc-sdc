
#include <petsc/private/tsimpl.h> 
#include <petsctime.h>

//#define MAX_INJECTION    100000000.0
static PetscClassId      TSINJECT_CLASSID;
static PetscFunctionList TSInjectorList;
PetscBool                TSInjectRegisterAllCalled;
PetscReal                bitsPos[64];
static PetscReal         beta = 2*3.14159265359;

#undef __FUNCT__
#define __FUNCT__ "TSInjectRandInt"
/*@C
   TSInjectRandInt -  generate a random int

   Level: advanced

.keywords: TSInject

@*/
PetscErrorCode TSInjectRandInt(TSInject inject, PetscInt *r)  // RAND_MAX assumed to be 32767
{
    PetscFunctionBegin;
    inject->next = inject->next * 1103515245 + 12345;
    *r = PetscAbs((PetscInt)(inject->next/65536) % 32768);
    PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSInjectRandReal"
/*@C
   TSInjectRandReal -  generate a random real

   Level: advanced

.keywords: TSInject

@*/
PetscErrorCode TSInjectRandReal(TSInject inject, PetscReal *r)  {
  PetscInt i;
  
  PetscFunctionBegin;
  TSInjectRandInt(inject, &i);
  *r = ((PetscReal)i)/32767.0;
  PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "TSInjectRandSeed"
/*@C
   TSInjectRandSeed -  generate a random int

   Level: advanced

.keywords: TSInject

@*/
PetscErrorCode TSInjectRandSeed(TSInject inject, PetscInt seed)
{
  PetscFunctionBegin;
  inject->next = seed;
  inject->rNext = *(PetscReal*)&seed/32767.0;
  PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "TSInjectNormalReal"
/*
  Marsaglia, George, and Wai Wan Tsang. "The ziggurat method for generating random variables." Journal of statistical software 5.8 (2000): 1-7.
*/
PetscErrorCode TSInjectNormalReal(TSInject inject, PetscReal *r) {
  PetscReal u1, u2;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = TSInjectRandReal(inject, &u1);CHKERRQ(ierr);
  ierr = TSInjectRandReal(inject, &u2);CHKERRQ(ierr);
  *r = PetscSqrtReal(-2*PetscLogReal(u1))*cos(beta*u2);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSInjection"
/*@C
   TSInjection -  inject an SDC

   Level: advanced

.keywords: TSInject

@*/
PetscErrorCode  TSInjection(TSInject inject, Vec X)
{
  PetscReal       r,err, r2;
  Vec             Y;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (!inject || (inject->lastCorrupted && inject->once) || inject->no) { 
    PetscFunctionReturn(0);
  }
  ierr  = TSInjectRandReal( inject, &r);CHKERRQ(ierr);
  r2 = PetscFloorReal(r);
  r2 = r - r2;
  if(r < inject->proba){ 
    ierr      = VecDuplicate(X, &Y);CHKERRQ(ierr);  
    ierr      = VecCopy(X, Y);CHKERRQ(ierr);
    ierr = (*inject->injector)(inject, Y, &err);CHKERRQ(ierr);  
    if(inject->mode == TSINJECT_SGNFCNT && err < inject->threshold) { 
      PetscInfo(inject, "Too small injection. Cancel it\n");
      PetscFunctionReturn(0); 
    }
    ierr      = VecCopy(Y,X);CHKERRQ(ierr);
    ierr      = VecDestroy(&Y);CHKERRQ(ierr);
    inject->corrupted = PETSC_TRUE;
  }
  CHKMEMQ;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSInject_BitFlip"
PetscErrorCode TSInject_BitFlip(TSInject inject, Vec X, PetscInt pos, PetscInt cmp, PetscReal *err) {
  PetscReal           *values;
  PetscReal  t, x;
  PetscInt      size;
  uint64_t i;
  uint64_t one = 1;//it expects that petscint and petscreal have the same number of bits
  PetscErrorCode    ierr;
  
  PetscFunctionBegin;  CHKMEMQ;
  ierr      = VecGetLocalSize(X, &size);CHKERRQ(ierr);
  ierr = VecGetArray (X, &values);CHKERRQ(ierr);
  PetscInfo3(inject,"Injection on cmp %i/%i on bit %i\n",cmp,size,pos);CHKERRQ(ierr);
//  ierr = VecGetValues(X,1, &cmp,&t);CHKERRQ(ierr);
  t = values[cmp];
  i   = *(uint64_t*)&t; 
  i ^= (one << pos);
  x = *(PetscReal*)&i;
//  if(PetscAbs(t) > MAX_INJECTION){ t = MAX_INJECTION;}
  *err = PetscAbs(t - x);
//  ierr = VecSetValues(X,1, &cmp,&x, INSERT_VALUES);CHKERRQ(ierr); CHKMEMQ;
  values[cmp] = x;
  PetscInfo3(inject, "The value was %g, now it is %g (err: %g)\n", (double)t, (double)x, (double)*err);CHKERRQ(ierr);
  ierr = VecRestoreArray(X, &values);CHKERRQ(ierr);  CHKMEMQ;
  VecAssemblyBegin(X);
  VecAssemblyEnd(X);
  PetscFunctionReturn(0);
}

/*@C
   TSInject_Single -  one random position 

   Level: advanced

.keywords: TSInject

@*/
#undef __FUNCT__
#define __FUNCT__ "TSInject_Single"
PetscErrorCode TSInject_Single(TSInject inject, Vec Y, PetscReal * err) {
  PetscInt          size, cmp, pos, r;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr      = VecGetLocalSize(Y, &size);CHKERRQ(ierr);
  ierr      = TSInjectRandInt( inject, &r);CHKERRQ(ierr);
  cmp       = r%size;  
  ierr      = TSInjectRandInt( inject, &r);CHKERRQ(ierr); 
  pos       = r%inject->Nbits;  
  ierr      = TSInject_BitFlip(inject, Y, pos, cmp, err);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*@C
   TSInject_Raz -  retour a zero 

   Level: advanced

.keywords: TSInject

@*/
#undef __FUNCT__
#define __FUNCT__ "TSInject_Raz"
PetscErrorCode TSInject_Raz(TSInject inject, Vec Y, PetscReal * err) {
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = PetscInfo(inject,"Injection return to zero/n");CHKERRQ(ierr);
  ierr = VecZeroEntries(Y);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*@C
   TSInject_Multi -  multi bit flips

   Level: advanced

.keywords: TSInject

@*/
#undef __FUNCT__
#define __FUNCT__ "TSInject_Multi"
PetscErrorCode TSInject_Multi(TSInject inject, Vec Y, PetscReal * err) {
  PetscInt          size, Ninj, i, cmp, pos, r, t;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr      = VecGetLocalSize(Y, &size);CHKERRQ(ierr);
  ierr      = TSInjectRandInt( inject, &r);CHKERRQ(ierr);
  cmp       = r%size;
  ierr      = TSInjectRandInt( inject, &r);CHKERRQ(ierr);
  Ninj      = r%inject->Nbits;
  // select unique pos that should be bit flip
  for (i = Ninj - 1; i > 0; i--) {
      ierr         = TSInjectRandInt( inject, &r);CHKERRQ(ierr);
      pos          = r%inject->Nbits;
      t            = bitsPos[i];
      bitsPos[i]   = bitsPos[pos];
      bitsPos[pos] = t;
  }
  for(i = 0; i<Ninj; i++){
    pos       = bitsPos[i];
    ierr      = TSInject_BitFlip(inject, Y, pos, cmp, err);CHKERRQ(ierr); 
  }
  PetscFunctionReturn(0);
  
  
}

/*@C
   TSInject_Offset -  offset

   Level: advanced

.keywords: TSInject

@*/
#undef __FUNCT__
#define __FUNCT__ "TSInject_Offset"
PetscErrorCode TSInject_Offset(TSInject inject, Vec Y, PetscReal * err) {
  PetscInt          size, Ninj, i, cmp, pos, r, t;
  PetscErrorCode    ierr;
  PetscReal*        values;

  PetscFunctionBegin;
  ierr      = VecGetLocalSize(Y, &size);CHKERRQ(ierr);
  ierr      = TSInjectRandInt( inject, &r);CHKERRQ(ierr);
  cmp       = r%size;
  ierr      = TSInjectRandInt( inject, &r);CHKERRQ(ierr);
  Ninj      = r%inject->Nbits;
  // select unique pos that should be bit flip
  for (i = Ninj - 1; i > 0; i--) {
      ierr         = TSInjectRandInt( inject, &r);CHKERRQ(ierr);
      pos          = r%inject->Nbits;
      t            = bitsPos[i];
      bitsPos[i]   = bitsPos[pos];
      bitsPos[pos] = t;
  }
  ierr = VecGetArray (Y, &values);CHKERRQ(ierr);
  for(i = 0; i<1; i++){
    pos       = bitsPos[i];
    PetscInfo3(inject,"Injection on cmp %i/%i on bit %i\n",cmp,size,pos);CHKERRQ(ierr);
    values[cmp] += inject->threshold;
  }
  ierr = VecRestoreArray(Y, &values);CHKERRQ(ierr);  CHKMEMQ;
  VecAssemblyBegin(Y);
  VecAssemblyEnd(Y);
  PetscFunctionReturn(0);
}

/*@C
   TSInject_Scale

   Level: advanced

.keywords: TSInject

@*/
#undef __FUNCT__
#define __FUNCT__ "TSInject_Scale"
PetscErrorCode TSInject_Scale(TSInject inject, Vec Y, PetscReal * err) {
  PetscInt          cmp, r, size;
  PetscErrorCode    ierr;
  PetscReal         *values, scale;

  PetscFunctionBegin;
  ierr      = VecGetLocalSize(Y, &size);CHKERRQ(ierr);
  ierr      = TSInjectRandInt( inject, &r);CHKERRQ(ierr);
  cmp       = r%size;
  ierr      = TSInjectNormalReal( inject, &scale);CHKERRQ(ierr);
  ierr = VecGetArray (Y, &values);CHKERRQ(ierr);
  PetscInfo3(inject,"Injection on cmp %i/%i with a scale %g\n",cmp,size,scale);CHKERRQ(ierr);
  values[cmp] *= scale;
  ierr = VecRestoreArray(Y, &values);CHKERRQ(ierr);  CHKMEMQ;
  VecAssemblyBegin(Y);
  VecAssemblyEnd(Y);
  PetscFunctionReturn(0);
}

/*@C
   TSInjectUpdate 

   Level: advanced

.keywords: TSInject

@*/
#undef __FUNCT__
#define __FUNCT__ "TSInjectUpdate"
PetscErrorCode TSInjectUpdate(TSInject inject, PetscBool detected) {
  PetscFunctionBegin;
  if (!inject) { 
    PetscFunctionReturn(0);
  }
  inject->lastCorrupted = inject->corrupted;
  inject->corrupted = PETSC_FALSE;
  if(inject->viewer) {
    inject->tp += detected && inject->corrupted;
    inject->fp += detected && !(inject->corrupted);
    inject->tn += !detected && !(inject->corrupted);
    inject->fn += !detected && inject->corrupted;
  }
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSInjectMonitor"
PetscErrorCode TSInjectMonitor(TSInject inject, PetscInt steps, PetscReal ptime,  PetscBool detected) {
  PetscErrorCode    ierr;
  
  PetscFunctionBegin;
  if(inject && inject->viewer){
    ierr = PetscViewerASCIIPrintf(inject->viewer,"%i, %g, %i, %i\n",steps,ptime,(detected?1:0),(inject->corrupted?1:0));CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
  
  
}

/*@C
   TSInjectDestroy

   Level: advanced

.keywords: TSInject

@*/
#undef __FUNCT__
#define __FUNCT__ "TSInjectDestroy"
PetscErrorCode TSInjectDestroy(TSInject* inject) {
  PetscViewer viewer;
  PetscErrorCode ierr;
  PetscBool     flg;
  
  PetscFunctionBegin;
  // destroy log-inject
  ierr = PetscStrcmp((*inject)->log_inject, "", &flg); CHKERRQ(ierr);
  if(!flg) {
      ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD,(*inject)->log_inject,&viewer);CHKERRQ(ierr);
      ierr = (*(*inject)->ops->view)(*inject, viewer);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  }
  // destroy log-lte
  if((*inject)->log_lte) {
      ierr = PetscViewerASCIIPrintf((*inject)->log_lte,"\n");CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&(*inject)->log_lte);CHKERRQ(ierr);
  }
  ierr = PetscViewerDestroy(&(*inject)->viewer);CHKERRQ(ierr);
  ierr = PetscRandomDestroy(&(*inject)->ran);CHKERRQ(ierr);
  ierr = PetscFunctionListDestroy(&TSInjectorList);CHKERRQ(ierr);
  ierr = PetscHeaderDestroy(inject);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSInjectLogLEE"
PETSC_EXTERN PetscErrorCode  TSInjectLogLEE(TSInject inject, PetscInt stepno, PetscReal h, PetscReal enorm1, PetscReal enorm2) {
  PetscBool      iascii;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if(!inject->log_lte) {
     PetscFunctionReturn(0);
  }
  ierr = PetscObjectTypeCompare((PetscObject)inject->log_lte,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  if (iascii) {
    ierr = PetscViewerASCIIPrintf(inject->log_lte,"\n%i, %g, %.5f, %.5f", stepno, h, enorm1, enorm2);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
  
}


#undef __FUNCT__
#define __FUNCT__ "TSInjectLogNormNew"
PETSC_EXTERN PetscErrorCode  TSInjectLogNormNew(TSInject inject, PetscInt stepno, PetscReal h) {
  PetscBool      iascii;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if(!inject->log_lte) {
     PetscFunctionReturn(0);
  }
  ierr = PetscObjectTypeCompare((PetscObject)inject->log_lte,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  if (iascii) {
    ierr = PetscViewerASCIIPrintf(inject->log_lte,"\n%i, %g", stepno, h);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
  
}


#undef __FUNCT__
#define __FUNCT__ "TSInjectLogNorm"
PETSC_EXTERN PetscErrorCode  TSInjectLogNorm(TSInject inject, PetscReal enorm1) {
  PetscBool      iascii;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if(!inject->log_lte) {
     PetscFunctionReturn(0);
  }
  ierr = PetscObjectTypeCompare((PetscObject)inject->log_lte,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  if (iascii) {
    ierr = PetscViewerASCIIPrintf(inject->log_lte,", %.5f", enorm1);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
  
}




#undef __FUNCT__
#define __FUNCT__ "TSInjectView"
/*@C
  TSInjectView - 

  Not Collective

  Level: advanced

.keywords: TSInject

@*/
PETSC_EXTERN PetscErrorCode  TSInjectView(TSInject in, PetscViewer viewer)
{
  PetscBool      iascii;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  if (iascii) {
    //ierr = PetscViewerASCIIPrintf(viewer,"  Inject type %s\n",in->mode);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer,"  FP %i FN %i TP %i TN %i\n",in->fp, in->fn, in->tp, in->tn);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}


/*@C
   TSInjectSetFromOptions

   Level: advanced

.keywords: TSInject

@*/
#undef __FUNCT__
#define __FUNCT__ "TSInjectSetFromOptions"
PetscErrorCode TSInjectSetFromOptions(PetscOptionItems *PetscOptionsObject, TSInject inject){
  PetscBool      flg;
  char           filename[256];
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscOptionsHead(PetscOptionsObject,"Injection of SDCs:: options");CHKERRQ(ierr);
  PetscOptionsEnum("-ts_inject_mode","Type of injection","",InjectTypes,(PetscEnum)inject->mode,(PetscEnum*)&inject->mode,NULL);
  if(inject->mode == TSINJECT_SGNFCNT) {  inject->injector = TSInject_Single;}
  else if(inject->mode == TSINJECT_SINGLE) {  inject->injector = TSInject_Single;}
  else if(inject->mode == TSINJECT_MULTI) {  inject->injector = TSInject_Multi;}
  else if(inject->mode == TSINJECT_RAZ) {  inject->injector = TSInject_Raz;}
  else if(inject->mode == TSINJECT_OFFSET) {  inject->injector = TSInject_Offset;}
  else if(inject->mode == TSINJECT_SCALE) {  inject->injector = TSInject_Scale;}
  ierr = PetscOptionsInt("-ts_inject_cntr_max","Maximum of the counter","",inject->cntr_max,&inject->cntr_max,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-ts_inject_proba","Probability of injecting an SDC","",inject->proba,&inject->proba,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-ts_inject_threshold","Threshold for significant SDCs","",inject->threshold,&inject->threshold,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-ts_inject_once","No injection when rejected step","",inject->once,&inject->once,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsString("-ts_inject_monitor","Monitoring injections","","",filename,256, &flg);CHKERRQ(ierr);
  if(flg) { 
    ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD,filename,&inject->viewer);CHKERRQ(ierr);
  }    
  ierr = PetscOptionsString("-ts_inject_log","Log Nfp, and so on","","", inject->log_inject,256,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsString("-ts_inject_log_lte","Log weigthed norm","","",filename,256, &flg);CHKERRQ(ierr);
  if(flg) { 
    ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD,filename,&inject->log_lte);CHKERRQ(ierr);
  }    
  ierr = PetscOptionsBool("-ts_inject_rand","Random seed","", PETSC_FALSE,&flg,NULL);CHKERRQ(ierr);
  if(flg) {
    PetscLogDouble t;
    ierr = PetscGetCPUTime(&t);CHKERRQ(ierr);
    ierr = TSInjectRandSeed(inject, (PetscInt)t);CHKERRQ(ierr);
  }
	ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "TSInjectCreate"
PetscErrorCode  TSInjectCreate(MPI_Comm comm,TSInject *ininject){
  PetscErrorCode ierr;
  size_t         t;
  PetscInt       i;
  PetscReal       r;
  TSInject       inject;

  PetscFunctionBegin;
  PetscValidPointer(ininject,1);
  *ininject = NULL;
  ierr                       = PetscClassIdRegister("TSInject",&TSINJECT_CLASSID);CHKERRQ(ierr);
  ierr = PetscHeaderCreate(inject,TSINJECT_CLASSID,"TSInject","Injection of SDC","TS",comm,TSInjectDestroy, TSInjectView);CHKERRQ(ierr);
  inject->tp = 0;
  inject->fp = 0;
  inject->fn = 0;
  inject->tn = 0;
  inject->cntr=0;
  inject->cntr_max=0;
  inject->mode=TSINJECT_SINGLE;
  inject->corrupted = PETSC_FALSE;
  inject->lastCorrupted = PETSC_FALSE;
  inject->viewer    = NULL;
  inject->proba= 0.1;
  inject->threshold= 1e-6;
  inject->once    = PETSC_FALSE;
  ierr      = PetscDataTypeGetSize(PETSC_DOUBLE, &t); CHKERRQ(ierr); //assuming petscreal
  
  ierr = PetscRandomCreate( comm,&inject->ran);CHKERRQ(ierr);
  ierr = PetscRandomSetType(inject->ran, PETSCRAND48);CHKERRQ(ierr);
  ierr = PetscRandomSeed(inject->ran);CHKERRQ(ierr);
  ierr = PetscInfo(inject,"Injection\n");
  ierr = PetscRandomGetValue(inject->ran, &r);CHKERRQ(ierr);
  ierr = TSInjectRandSeed(inject, 1);CHKERRQ(ierr);
  inject->injector =  TSInject_Single;
  inject->ops->setfromoptions 		 = TSInjectSetFromOptions;
  inject->ops->destroy        		 = TSInjectDestroy;
  inject->ops->view           		 = TSInjectView;
  inject->Nbits     = 8*(PetscInt)t;
  inject->no = PETSC_FALSE;
  // ini bitsPos
  for(i = 0; i < inject->Nbits; i++) {
    bitsPos[i] = i;
  }
  *ininject = inject;
  PetscFunctionReturn(0);
}


