
#include <petsc/private/tsimpl.h> /*I  "petscts.h" I*/





static PetscFunctionList TSResilList;
static PetscObjectList   TSResilListMonitors;
static PetscBool         TSResilPackageInitialized,TSResilRegisterAllCalled;
static PetscClassId      TSRESIL_CLASSID; 
static PetscClassId      TSRESILSURROGATE_CLASSID;  
static PetscClassId      TSRESILCHECK_CLASSID; 
//static PetscClassId      TSRESILMONITOR_CLASSID; 



PETSC_EXTERN PetscErrorCode TSResilCreate_Estimate(MPI_Comm comm,TSResil);
PETSC_EXTERN PetscErrorCode TSResilCreate_Delta(MPI_Comm, TSResil);
// PETSC_EXTERN PetscErrorCode TSResilSurrogateReplication_Create(TSResil);


PETSC_EXTERN PetscErrorCode TSResilCreate_Percentile(MPI_Comm, TSResil);
PETSC_EXTERN PetscErrorCode TSResilCreate_Online(MPI_Comm, TSResil);
PETSC_EXTERN PetscErrorCode TSResilCreate_Feedback(MPI_Comm comm,TSResil);
PETSC_EXTERN PetscErrorCode TSResilCreate_ImpactDriven(MPI_Comm comm,TSResil);
// PETSC_EXTERN PetscErrorCode TSResilCheckEpsilon_Create(TSResil);



#undef __FUNCT__
#define __FUNCT__ "TSResilUpdate"
PetscErrorCode TSResilUpdate(TSResil resil, TS ts, Vec old_sol){
  TSResilSurrogate    s;
  TSResilCheck        c;
  PetscErrorCode      ierr;

  PetscFunctionBegin;
  if (!resil) { 
    PetscFunctionReturn(0);
  }
  s = resil->surrogate;
  c = resil->check;
  resil->Nnegative += 1;
	if (s->ops->update) {ierr = (*s->ops->update)(resil,ts, old_sol);CHKERRQ(ierr);}
	if (c->ops->update) {ierr = (*c->ops->update)(resil,ts, old_sol);CHKERRQ(ierr);}
	resil->FP = PETSC_FALSE;
//  ierr = TSGetSolution(ts,&Y);CHKERRQ(ierr);
//  ierr = VecCopy(ts->vec_sol,resil->lastSol);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


  
#undef __FUNCT__
#define __FUNCT__ "TSResilSetSolution"
PetscErrorCode  TSResilSetSolution(TSResil r,Vec u)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  PetscValidHeaderSpecific(r,TSRESIL_CLASSID,1);
  PetscValidHeaderSpecific(u,VEC_CLASSID,2);
  ierr = PetscObjectReference((PetscObject)u);CHKERRQ(ierr);
  ierr = VecDuplicate(u,&r->lastSol);CHKERRQ(ierr);
  ierr = VecCopy(u,r->lastSol);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}




#undef __FUNCT__
#define __FUNCT__ "TSResilCheckStep"
/*@
   TSResilCheckStep - checks whether to accept a step

   Collective

   Input Arguments:
+  resil - resilive controller context
-  ts - time stepper

   Output Arguments:
.  accept - PETSC_TRUE to accept the step, PETSC_FALSE to reject

   Level: developer

.seealso:
@*/
PetscErrorCode TSResilCheckStep(TSResil resil,TS ts, Vec old_sol, PetscBool *accept){
  TSResilSurrogate    s;
  TSResilCheck        c;
  PetscErrorCode      ierr;
  PetscBool           equal;
	
  PetscFunctionBegin;
  if(!resil) {
    PetscFunctionReturn(0);
  }
  PetscValidHeaderSpecific(resil,TSRESIL_CLASSID,1);  
  *accept = PETSC_TRUE;
  if(resil->alwaysAccept) {PetscFunctionReturn(0); }
  ierr = VecEqual(ts->vec_sol, resil->lastSol, &equal);CHKERRQ(ierr);
	if(!resil->lastAccept && equal) {
    resil->FP = PETSC_TRUE;
		resil->Nfp += 1;
    ierr    = PetscInfo1(resil,"Made a false positive. Nfp = %i, accepting step.\n",resil->Nfp);CHKERRQ(ierr);
    PetscFunctionReturn(0);
	}
  else {
		resil->FP = PETSC_FALSE;
    ierr = VecCopy(ts->vec_sol, resil->lastSol);CHKERRQ(ierr); //useful when a step is rejecting, to know whether or not it is normal
  }
	
  s = resil->surrogate;
  c = resil->check;
  if(s->ops->compute) {ierr = (*s->ops->compute)(resil,ts,old_sol);CHKERRQ(ierr);}
  if(c->ops->check) {ierr = (*c->ops->check)(resil,ts,accept);CHKERRQ(ierr);  }
  resil->lastAccept = *accept;
  
  PetscFunctionReturn(0);
}


/* -----------------------------------------------------------------*/
/* --------------------   RESIL MONITOR ----------------------------*/

#undef __FUNCT__
#define __FUNCT__ "TSResilAddASCIIMonitor"
PETSC_EXTERN PetscErrorCode TSResilAddASCIIMonitor(TSResil resil, TSResilObjMonitor rmon)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscViewerASCIIOpen(PetscObjectComm((PetscObject)resil),rmon->name,&rmon->viewer);CHKERRQ(ierr);
  ierr = PetscObjectListAdd(&TSResilListMonitors,rmon->name,(PetscObject)rmon);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilMonitor"
PETSC_EXTERN PetscErrorCode TSResilMonitor(TS ts, PetscInt steps, PetscReal ptime,  Vec sol) {
  PetscErrorCode    ierr;
  TSResil           resil = ts->resil;
  PetscObjectList   listMon = TSResilListMonitors;
  TSResilObjMonitor rmon;
  
  PetscFunctionBegin;
  if (resil) { 
    PetscValidHeaderSpecific(resil,TSRESIL_CLASSID,1);
    ierr = VecLockPush(sol);CHKERRQ(ierr);
    while(listMon) {
      PetscObject po;
      ierr = PetscObjectListGet(listMon,&po);CHKERRQ(ierr);
      rmon = (TSResilObjMonitor)po;
      ierr = (*rmon->monitor)(resil,steps,ptime,sol,rmon->viewer);CHKERRQ(ierr);
      ierr = PetscObjectListNext(&listMon);CHKERRQ(ierr);
    }
    ierr = VecLockPop(sol);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilDestroyMonitors"
PETSC_EXTERN PetscErrorCode TSResilDestroyMonitors() {
  PetscErrorCode  ierr;
  PetscObjectList listMon = TSResilListMonitors;
  TSResilObjMonitor rmon;
  
  PetscFunctionBegin;
  while(listMon) {
    PetscObject po;
    ierr = PetscObjectListGet(listMon,&po);CHKERRQ(ierr);
    rmon = (TSResilObjMonitor)po;
    ierr = PetscViewerDestroy(&(rmon->viewer));CHKERRQ(ierr);
    ierr = PetscObjectListNext(&listMon);CHKERRQ(ierr);
  }
  ierr = PetscObjectListDestroy(&TSResilListMonitors);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


/*----------------------------------------------------------------------------*/

#undef __FUNCT__
#define __FUNCT__ "TSResilRegister"
/*@C
   TSResilRegister -  adds a TSResil implementation

   Not Collective 

   Input Parameters:
+  name_scheme - name of user-defined module scheme
-  routine_create - routine to create method context

   Notes:
   TSResilRegister() may be called multiple times to add several user-defined families.

   Sample usage:
.vb
   TSResilRegister("my_scheme",MySchemeCreate);
.ve

   Then, your scheme can be chosen with the procedural interface via
$     TSResil[Module]_SetType(ts,"my_scheme")
   or at runtime via the option
$     -ts_resil_[module]_type my_scheme

   Level: advanced

.keywords: TSResil, register

@*/
PETSC_EXTERN PetscErrorCode  TSResilRegister(const char sname[],PetscErrorCode (*function)(MPI_Comm, TSResil))
{
    PetscErrorCode ierr;

    PetscFunctionBegin;
	  ierr = PetscFunctionListAdd(&TSResilList,sname,function);CHKERRQ(ierr);
    PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilRegisterAll"
/*@C
  TSResilRegisterAll - Registers all of the resilience schemes in TSResil

  Not Collective

  Level: advanced

.keywords: TSResil, register, all

.seealso: TSResilRegisterDestroy()
@*/
PETSC_EXTERN PetscErrorCode  TSResilRegisterAll(void)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (TSResilRegisterAllCalled) PetscFunctionReturn(0);
  TSResilRegisterAllCalled = PETSC_TRUE;
  // ierr = TSResilRegister(TSRESILPOS, 			TSResilSurrogatePosition_Create);CHKERRQ(ierr);

  ierr = TSResilRegister(TSRESILEST, 			TSResilCreate_Estimate);CHKERRQ(ierr);
	ierr = TSResilRegister(TSRESILDELTA, 		TSResilCreate_Delta);CHKERRQ(ierr);
  // ierr = TSResilRegister(TSRESILREPLIC,   TSResilSurrogateReplication_Create);CHKERRQ(ierr);
  ierr =  TSResilRegister(TSRESILONLINE, 			TSResilCreate_Online);CHKERRQ(ierr);
  ierr = TSResilRegister(TSRESILPER, 			TSResilCreate_Percentile);CHKERRQ(ierr);
	ierr = TSResilRegister(TSRESILFB, 			TSResilCreate_Feedback);CHKERRQ(ierr);
	ierr = TSResilRegister(TSRESILID,  			TSResilCreate_ImpactDriven);CHKERRQ(ierr);
	//   ierr = TSResilRegister(TSRESILEPS, 			TSResilCheckEpsilon_Create);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilFinalizePackage"
/*@C
  TSFinalizePackage - This function destroys everything in the TSResil package. It is
  called from PetscFinalize().

  Level: developer

.keywords: Petsc, destroy, package
.seealso: PetscFinalize()
@*/
PETSC_EXTERN PetscErrorCode  TSResilFinalizePackage(void)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscFunctionListDestroy(&TSResilList);CHKERRQ(ierr);
  TSResilPackageInitialized = PETSC_FALSE;
  TSResilRegisterAllCalled  = PETSC_FALSE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilInitializePackage"
/*@C
  TSResilInitializePackage - This function initializes everything in the TSResil package.

  Level: developer

.keywords: TSResil, initialize, package
.seealso: PetscInitialize()
@*/
PETSC_EXTERN PetscErrorCode  TSResilInitializePackage(void)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (TSResilPackageInitialized) PetscFunctionReturn(0);
  TSResilPackageInitialized = PETSC_TRUE;
  ierr = PetscClassIdRegister("TSResil",&TSRESIL_CLASSID);CHKERRQ(ierr);
  ierr = TSResilRegisterAll();CHKERRQ(ierr);
  ierr = PetscRegisterFinalize(TSResilFinalizePackage);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilLoad"
/*@C ?? TO DO 
  TSResilLoad - Loads a TSResil that has been stored in binary  with TSResilView().

  Collective on PetscViewer

  Input Parameters:
+ newdm - the newly loaded TSResil, this needs to have been created with TSResilCreate() or
           some related function before a call to TSResilLoad().
- viewer - binary file viewer, obtained from PetscViewerBinaryOpen() or
           HDF5 file viewer, obtained from PetscViewerHDF5Open()

   Level: intermediate

  Notes:
   The type is determined by the data in the file, any type set into the TSResil before this call is ignored.

  Notes for advanced users:
  Most users should not need to know the details of the binary storage
  format, since TSResilLoad() and TSResilView() completely hide these details.
  But for anyone who's interested, the standard binary matrix storage
  format is
.vb
     has not yet been determined
.ve

.seealso: PetscViewerBinaryOpen(), TSResilView(), MatLoad(), VecLoad()

PetscErrorCode  TSResilLoad(TSResil resil,PetscViewer viewer)
{
  PetscErrorCode ierr;
  PetscBool      isbinary;
  char           type[256];

  PetscFunctionBegin;
  PetscValidHeaderSpecific(resil,TSRESIL_CLASSID,1);
  PetscValidHeaderSpecific(viewer,PETSC_VIEWER_CLASSID,2);
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERBINARY,&isbinary);CHKERRQ(ierr);
  if (!isbinary) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Invalid viewer; open viewer with PetscViewerBinaryOpen()");

  ierr = PetscViewerBinaryRead(viewer,type,256,NULL,PETSC_CHAR);CHKERRQ(ierr);
  ierr = TSResil(resil, type);CHKERRQ(ierr);
  if (resil->ops->load) {
    ierr = (*resil->ops->load)(resil,viewer);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}
*/
#undef __FUNCT__
#define __FUNCT__ "TSResilView"
PETSC_EXTERN PetscErrorCode  TSResilView(TSResil resil,PetscViewer viewer)
{
  PetscErrorCode ierr;
  TSResilSurrogate    s = resil->surrogate;
  TSResilCheck        c = resil->check;
  PetscBool           iascii,isbinary;

  PetscFunctionBegin;
  PetscValidHeaderSpecific(resil,TSRESIL_CLASSID,1);
  if (!viewer) {ierr = PetscViewerASCIIGetStdout(PetscObjectComm((PetscObject)resil),&viewer);CHKERRQ(ierr);}
  PetscValidHeaderSpecific(viewer,PETSC_VIEWER_CLASSID,2);
  PetscCheckSameComm(resil,1,viewer,2);
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERBINARY,&isbinary);CHKERRQ(ierr);
  if (iascii) {
    ierr = PetscObjectPrintClassNamePrefixType((PetscObject)resil,viewer);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(viewer);CHKERRQ(ierr);
  	if (s->ops->view) {ierr = (*s->ops->view)(resil,viewer);CHKERRQ(ierr);}
  	if (c->ops->view) {ierr = (*c->ops->view)(resil,viewer);CHKERRQ(ierr);}
    ierr = PetscViewerASCIIPopTab(viewer);CHKERRQ(ierr);
  } else if (isbinary) {
    char type[256];

    /* need to save FILE_CLASS_ID for resil class */
    ierr = PetscStrncpy(type,((PetscObject)resil)->type_name,256);CHKERRQ(ierr);
    ierr = PetscViewerBinaryWrite(viewer,type,256,PETSC_CHAR,PETSC_FALSE);CHKERRQ(ierr);
  } 
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilReset"
/*@
   TSResilReset - Resets a TSResil context.

   Collective on TS

   Input Parameter:
.  resil - the TSResil context obtained from TSResilCreate()

   Level: developer

.seealso: TSResilCreate(), TSResilDestroy()
@*/
PETSC_EXTERN PetscErrorCode  TSResilReset(TSResil resil)
{
  PetscErrorCode      ierr;
  TSResilSurrogate    s = resil->surrogate;
  TSResilCheck        c = resil->check;

  PetscFunctionBegin;
  PetscValidHeaderSpecific(resil,TSRESIL_CLASSID,1);
  if (c->ops->reset) {ierr = (*c->ops->reset)(resil);CHKERRQ(ierr);}
  if (s->ops->reset) {ierr = (*s->ops->reset)(resil);CHKERRQ(ierr);}
  ierr = VecDestroy(&resil->lastSol);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilDestroy"
PETSC_EXTERN PetscErrorCode  TSResilDestroy(TSResil *resil)
{
  PetscErrorCode ierr;
  TSResilSurrogate    s = (*resil)->surrogate;
  TSResilCheck        c = (*resil)->check;
  
  PetscFunctionBegin;
  if (!*resil) PetscFunctionReturn(0);
  PetscValidHeaderSpecific(*resil,TSRESIL_CLASSID,1);
  if (--((PetscObject)(*resil))->refct > 0) {*resil = NULL; PetscFunctionReturn(0);}

  ierr = TSResilReset(*resil);CHKERRQ(ierr);

  if (s->ops->destroy) {ierr = (*s->ops->destroy)(*resil);CHKERRQ(ierr);}
  if (c->ops->destroy) {ierr = (*c->ops->destroy)(*resil);CHKERRQ(ierr);}
  ierr = PetscHeaderDestroy(&s);CHKERRQ(ierr);
  ierr = PetscHeaderDestroy(&c);CHKERRQ(ierr);
  ierr = PetscHeaderDestroy(resil);CHKERRQ(ierr);
  ierr = TSResilDestroyMonitors();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilSetFromOptions"
/*
   TSResilSetFromOptions - Sets various TSResil parameters from user options.

   Collective on TSResil

   Input Parameter:
.  resil - the TSResil context

   Options Database Keys:
.  -ts_resil_type <type> - basic

   Level: advanced

   Notes:
   This function is automatically called by TSSetFromOptions()

.keywords: TS, TSGetResil(), TSResilSetType()

.seealso: TSGetType()
*/
PetscErrorCode  TSResilSetFromOptions(PetscOptionItems *PetscOptionsObject,TSResil resil) {
  PetscErrorCode      ierr;
  TSResilSurrogate    s = resil->surrogate;
  TSResilCheck        c = resil->check;
  PetscBool           flg;

  PetscFunctionBegin;
  PetscValidHeaderSpecific(resil,TSRESIL_CLASSID,1);
  /* This should use PetscOptionsBegin() if/when this becomes an object used outside of TS, but currently this
  * function can only be called from inside TSSetFromOptions_GL()  */
  ierr = PetscOptionsHead(PetscOptionsObject,"TS Resilience options");CHKERRQ(ierr);
	
	
	// check module
  ierr = PetscOptionsFList("-ts_resil_check","Check method to use for Resilience","",TSResilList,
                          resil->chk_name, resil->chk_name,sizeof(resil->chk_name),&flg);CHKERRQ(ierr);
  ierr = TSResilSetType(resil, resil->chk_name);CHKERRQ(ierr);
	if (c->ops->setfromoptions) {ierr = (*c->ops->setfromoptions)(PetscOptionsObject,resil);CHKERRQ(ierr);}
  
  
	// surrogate module (not for AID)
  ierr = PetscStrcmp(resil->chk_name, TSRESILID, &flg);
  if(!flg){
    ierr = PetscOptionsFList("-ts_resil_surrogate","Surrogate function to use for Resilience","",TSResilList,
                            resil->sf_name, resil->sf_name,sizeof(resil->sf_name),&flg);CHKERRQ(ierr);
  
    ierr = TSResilSetType(resil, resil->sf_name);CHKERRQ(ierr);
  	if (s->ops->setfromoptions) {ierr = (*s->ops->setfromoptions)(PetscOptionsObject,resil);CHKERRQ(ierr);}
  }
	
	
  ierr = PetscOptionsInt("-ts_resil_adapt_pred","Frequence for updating the best prediction method","TSResilBestPredictor",resil->adapt_pred, &resil->adapt_pred,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-ts_resil_accept","Always accept","",resil->alwaysAccept,&resil->alwaysAccept,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnum("-ts_resil_wnormtype","Type of norm computed for error estimation","",NormTypes,(PetscEnum)resil->wnormtype,(PetscEnum*)&resil->wnormtype,NULL);CHKERRQ(ierr);

  /*ierr = PetscOptionsBool("-ts_resil_monitor","TSResil Monitor ","",PETSC_FALSE,&flg,NULL);CHKERRQ(ierr);
  if(flg) { 
    ierr                       = PetscClassIdRegister("TSResilObjMonitor",&TSRESILMONITOR_CLASSID);CHKERRQ(ierr);
    ierr                       = PetscHeaderCreate(rmon,TSRESILMONITOR_CLASSID,"TSResilObjMonitor","Resilience//Percentile monitor","TS",
                                  PetscObjectComm((PetscObject)resil), 0,0);CHKERRQ(ierr);	
    rmon->monitor = TSResilMonitor_Detection;   
    ierr = PetscStrcpy(rmon->name, "detection.txt");CHKERRQ(ierr);
    ierr = TSResilAddASCIIMonitor(resil, rmon);CHKERRQ(ierr);
  }  */
  
  ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilSetType"
PetscErrorCode TSResilSetType(TSResil resil, TSResilType mname)
{
	  PetscErrorCode ierr,(*r)(MPI_Comm, TSResil);
    MPI_Comm    comm;
    
	  PetscFunctionBegin;
	  PetscValidHeaderSpecific(resil,TSRESIL_CLASSID,1);
	  ierr = PetscFunctionListFind(TSResilList,mname,&r);CHKERRQ(ierr);
	  if (!r) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_ARG_UNKNOWN_TYPE,"Unknown TSResil Scheme \"%s\" given",mname);
    ierr = PetscObjectGetComm((PetscObject)resil,&comm);CHKERRQ(ierr);
	  ierr = (*r)(comm, resil);CHKERRQ(ierr);
	  PetscFunctionReturn(0);
}




#undef __FUNCT__
#define __FUNCT__ "TSResilCreate"
/*@
  TSResilCreate - create an resilive controller context for time stepping

  Collective on MPI_Comm

  Input Parameter:
. comm - The communicator

  Output Parameter:
. resil - new TSResil object

  Level: developer

  Notes:
  TSResil creation is handled by TS, so users should not need to call this function.

.keywords: TSResil, create
.seealso: TSGetResil(), TSResilSetType(), TSResilDestroy()
@*/
PetscErrorCode  TSResilCreate(MPI_Comm comm,TSResil *inresil)
{
  PetscErrorCode       ierr;
  TSResil              resil;
  TSResilSurrogate     s;
  TSResilCheck         c;

  PetscFunctionBegin;
  PetscValidPointer(inresil,1);
  *inresil = NULL;
  ierr = TSResilInitializePackage();CHKERRQ(ierr);
  ierr = PetscHeaderCreate(resil,TSRESIL_CLASSID,"TSResil","Time stepping resilience","TS",comm,TSResilDestroy,TSResilView);CHKERRQ(ierr);
  resil->adapt_pred = 5;
  resil->FSAL       = 0;
  PetscStrncpy(resil->sf_name, "", 20);
  PetscStrncpy(resil->chk_name, "id", 20);
  ierr = PetscClassIdRegister("TSResilSurrogate",&TSRESILSURROGATE_CLASSID);CHKERRQ(ierr);
  ierr = PetscHeaderCreate(s,TSRESILSURROGATE_CLASSID,"TSResilSurrogate","Surrogate resilience","TS",comm,0,0);CHKERRQ(ierr);
  ierr = PetscClassIdRegister("TSResilCheck",&TSRESILCHECK_CLASSID);CHKERRQ(ierr);
  ierr = PetscHeaderCreate(c,TSRESILCHECK_CLASSID,"TSResilCheck","Check resilience","TS",comm,0,0);CHKERRQ(ierr);
  resil->surrogate = s;
  resil->alwaysAccept = PETSC_FALSE;
  resil->check = c;
  *inresil = resil;
  PetscFunctionReturn(0);
}
