#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/


#undef __FUNCT__
#define __FUNCT__ "TSResil_Zero"
static PetscErrorCode TSResil_Zero(TSResil resil, TS ts, Vec old_sol)
{
  TSResilPrediction	  p = resil->prediction;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if(!p->pred) { ierr = VecDuplicate(resil->lastSol, &p->pred);CHKERRQ(ierr);}
  ierr = VecZeroEntries(p->pred);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilCreate_Zero"
PETSC_EXTERN PetscErrorCode TSResilCreate_Zero(MPI_Comm comm,TSResil resil)
{
  TSResilPrediction	  p = resil->prediction;
  
	PetscFunctionBegin;
  p->ops->compute        = TSResil_Zero;
	PetscFunctionReturn(0);
}
