#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/


#undef __FUNCT__
#define __FUNCT__ "TSResilCompute_Quadratic"
static PetscErrorCode TSResilCompute_Quadratic(TSResil_Prediction p, Vec X1, Vec X2, Vec X3, Vec Y) {
  PetscErrorCode ierr;
    
  PetscFunctionBegin;
  ierr = VecCopy(X1,Y);CHKERRQ(ierr);
  ierr = VecAYPBXPCZ(Y, 3.0, -3.0, 1.0, X2, X3);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
