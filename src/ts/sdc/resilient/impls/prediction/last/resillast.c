#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/


#undef __FUNCT__
#define __FUNCT__ "TSResilCompute_Last"
static PetscErrorCode TSResilCompute_Last(Vec X1, Vec X2, Vec X3, Vec Y) {
{
  PetscErrorCode ierr;
    
  PetscFunctionBegin;
  ierr = VecCopy(X1,Y);
  PetscFunctionReturn(0);
}
