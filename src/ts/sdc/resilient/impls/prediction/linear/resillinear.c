#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/


#undef __FUNCT__
#define __FUNCT__ "TSResilCompute_Linear"
static PetscErrorCode TSResilCompute_Linear(TSResil_Prediction p, Vec X1, Vec X2, Vec X3, Vec Y) {
{
  PetscErrorCode ierr;
    
  PetscFunctionBegin;
  ierr = VecCopy(X1,Y);
  ierr = VecAYPBX(Y, 2.0, -1.0, X1);
  PetscFunctionReturn(0);
}
