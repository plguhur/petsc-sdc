#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/
#include "petscsys.h"   

typedef struct {
  PETSCHEADER(struct _TSResilOps);
	TSResilEstimate			estimate1,estimate2;
	EstType		          type1, type2;
	PetscBool           monitor;
	
} TSResil_Delta;

PETSC_EXTERN PetscErrorCode TSResilCreate_Embedded(TSResilEstimate) ;
PETSC_EXTERN PetscErrorCode TSResilCreate_Radau(MPI_Comm, TSResilEstimate);
PETSC_EXTERN PetscErrorCode TSResilCreate_BDF(MPI_Comm, TSResilEstimate);
PETSC_EXTERN PetscErrorCode TSResilCreate_Last(MPI_Comm, TSResilEstimate);
PETSC_EXTERN PetscErrorCode TSResilCreate_AdamsB(MPI_Comm, TSResilEstimate);
static PetscClassId      TSRESILDELTA_CLASSID;
static PetscClassId      TSRESILEST1_CLASSID;
static PetscClassId      TSRESILEST2_CLASSID;
static PetscClassId      TSRESILMONITORDELTA_CLASSID;

#undef __FUNCT__
#define __FUNCT__ "TSResilCompute_Delta"
static PetscErrorCode TSResilCompute_Delta(TSResil r, TS ts, Vec old_sol)
{
	TSResilSurrogate	  s = r->surrogate;
	TSResil_Delta	  		*d = (TSResil_Delta*)s->data;
  TSResilEstimate		  est1 = d->estimate1, est2 = d->estimate2;
	PetscErrorCode 			ierr;

  PetscFunctionBegin;
	(*est1->ops->compute)(est1, ts, old_sol);
	(*est2->ops->compute)(est2, ts, old_sol);
	if(s->vecForm) 
	{ 	
      if(!s->v_sf) { ierr = VecDuplicate(est1->v_est, &s->v_sf);CHKERRQ(ierr);}
      ierr = VecCopy(est1->v_est,s->v_sf);CHKERRQ(ierr);
			ierr = VecAYPX(s->v_sf,-1,est2->v_est);CHKERRQ(ierr); 
	}
	else {	s->r_sf = PetscAbs(est1->r_est - est2->r_est); }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilUpdate_Delta"
PetscErrorCode TSResilUpdate_Delta(TSResil resil, TS ts, Vec old_sol)
{
  TSResilSurrogate	  s = resil->surrogate;
  TSResil_Delta	  		*d = (TSResil_Delta*)s->data;
  PetscErrorCode      ierr;

  PetscFunctionBegin;
	if (d->estimate1->ops->update)  {ierr = (*d->estimate1->ops->update)(d->estimate1,ts,old_sol);CHKERRQ(ierr);}
	if (d->estimate2->ops->update)  {ierr = (*d->estimate2->ops->update)(d->estimate2,ts,old_sol);CHKERRQ(ierr);}
  if (d->type1 == TSRESILEST_LAST) {
    if(d->estimate1->vecForm) {
			ierr = VecCopy(d->estimate2->v_est, d->estimate1->v_est);CHKERRQ(ierr);
	  }
  	else {
			d->estimate1->r_est = d->estimate2->r_est;
  	}
  }
  if (d->type2 == TSRESILEST_LAST) {
    if(d->estimate2->vecForm) {
			ierr = VecCopy(d->estimate1->v_est, d->estimate2->v_est);CHKERRQ(ierr);
	  }
  	else {
			d->estimate2->r_est = d->estimate1->r_est;
  	}
  }
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilReset_Delta"
static PetscErrorCode TSResilReset_Delta(TSResil r)
{
	TSResilSurrogate	  s = (TSResilSurrogate)r->surrogate;
	TSResil_Delta	  		*d = (TSResil_Delta*)s->data;
  TSResilEstimate		est1 = d->estimate1, est2 = d->estimate2;
	PetscErrorCode ierr;

  PetscFunctionBegin;
  if(est1->ops->reset) { ierr = (*est1->ops->reset)(est1);CHKERRQ(ierr);}
  if(est2->ops->reset) { ierr = (*est2->ops->reset)(est2);CHKERRQ(ierr);}
  ierr = PetscHeaderDestroy(&est1);CHKERRQ(ierr);
  ierr = PetscHeaderDestroy(&est2);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilDestroy_Delta"
static PetscErrorCode TSResilDestroy_Delta(TSResil r)
{
  TSResilSurrogate	  s = (TSResilSurrogate)r->surrogate;
	TSResil_Delta	  		*d = (TSResil_Delta*)s->data;
	PetscErrorCode ierr;

  PetscFunctionBegin;
	ierr = TSResilReset_Delta(r);CHKERRQ(ierr);
  ierr = PetscHeaderDestroy(&d);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}




#undef __FUNCT__
#define __FUNCT__ "TSResilSetEstimate_Delta"
static PetscErrorCode TSResilSetEstimate_Delta(TSResil r, TSResilEstimate est, EstType type)
{
	PetscErrorCode 			ierr;
  MPI_Comm    comm;

  PetscFunctionBegin;
	if(type == TSRESILEST_EMBD) {
			ierr  = TSResilCreate_Embedded(est);CHKERRQ(ierr);
	}
	else if(type == TSRESILEST_RADAU) {
      ierr = PetscObjectGetComm((PetscObject)r,&comm);CHKERRQ(ierr);
			ierr  = TSResilCreate_Radau(comm, est);CHKERRQ(ierr);
	}
	else if(type == TSRESILEST_BDF) {
      ierr = PetscObjectGetComm((PetscObject)r,&comm);CHKERRQ(ierr);
			ierr  = TSResilCreate_BDF(comm, est);CHKERRQ(ierr);
	}
	else if(type == TSRESILEST_LAST) {
      ierr = PetscObjectGetComm((PetscObject)r,&comm);CHKERRQ(ierr);
			ierr  = TSResilCreate_Last(comm, est);CHKERRQ(ierr);
	}
	else if(type == TSRESILEST_ADAMSB) {
      ierr = PetscObjectGetComm((PetscObject)r,&comm);CHKERRQ(ierr);
			ierr  = TSResilCreate_AdamsB(comm, est);CHKERRQ(ierr);
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilMonitor_Delta"
static PetscErrorCode TSResilMonitor_Delta(TSResil resil, PetscInt steps, PetscReal curtime,  Vec sol, PetscViewer viewer)
{
	TSResilSurrogate	  s = (TSResilSurrogate)resil->surrogate;
  TSResil_Delta	  		*d = (TSResil_Delta*)s->data;
  TSResilEstimate		  est1 = d->estimate1, est2 = d->estimate2;
  PetscErrorCode 			ierr;

  PetscFunctionBegin;
	if(s->vecForm) 
	{ 	
      ierr = VecNorm(est1->v_est, s->norm, &est1->r_est);CHKERRQ(ierr);
      ierr = VecNorm(est2->v_est, s->norm, &est2->r_est);CHKERRQ(ierr); 
	}
  ierr = PetscViewerASCIIPrintf(viewer,"%i, %g, %g, %g, ",steps,curtime,est1->r_est,est2->r_est);CHKERRQ(ierr);
  if(s->vecForm) {
    ierr = VecView(s->v_sf, viewer); // not tested
    ierr = PetscViewerASCIIPrintf(viewer, "\n");CHKERRQ(ierr);
  } else {
    ierr = PetscViewerASCIIPrintf(viewer,"%g\n",s->r_sf);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilSetFromOptions_Delta"
static PetscErrorCode TSResilSetFromOptions_Delta(PetscOptionItems *PetscOptionsObject,TSResil r)
{
	TSResilSurrogate	  s = (TSResilSurrogate)r->surrogate;
	TSResil_Delta	  		*d = (TSResil_Delta*)s->data;
  TSResilEstimate		  est1 = d->estimate1, est2 = d->estimate2;
  PetscBool           flg = PETSC_FALSE;
  TSResilObjMonitor   rmon = NULL;
	PetscErrorCode 			ierr;

  PetscFunctionBegin;
  ierr = PetscOptionsHead(PetscOptionsObject,"Resil:: Surrogate function:: Delta options");CHKERRQ(ierr);
  PetscOptionsEnum("-ts_resil_est_type1","Type of estimate 1","",EstTypes,(PetscEnum)d->type1,(PetscEnum*)&d->type1,NULL);
	PetscOptionsEnum("-ts_resil_est_type2","Type of estimate 2","",EstTypes,(PetscEnum)d->type2,(PetscEnum*)&d->type2,NULL);
  ierr = PetscOptionsInt("-ts_resil_est1_order","Order of method","",est1->order,&est1->order,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-ts_resil_est2_order","Order of method","",est2->order,&est2->order,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-ts_resil_monitor_surrogate","Monitor surrogate function","",PETSC_FALSE,&flg,NULL);CHKERRQ(ierr);
  if(flg) { 
    ierr                       = PetscClassIdRegister("TSResilObjMonitor",&TSRESILMONITORDELTA_CLASSID);CHKERRQ(ierr);
    ierr                       = PetscHeaderCreate(rmon,TSRESILMONITORDELTA_CLASSID,"TSResilObjMonitor","Resilience//Delta monitor","TS",
                                  PetscObjectComm((PetscObject)r), 0,0);CHKERRQ(ierr);	
    rmon->monitor = TSResilMonitor_Delta;CHKMEMQ;
    ierr = PetscStrcpy(rmon->name, "surr.txt");CHKERRQ(ierr);
    ierr = TSResilAddASCIIMonitor(r, rmon);CHKERRQ(ierr);
    d->monitor = PETSC_TRUE;
  }    
  ierr = TSResilSetEstimate_Delta(r, est1, d->type1);CHKERRQ(ierr);
	ierr = TSResilSetEstimate_Delta(r, est2, d->type2);CHKERRQ(ierr);
	ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilView_Delta"
static PetscErrorCode TSResilView_Delta(TSResil r,PetscViewer viewer)
{
	TSResilSurrogate	  s = (TSResilSurrogate)r->surrogate;
	TSResil_Delta	  		*d = (TSResil_Delta*)s->data;
	TSResilEstimate		est1 = d->estimate1, est2 = d->estimate2;
  PetscErrorCode 			ierr;

  PetscFunctionBegin;
  if(est1->ops->view) { ierr = (*est1->ops->view)(est1,viewer);CHKERRQ(ierr);}
  if(est1->ops->view) { ierr = (*est2->ops->view)(est2,viewer);CHKERRQ(ierr);}
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilCreate_Delta"
PETSC_EXTERN PetscErrorCode TSResilCreate_Delta(MPI_Comm comm,TSResil r)
{
	TSResilSurrogate	  s = (TSResilSurrogate)r->surrogate;
	TSResil_Delta	  		*d;
  TSResilEstimate     est1, est2;
	PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr                       = PetscClassIdRegister("TSResil_Delta",&TSRESILDELTA_CLASSID);CHKERRQ(ierr);
  ierr                       = PetscHeaderCreate(d,TSRESILDELTA_CLASSID,"TSResil_Delta","Resilience//Delta module for surrogating","TS",comm,
					TSResilDestroy_Delta,TSResilView_Delta);CHKERRQ(ierr);	
  ierr                       = PetscClassIdRegister("TSResilEstimate",&TSRESILEST1_CLASSID);CHKERRQ(ierr);
  ierr                       = PetscHeaderCreate(est1,TSRESILEST1_CLASSID,"TSResilEstimate","Resilience//est1 module for surrogating","TS",comm,
          0,0);CHKERRQ(ierr);	
  ierr                       = PetscClassIdRegister("TSResilEstimate",&TSRESILEST2_CLASSID);CHKERRQ(ierr);
  ierr                       = PetscHeaderCreate(est2,TSRESILEST2_CLASSID,"TSResilEstimate","Resilience//est2 module for surrogating","TS",comm,
  				0,0);CHKERRQ(ierr);	
  ierr                       = PetscLogObjectParent((PetscObject)s,(PetscObject)d);CHKERRQ(ierr);
  ierr                       = PetscObjectIncrementTabLevel((PetscObject)d,(PetscObject)s,1);CHKERRQ(ierr);
  s->data                		 = (void*)d;
  s->ops->compute        		 = TSResilCompute_Delta;
  s->ops->setfromoptions 		 = TSResilSetFromOptions_Delta;
  s->ops->destroy        		 = TSResilDestroy_Delta;
  s->ops->view           		 = TSResilView_Delta;
	s->ops->update         	   = TSResilUpdate_Delta;
	d->type1									 = TSRESILEST_EMBD;
	d->type2									 = TSRESILEST_LAST;
  est1->order                = 0;
  est2->order                = 0;
  est1->norm                 = NORM_2;
  est2->norm                 = NORM_2;
  d->estimate1               = est1;
  d->estimate2               = est2;
  d->monitor                 = PETSC_FALSE;
  CHKMEMQ;
  PetscFunctionReturn(0);
 
}
