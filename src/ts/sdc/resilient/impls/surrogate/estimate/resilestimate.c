#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/
#include "petscsys.h"   

typedef struct {
  PETSCHEADER(struct _TSResilOps);
	TSResilEstimate			estimate;
	EstType		          type;
	PetscBool           monitor;
	
} TSResil_Estimate;

PETSC_EXTERN PetscErrorCode TSResilCreate_Embedded(TSResilEstimate) ;
PETSC_EXTERN PetscErrorCode TSResilCreate_Radau(MPI_Comm, TSResilEstimate);
PETSC_EXTERN PetscErrorCode TSResilCreate_BDF(MPI_Comm, TSResilEstimate);
PETSC_EXTERN PetscErrorCode TSResilCreate_Last(MPI_Comm, TSResilEstimate);
PETSC_EXTERN PetscErrorCode TSResilCreate_AdamsB(MPI_Comm, TSResilEstimate);
static PetscClassId      TSRESILESTIMATE_CLASSID;
static PetscClassId      TSRESILEST_CLASSID;
static PetscClassId      TSRESILMONITORESTIMATE_CLASSID;

#undef __FUNCT__
#define __FUNCT__ "TSResilCompute_Estimate"
static PetscErrorCode TSResilCompute_Estimate(TSResil r, TS ts, Vec old_sol)
{
	TSResilSurrogate	  s = r->surrogate;
	TSResil_Estimate	  		*d = (TSResil_Estimate*)s->data;
  TSResilEstimate		  est = d->estimate;
	PetscErrorCode 			ierr;

  PetscFunctionBegin;
	(*est->ops->compute)(est, ts, old_sol);
	if(s->vecForm) 
	{ 	
      if(!s->v_sf) { ierr = VecDuplicate(est->v_est, &s->v_sf);CHKERRQ(ierr);}
      ierr = VecCopy(est->v_est,s->v_sf);CHKERRQ(ierr);
	}
	else {	s->r_sf = est->r_est; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilUpdate_Estimate"
PetscErrorCode TSResilUpdate_Estimate(TSResil resil, TS ts, Vec old_sol)
{
  TSResilSurrogate	  s = resil->surrogate;
  TSResil_Estimate	  		*d = (TSResil_Estimate*)s->data;
  PetscErrorCode      ierr;

  PetscFunctionBegin;
	if (d->estimate->ops->update)  {ierr = (*d->estimate->ops->update)(d->estimate,ts,old_sol);CHKERRQ(ierr);}
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilReset_Estimate"
static PetscErrorCode TSResilReset_Estimate(TSResil r)
{
	TSResilSurrogate	  s = (TSResilSurrogate)r->surrogate;
	TSResil_Estimate	  		*d = (TSResil_Estimate*)s->data;
  TSResilEstimate		est = d->estimate;
	PetscErrorCode ierr;

  PetscFunctionBegin;
  if(est->ops->reset) { ierr = (*est->ops->reset)(est);CHKERRQ(ierr);}
  ierr = PetscHeaderDestroy(&est);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilDestroy_Estimate"
static PetscErrorCode TSResilDestroy_Estimate(TSResil r)
{
  TSResilSurrogate	  s = (TSResilSurrogate)r->surrogate;
	TSResil_Estimate	  		*d = (TSResil_Estimate*)s->data;
	PetscErrorCode ierr;

  PetscFunctionBegin;
	ierr = TSResilReset_Estimate(r);CHKERRQ(ierr);
  ierr = PetscHeaderDestroy(&d);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}




#undef __FUNCT__
#define __FUNCT__ "TSResilSetEstimate_Estimate"
static PetscErrorCode TSResilSetEstimate_Estimate(TSResil r, TSResilEstimate est, EstType type)
{
	PetscErrorCode 			ierr;
  MPI_Comm    comm;

  PetscFunctionBegin;
	if(type == TSRESILEST_EMBD) {
			ierr  = TSResilCreate_Embedded(est);CHKERRQ(ierr);
	}
	else if(type == TSRESILEST_RADAU) {
      ierr = PetscObjectGetComm((PetscObject)r,&comm);CHKERRQ(ierr);
			ierr  = TSResilCreate_Radau(comm, est);CHKERRQ(ierr);
	}
	else if(type == TSRESILEST_BDF) {
      ierr = PetscObjectGetComm((PetscObject)r,&comm);CHKERRQ(ierr);
			ierr  = TSResilCreate_BDF(comm, est);CHKERRQ(ierr);
	}
	else if(type == TSRESILEST_ADAMSB) {
      ierr = PetscObjectGetComm((PetscObject)r,&comm);CHKERRQ(ierr);
			ierr  = TSResilCreate_AdamsB(comm, est);CHKERRQ(ierr);
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilMonitor_Estimate"
static PetscErrorCode TSResilMonitor_Estimate(TSResil resil, PetscInt steps, PetscReal curtime,  Vec sol, PetscViewer viewer)
{
	TSResilSurrogate	  s = (TSResilSurrogate)resil->surrogate;
  TSResil_Estimate	  		*d = (TSResil_Estimate*)s->data;
  TSResilEstimate		  est = d->estimate;
  PetscErrorCode 			ierr;

  PetscFunctionBegin;
  ierr = PetscViewerASCIIPrintf(viewer,"%i, %g, %g\n",steps,curtime,s->r_sf);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilSetFromOptions_Estimate"
static PetscErrorCode TSResilSetFromOptions_Estimate(PetscOptionItems *PetscOptionsObject,TSResil r)
{
	TSResilSurrogate	  s = (TSResilSurrogate)r->surrogate;
	TSResil_Estimate	  		*d = (TSResil_Estimate*)s->data;
  TSResilEstimate		  est = d->estimate;
  PetscBool           flg = PETSC_FALSE;
  TSResilObjMonitor   rmon = NULL;
	PetscErrorCode 			ierr;

  PetscFunctionBegin;
  ierr = PetscOptionsHead(PetscOptionsObject,"Resil:: Surrogate function:: Estimate options");CHKERRQ(ierr);
  PetscOptionsEnum("-ts_resil_est_type","Type of estimate","",EstTypes,(PetscEnum)d->type,(PetscEnum*)&d->type,NULL);
  ierr = PetscOptionsInt("-ts_resil_est_order","Order of method","",est->order,&est->order,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-ts_resil_monitor_surrogate","Monitor surrogate function","",PETSC_FALSE,&flg,NULL);CHKERRQ(ierr);
  if(flg) { 
    ierr                       = PetscClassIdRegister("TSResilObjMonitor",&TSRESILMONITORESTIMATE_CLASSID);CHKERRQ(ierr);
    ierr                       = PetscHeaderCreate(rmon,TSRESILMONITORESTIMATE_CLASSID,"TSResilObjMonitor","Resilience//Estimate monitor","TS",
                                  PetscObjectComm((PetscObject)r), 0,0);CHKERRQ(ierr);	
    rmon->monitor = TSResilMonitor_Estimate;CHKMEMQ;
    ierr = PetscStrcpy(rmon->name, "surr.txt");CHKERRQ(ierr);
    ierr = TSResilAddASCIIMonitor(r, rmon);CHKERRQ(ierr);
    d->monitor = PETSC_TRUE;
  }    
  ierr = TSResilSetEstimate_Estimate(r, est, d->type);CHKERRQ(ierr);
	ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilView_Estimate"
static PetscErrorCode TSResilView_Estimate(TSResil r,PetscViewer viewer)
{
	TSResilSurrogate	  s = (TSResilSurrogate)r->surrogate;
	TSResil_Estimate	  		*d = (TSResil_Estimate*)s->data;
	TSResilEstimate		  est = d->estimate;
  PetscErrorCode 			ierr;

  PetscFunctionBegin;
  if(est->ops->view) { ierr = (*est->ops->view)(est,viewer);CHKERRQ(ierr);}
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilCreate_Estimate"
PETSC_EXTERN PetscErrorCode TSResilCreate_Estimate(MPI_Comm comm,TSResil r)
{
	TSResilSurrogate	  s = (TSResilSurrogate)r->surrogate;
	TSResil_Estimate	  		*d;
  TSResilEstimate     est;
	PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr                       = PetscClassIdRegister("TSResil_Estimate",&TSRESILESTIMATE_CLASSID);CHKERRQ(ierr);
  ierr                       = PetscHeaderCreate(d,TSRESILESTIMATE_CLASSID,"TSResil_Estimate","Resilience//Estimate module for surrogating","TS",comm,
					TSResilDestroy_Estimate,TSResilView_Estimate);CHKERRQ(ierr);	
  ierr                       = PetscClassIdRegister("TSResilEstimate",&TSRESILEST_CLASSID);CHKERRQ(ierr);
  ierr                       = PetscHeaderCreate(est,TSRESILEST_CLASSID,"TSResilEstimate","Resilience//est module for surrogating","TS",comm,
          0,0);CHKERRQ(ierr);	
  ierr                       = PetscLogObjectParent((PetscObject)s,(PetscObject)d);CHKERRQ(ierr);
  ierr                       = PetscObjectIncrementTabLevel((PetscObject)d,(PetscObject)s,1);CHKERRQ(ierr);
  s->data                		 = (void*)d;
  s->ops->compute        		 = TSResilCompute_Estimate;
  s->ops->setfromoptions 		 = TSResilSetFromOptions_Estimate;
  s->ops->destroy        		 = TSResilDestroy_Estimate;
  s->ops->view           		 = TSResilView_Estimate;
	s->ops->update         	   = TSResilUpdate_Estimate;
	d->type 									 = TSRESILEST_EMBD;
  est->order                = 0;
  est->norm                 = NORM_2;
  d->estimate               = est;
  d->monitor                 = PETSC_FALSE;
  PetscFunctionReturn(0);
 
}
