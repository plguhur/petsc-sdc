#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/


#undef __FUNCT__
#define __FUNCT__ "TSResilCompute_Embedded"
static PetscErrorCode TSResilCompute_Embedded(TSResilEstimate estimate, TS ts, Vec old_sol)
{
	Vec 					   X,Y;
  PetscErrorCode   ierr;

  PetscFunctionBegin;
  ierr = TSGetSolution(ts,&X);CHKERRQ(ierr);
  ierr = VecDuplicate(X,&Y);CHKERRQ(ierr);
  ierr  = TSEvaluateStep(ts,estimate->order,Y,NULL);CHKERRQ(ierr);
	ierr = VecAYPX(Y,-1,X);CHKERRQ(ierr);
	if(estimate->vecForm) {
			ierr = VecCopy(Y,estimate->v_est);CHKERRQ(ierr);
	}
	else {
			ierr  = VecNorm(Y,estimate->norm,&estimate->r_est);CHKERRQ(ierr);
	}
  PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "TSResilCreate_Embedded"
PETSC_EXTERN PetscErrorCode TSResilCreate_Embedded(TSResilEstimate estimate)
{

	PetscFunctionBegin;
  estimate->ops->compute        = TSResilCompute_Embedded;
	PetscFunctionReturn(0);
}
