#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/

typedef struct {
  PETSCHEADER(int);
	Vec 			X1,X2,X3,F1,F2,F3;
  PetscReal time_step; /* time_step with which is computed history. might be different from ts->time_step */
	PetscInt  history;   /* length of history */
} TSResil_Radau;

static PetscClassId      TSRESIL_EST_RADAU_CLASSID;
static PetscErrorCode TSResilRadau_1(TSResilEstimate, TS, Vec);
static PetscErrorCode TSResilRadau_2(TSResilEstimate, TS, Vec);
static PetscErrorCode TSResilRadau_3(TSResilEstimate, TS, Vec);
static PetscErrorCode TSResilRadau_4(TSResilEstimate, TS, Vec);




  
#undef __FUNCT__
#define __FUNCT__ "TSResilCompute_Radau"
static PetscErrorCode TSResilCompute_Radau(TSResilEstimate estimate, TS ts, Vec old_sol)
{
  TSResil_Radau	  *radau = (TSResil_Radau*)estimate->data;
  
  PetscFunctionBegin;
  if(!radau->time_step) { radau->time_step = ts->time_step;}
  if(estimate->order == 1) {TSResilRadau_1(estimate, ts, old_sol);}
  if(estimate->order == 2) {TSResilRadau_2(estimate, ts, old_sol);}
  if(estimate->order == 3) {TSResilRadau_3(estimate, ts, old_sol);}
  if(estimate->order == 4) {TSResilRadau_4(estimate, ts, old_sol);}
  //if(estimate->order == 5) {TSResilRadau_5(*estimate, ts);}
 PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilRadau_1"
static PetscErrorCode TSResilRadau_1(TSResilEstimate estimate, TS ts, Vec old_sol)
{
	Vec 						nextStage, Y, *slopes;
	PetscInt 				s;
  PetscReal       h = (PetscReal)ts->time_step;
  PetscErrorCode 	ierr;
	
	PetscFunctionBegin;
  ierr = VecDuplicate(ts->vec_sol,&Y);CHKERRQ(ierr);
	ierr = VecCopy(ts->vec_sol, Y);CHKERRQ(ierr);
	ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr);
  ierr = VecDuplicate(Y,&nextStage);CHKERRQ(ierr);
	if(ts->resil->FSAL) 
	{		
			ierr = VecAXPBYPCZ(Y, 0.5*h,0.5*h,-1.0,slopes[0],slopes[ts->resil->FSAL]);CHKERRQ(ierr);
	}
	else
	{
			ierr = TSComputeRHSFunction(ts, ts->ptime, Y, nextStage);
			ierr = VecAXPBYPCZ(Y, 0.5*h,0.5*h,-1.0,slopes[0],nextStage);CHKERRQ(ierr);
	}
	ierr = VecAYPX(Y,1.0,old_sol);CHKERRQ(ierr);
	// transform into scalar if required
	if(estimate->vecForm) {
			ierr = VecCopy(Y,estimate->v_est);CHKERRQ(ierr);
	}
	else {
			ierr  = VecNorm(Y,estimate->norm, &estimate->r_est);CHKERRQ(ierr);
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilRadau_2"
static PetscErrorCode TSResilRadau_2(TSResilEstimate estimate, TS ts, Vec old_sol)
{
	Vec 						Y, *slopes;
	TSResil_Radau	  *radau = (TSResil_Radau*)estimate->data;
	PetscInt 				s;
  PetscReal       h = (PetscReal)ts->time_step;
  PetscErrorCode 	ierr;
	
	PetscFunctionBegin;
  ierr = VecDuplicate(ts->vec_sol,&Y);CHKERRQ(ierr);
	ierr = VecCopy(ts->vec_sol, Y);CHKERRQ(ierr);
	ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr);
	if (radau->history > 0) {
		
		ierr = VecAXPBYPCZ(Y,-2.0/3.0,5.0/6.0,-1.0/6.0,old_sol,radau->X1);
		ierr = VecAXPBYPCZ(Y,h*2.0/3.0,h*1.0/3.0,1.0,slopes[0],radau->F1);
		
		// transform into scalar if required
		if(estimate->vecForm) {
				ierr = VecCopy(Y,estimate->v_est);CHKERRQ(ierr);
		}
		else {
				ierr  = VecNorm(Y,estimate->norm, &estimate->r_est);CHKERRQ(ierr);
		}
		
	}
	else {
		TSResilRadau_1(estimate, ts, old_sol);
	}

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilRadau_3"
static PetscErrorCode TSResilRadau_3(TSResilEstimate estimate, TS ts, Vec old_sol)
{
	TSResil_Radau	  *radau = (TSResil_Radau*)estimate->data;
	Vec 						nextStage, Y, *slopes;
  PetscReal       h = (PetscReal)ts->time_step;
	PetscInt 				s;
  PetscErrorCode 	ierr;
	
	PetscFunctionBegin;
  ierr = VecDuplicate(ts->vec_sol,&Y);CHKERRQ(ierr);
	ierr = VecCopy(ts->vec_sol, Y);CHKERRQ(ierr);
	ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr);
	if (radau->history > 0) {
		if(ts->resil->FSAL) {		
				ierr = VecAXPBYPCZ(Y, h*2.0/3.0,h/6.0,-0.5,slopes[0],slopes[ts->resil->FSAL]);CHKERRQ(ierr);
		}	else {
        ierr = VecDuplicate(Y,&nextStage);CHKERRQ(ierr);
				ierr = TSComputeRHSFunction(ts, ts->ptime, Y, nextStage);CHKERRQ(ierr);
				ierr = VecAXPBYPCZ(Y, h*2.0/3.0,h/6.0,-0.5,slopes[0],nextStage);CHKERRQ(ierr);
		}
		ierr = VecAXPBYPCZ(Y,h/6.0,0.5,1.0,radau->F1,radau->X1);CHKERRQ(ierr);
		// transform into scalar if required
		if(estimate->vecForm) {
				ierr = VecCopy(Y,estimate->v_est);CHKERRQ(ierr);
		} else {
				ierr  = VecNorm(Y,estimate->norm, &estimate->r_est);CHKERRQ(ierr);
		}
		
	}	else {
		TSResilRadau_1(estimate, ts, old_sol);
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilRadau_4"
static PetscErrorCode TSResilRadau_4(TSResilEstimate estimate, TS ts, Vec old_sol)
{
	TSResil_Radau	  *radau = (TSResil_Radau*)estimate->data;
	Vec 						Y, *slopes;
	PetscInt 				s;
  PetscReal       h = (PetscReal)ts->time_step;
  PetscErrorCode 	ierr;
	
	PetscFunctionBegin;
  ierr = VecDuplicate(ts->vec_sol,&Y);CHKERRQ(ierr);
	ierr = VecCopy(ts->vec_sol, Y);CHKERRQ(ierr);
	ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr);
  
	if (radau->history > 1) {
		ierr = VecAXPBYPCZ(Y, h*3.0/5.0, h*3.0/10.0,-1.0/30.0,radau->F1,slopes[0]);CHKERRQ(ierr);
		ierr = VecAXPBYPCZ(Y,h/10.0,3.0/10.0,1.0,radau->F2,radau->X1);CHKERRQ(ierr);
		ierr = VecAXPBYPCZ(Y,-3.0/5.0,1.0/3.0,1.0,old_sol,radau->X2);CHKERRQ(ierr);
		// transform into scalar if required
		if(estimate->vecForm) {
				ierr = VecCopy(Y,estimate->v_est);CHKERRQ(ierr);
		}
		else {
				ierr  = VecNorm(Y,estimate->norm, &estimate->r_est);CHKERRQ(ierr);
		}
		
	}
	else {
    TSResilRadau_3(estimate, ts, old_sol);		
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilAdaptTS_Radau"
static PetscErrorCode TSResilAdaptTS_Radau(TSResilEstimate estimate, TS ts, Vec old_sol) {
  Vec 			v, X = ts->vec_sol, *slopes;
  TSResil_Radau	  *radau = (TSResil_Radau*)estimate->data;
  PetscReal h2=(PetscReal)ts->time_step, h = (PetscReal)radau->time_step, alpha = -(h2-h)/h, beta = h2/h;
  PetscInt  s;
  PetscErrorCode 	ierr;
  
  PetscFunctionBegin;
  ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr);
  ierr = VecDuplicate(X, &v);CHKERRQ(ierr);

  if(radau->X1) {
    ierr = VecAXPBY(radau->X1, alpha, beta, old_sol);CHKERRQ(ierr);
  }
  if(radau->X2) {
    ierr = VecAXPBY(radau->X2, alpha, beta, radau->X1);CHKERRQ(ierr);
  }
  if(radau->F1) {
    ierr = VecAXPBY(radau->F1, alpha, beta, slopes[0]);CHKERRQ(ierr);
  }
  if(radau->F2) {
    ierr = VecAXPBY(radau->F2, alpha, beta, radau->F1);CHKERRQ(ierr);
  }
  radau->time_step = ts->time_step;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilUpdate_Radau"
PetscErrorCode TSResilUpdate_Radau(TSResilEstimate estimate, TS ts, Vec old_sol)
{
  TSResil_Radau	  *radau = (TSResil_Radau*)estimate->data;
  PetscErrorCode      ierr;
	PetscInt 						s;
	Vec									Y, *slopes;

  PetscFunctionBegin;
  if(ts->time_step != radau->time_step) {ierr = TSResilAdaptTS_Radau(estimate,ts,old_sol);CHKERRQ(ierr);}
  ierr = TSGetSolution(ts,&Y);CHKERRQ(ierr);
	ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr);
	if(estimate->order == 2) {
    if(!radau->X1) { ierr = VecDuplicate(Y, &radau->X1);CHKERRQ(ierr);}
    if(!radau->F1) { ierr = VecDuplicate(Y, &radau->F1);CHKERRQ(ierr);}
		ierr = VecCopy(slopes[0],radau->F1);CHKERRQ(ierr);
		ierr = VecCopy(old_sol,radau->X1);CHKERRQ(ierr);
	}
	if(estimate->order == 3) {
    if(!radau->X1) { ierr = VecDuplicate(Y, &radau->X1);CHKERRQ(ierr);}
    if(!radau->F1) { ierr = VecDuplicate(Y, &radau->F1);CHKERRQ(ierr);}
		ierr = VecCopy(old_sol,radau->X1);CHKERRQ(ierr);
		ierr = VecCopy(slopes[0],radau->F1);CHKERRQ(ierr);
	}
	if(estimate->order == 4) {
    if(!radau->X1) { ierr = VecDuplicate(Y, &radau->X1);CHKERRQ(ierr);}
    if(!radau->X2) { ierr = VecDuplicate(Y, &radau->X2);CHKERRQ(ierr);}
    if(!radau->F1) { ierr = VecDuplicate(Y, &radau->F1);CHKERRQ(ierr);}
    if(!radau->F2) { ierr = VecDuplicate(Y, &radau->F2);CHKERRQ(ierr);}
		ierr = VecCopy(radau->X1,radau->X2);CHKERRQ(ierr);
    ierr = VecCopy(old_sol,radau->X1);CHKERRQ(ierr);
    ierr = VecCopy(radau->F1,radau->F2);CHKERRQ(ierr);
    ierr = VecCopy(slopes[0],radau->F1);CHKERRQ(ierr);
	}
  radau->history += 1;
  PetscFunctionReturn(0);
}




#undef __FUNCT__
#define __FUNCT__ "TSResilReset_Radau"
static PetscErrorCode TSResilReset_Radau(TSResilEstimate estimate)
{
  TSResil_Radau	  *radau = (TSResil_Radau*)estimate->data;
	PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = VecDestroy(&radau->X1);CHKERRQ(ierr);
  ierr = VecDestroy(&radau->X2);CHKERRQ(ierr);
  ierr = VecDestroy(&radau->X3);CHKERRQ(ierr);
	ierr = VecDestroy(&radau->F1);CHKERRQ(ierr);
	ierr = VecDestroy(&radau->F2);CHKERRQ(ierr);
  ierr = VecDestroy(&radau->F3);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilDestroy_Radau"
static PetscErrorCode TSResilDestroy_Radau(TSResilEstimate estimate)
{
  TSResil_Radau	  *radau = (TSResil_Radau*)estimate->data;
	PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = TSResilReset_Radau(estimate);CHKERRQ(ierr);
  ierr = PetscFree(radau);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilSetFromOptions_Radau"
static PetscErrorCode TSResilSetFromOptions_Radau(PetscOptionItems *PetscOptionsObject,TSResilEstimate est)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscOptionsHead(PetscOptionsObject,"Radau resilive controller options");CHKERRQ(ierr);
  ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilView_Radau"
static PetscErrorCode TSResilView_Radau(TSResilEstimate estimate,PetscViewer viewer)
{
  TSResil_Radau	  *radau = (TSResil_Radau*)estimate->data;
	PetscErrorCode ierr;
  PetscBool      iascii;

  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  if (iascii) {
    ierr = PetscViewerASCIIPrintf(viewer,"  Radau: history %d\n",radau->history);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilCreate_Radau"
/*MC
   TSADAPTBASIC - Radau resilive controller for time stepping

   Level: intermediate

.seealso: TS, TSResil, TSSetResil()
M*/
PETSC_EXTERN PetscErrorCode TSResilCreate_Radau(MPI_Comm comm, TSResilEstimate estimate)
{
  PetscErrorCode ierr;
  TSResil_Radau  *a;

  PetscFunctionBegin;
  CHKMEMQ;
	ierr 											 = PetscClassIdRegister("TSResil_Radau",&TSRESIL_EST_RADAU_CLASSID);CHKERRQ(ierr);
  ierr                       = PetscHeaderCreate(a,TSRESIL_EST_RADAU_CLASSID,"TSResil_Radau","Resilience//Radau module for surrogating","TS",comm,
					TSResilDestroy_Radau,TSResilView_Radau);CHKERRQ(ierr);	
  ierr                       = PetscLogObjectParent((PetscObject)estimate,(PetscObject)a);CHKERRQ(ierr);
  ierr                       = PetscObjectIncrementTabLevel((PetscObject)a,(PetscObject)estimate,1);CHKERRQ(ierr);
	estimate->data             = (void*)a;
	a->history 								 = 0;
  estimate->ops->compute        		 = TSResilCompute_Radau;
  estimate->ops->setfromoptions 		 = TSResilSetFromOptions_Radau;
  estimate->ops->destroy        		 = TSResilDestroy_Radau;
  estimate->ops->view           		 = TSResilView_Radau;
  estimate->ops->update              = TSResilUpdate_Radau;
  a->time_step              = 0.0;
  PetscFunctionReturn(0);
}
