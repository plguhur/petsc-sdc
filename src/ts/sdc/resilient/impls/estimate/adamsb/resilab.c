#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/

typedef struct {
  PETSCHEADER(int);
	Vec 			F1,F2,F3;
  PetscReal h1,h2; /* time_step with which is computed history. might be different from ts->time_step */
	PetscInt  history;   /* length of history */
} TSResil_AdamsB;

static PetscClassId      TSRESIL_EST_ADAMSB_CLASSID;
static PetscErrorCode TSResilAdamsB_1(TSResilEstimate, TS, Vec);
static PetscErrorCode TSResilAdamsB_2(TSResilEstimate, TS, Vec);
static PetscErrorCode TSResilAdamsB_3(TSResilEstimate, TS, Vec);
static PetscErrorCode TSResilAdamsB_4(TSResilEstimate, TS, Vec);



  
#undef __FUNCT__
#define __FUNCT__ "TSResilCompute_AdamsB"
static PetscErrorCode TSResilCompute_AdamsB(TSResilEstimate estimate, TS ts, Vec old_sol)
{
  
  PetscFunctionBegin;
  if(estimate->order == 1) {TSResilAdamsB_1(estimate, ts, old_sol);}
  if(estimate->order == 2) {TSResilAdamsB_2(estimate, ts, old_sol);}
  if(estimate->order == 3) {TSResilAdamsB_3(estimate, ts, old_sol);}
  if(estimate->order == 4) {TSResilAdamsB_4(estimate, ts, old_sol);}
 PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilAdamsB_1"
static PetscErrorCode TSResilAdamsB_1(TSResilEstimate estimate, TS ts, Vec old_sol)
{
  Vec 						*slopes, lte;
	PetscInt 				s;
  PetscReal       h = (PetscReal)ts->time_step;
  PetscErrorCode 	ierr;
	
	PetscFunctionBegin;
  ierr = VecDuplicate(ts->vec_sol,&lte);CHKERRQ(ierr);
	ierr = VecCopy(ts->vec_sol, lte);CHKERRQ(ierr);
	ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr);
	ierr = VecAXPBYPCZ(lte, -1.0,-h,1.0,old_sol,slopes[0]);CHKERRQ(ierr);
	if(estimate->vecForm) {
			ierr = VecCopy(lte,estimate->v_est);CHKERRQ(ierr);
	}
	else {
			ierr  = VecNorm(lte,estimate->norm, &estimate->r_est);CHKERRQ(ierr);
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilAdamsB_2"
static PetscErrorCode TSResilAdamsB_2(TSResilEstimate estimate, TS ts, Vec old_sol)
{
  TSResil_AdamsB	*AdamsB = (TSResil_AdamsB*)estimate->data;
  Vec 						*slopes, lte;
	PetscInt 				s;
  PetscReal       h = (PetscReal)ts->time_step, delta = h/AdamsB->h1, alpha, beta;
  PetscErrorCode 	ierr;
	
	PetscFunctionBegin;
  if (AdamsB->history > 0) {
    ierr = VecDuplicate(ts->vec_sol,&lte);CHKERRQ(ierr);
  	ierr = VecCopy(ts->vec_sol, lte);CHKERRQ(ierr);
  	ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr);
    alpha= -h*(1+0.5*delta);
    beta = +h*0.5*delta;
  	ierr = VecAXPBYPCZ(lte, -1.0, alpha, 1.0, old_sol, slopes[0]);CHKERRQ(ierr);
  	ierr = VecAXPBY(lte, beta, 1.0, AdamsB->F1);CHKERRQ(ierr); 
  	if(estimate->vecForm) {
  			ierr = VecCopy(lte,estimate->v_est);CHKERRQ(ierr);
  	}
  	else {
  			ierr  = VecNorm(lte,estimate->norm, &estimate->r_est);CHKERRQ(ierr);
  	}
	}
	else {
		TSResilAdamsB_1(estimate, ts, old_sol);
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilAdamsB_3"
static PetscErrorCode TSResilAdamsB_3(TSResilEstimate estimate, TS ts, Vec old_sol)
{
  TSResil_AdamsB	*AdamsB = (TSResil_AdamsB*)estimate->data;
  Vec 						*slopes, lte;
	PetscInt 				s;
  PetscReal       h = (PetscReal)ts->time_step, alpha, beta, gamma;
  PetscErrorCode 	ierr;
	
	PetscFunctionBegin;
  if (AdamsB->history > 2) {
    ierr = VecDuplicate(ts->vec_sol,&lte);CHKERRQ(ierr);
  	ierr = VecCopy(ts->vec_sol, lte);CHKERRQ(ierr);
//  	ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr);
    alpha= -h*23.0/12.0;
    beta = +h*4.0/3.0;
    gamma= -h*5.0/12.0;
    ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr); 
  	ierr = VecAXPBYPCZ(lte, -1.0, alpha, 1.0, old_sol, slopes[0]);CHKERRQ(ierr); 
     
  	ierr = VecAXPBYPCZ(lte, beta, gamma, 1.0, AdamsB->F1, AdamsB->F2);CHKERRQ(ierr);
  	if(estimate->vecForm) {
  			ierr = VecCopy(lte,estimate->v_est);CHKERRQ(ierr);
  	}
  	else {
  			ierr  = VecNorm(lte,estimate->norm, &estimate->r_est);CHKERRQ(ierr);
  	}
	}
	else {
		TSResilAdamsB_2(estimate, ts, old_sol);
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilAdamsB_4"
static PetscErrorCode TSResilAdamsB_4(TSResilEstimate estimate, TS ts, Vec old_sol)
{
  TSResil_AdamsB	*AdamsB = (TSResil_AdamsB*)estimate->data;
  Vec 						*slopes, lte;
	PetscInt 				s;
  PetscReal       h = (PetscReal)ts->time_step, alpha, beta, gamma, eta;
  PetscErrorCode 	ierr;
	
	PetscFunctionBegin;
  if (AdamsB->history > 2) {
    ierr = VecDuplicate(ts->vec_sol,&lte);CHKERRQ(ierr);
  	ierr = VecCopy(ts->vec_sol, lte);CHKERRQ(ierr);
//  	ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr);
    alpha= -h*55.0/24.0;
    beta = +h*59.0/24.0;
    gamma= -h*37.0/24.0;
    eta  = +h*3.0/8.0;
    ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr); 
  	ierr = VecAXPBYPCZ(lte, -1.0, alpha, 1.0, old_sol, slopes[0]);CHKERRQ(ierr);
     
  	ierr = VecAXPBYPCZ(lte, beta, gamma, 1.0, AdamsB->F1, AdamsB->F2);CHKERRQ(ierr);
  	ierr = VecAXPBY(lte, eta, 1.0, AdamsB->F3);CHKERRQ(ierr);
  	if(estimate->vecForm) {
  			ierr = VecCopy(lte,estimate->v_est);CHKERRQ(ierr);
  	}
  	else {
  			ierr  = VecNorm(lte,estimate->norm, &estimate->r_est);CHKERRQ(ierr);
  	}
	}
	else {
		TSResilAdamsB_2(estimate, ts, old_sol);
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilUpdate_AdamsB"
PetscErrorCode TSResilUpdate_AdamsB(TSResilEstimate estimate, TS ts, Vec old_sol)
{
  TSResil_AdamsB	        *AdamsB = (TSResil_AdamsB*)estimate->data;
  PetscErrorCode      ierr;
	PetscInt 						s;
	Vec									*slopes;

  PetscFunctionBegin;
	ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr);
	if(estimate->order == 2) {
    if(!AdamsB->F1) { ierr = VecDuplicate(old_sol, &AdamsB->F1);CHKERRQ(ierr);}
		ierr = VecCopy(slopes[0],AdamsB->F1);CHKERRQ(ierr);
	}
	if(estimate->order == 3) {
    if(!AdamsB->F1) { ierr = VecDuplicate(old_sol, &AdamsB->F1);CHKERRQ(ierr);}
    if(!AdamsB->F2) { ierr = VecDuplicate(old_sol, &AdamsB->F2);CHKERRQ(ierr);}
		ierr = VecCopy(AdamsB->F1,AdamsB->F2);CHKERRQ(ierr);
    ierr = VecCopy(slopes[0],AdamsB->F1);CHKERRQ(ierr);
	}
	if(estimate->order == 4) {
    if(!AdamsB->F1) { ierr = VecDuplicate(old_sol, &AdamsB->F1);CHKERRQ(ierr);}
    if(!AdamsB->F2) { ierr = VecDuplicate(old_sol, &AdamsB->F2);CHKERRQ(ierr);}
    if(!AdamsB->F3) { ierr = VecDuplicate(old_sol, &AdamsB->F3);CHKERRQ(ierr);}
		ierr = VecCopy(AdamsB->F2,AdamsB->F3);CHKERRQ(ierr);
    ierr = VecCopy(AdamsB->F1,AdamsB->F2);CHKERRQ(ierr);
    ierr = VecCopy(slopes[0],AdamsB->F1);CHKERRQ(ierr);
	}
  AdamsB->h1 = ts->time_step;
  AdamsB->history += 1;
  PetscFunctionReturn(0);
}


/*----------------------------------------------------------*/

#undef __FUNCT__
#define __FUNCT__ "TSResilReset_AdamsB"
static PetscErrorCode TSResilReset_AdamsB(TSResilEstimate estimate)
{
  TSResil_AdamsB	  *AdamsB = (TSResil_AdamsB*)estimate->data;
	PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = VecDestroy(&AdamsB->F1);CHKERRQ(ierr);
  ierr = VecDestroy(&AdamsB->F2);CHKERRQ(ierr);
  ierr = VecDestroy(&AdamsB->F3);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilDestroy_AdamsB"
static PetscErrorCode TSResilDestroy_AdamsB(TSResilEstimate estimate)
{
  TSResil_AdamsB	  *AdamsB = (TSResil_AdamsB*)estimate->data;
	PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = TSResilReset_AdamsB(estimate);CHKERRQ(ierr);
  ierr = PetscFree(AdamsB);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilSetFromOptions_AdamsB"
static PetscErrorCode TSResilSetFromOptions_AdamsB(PetscOptionItems *PetscOptionsObject,TSResilEstimate est)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscOptionsHead(PetscOptionsObject,"AdamsB estimate controller options");CHKERRQ(ierr);
  ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilView_AdamsB"
static PetscErrorCode TSResilView_AdamsB(TSResilEstimate estimate,PetscViewer viewer)
{
  TSResil_AdamsB	  *AdamsB = (TSResil_AdamsB*)estimate->data;
	PetscErrorCode ierr;
  PetscBool      iascii;

  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  if (iascii) {
    ierr = PetscViewerASCIIPrintf(viewer,"  AdamsB: history %d\n",AdamsB->history);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilCreate_AdamsB"
/*MC
   TSADAPTBASIC - AdamsB resilive controller for time stepping

   Level: intermediate

.seealso: TS, TSResil, TSSetResil()
M*/
PETSC_EXTERN PetscErrorCode TSResilCreate_AdamsB(MPI_Comm comm, TSResilEstimate estimate)
{
  PetscErrorCode ierr;
  TSResil_AdamsB  *a;

  PetscFunctionBegin;
  CHKMEMQ;
	ierr 											 = PetscClassIdRegister("TSResil_AdamsB",&TSRESIL_EST_ADAMSB_CLASSID);CHKERRQ(ierr);
  ierr                       = PetscHeaderCreate(a,TSRESIL_EST_ADAMSB_CLASSID,"TSResil_AdamsB","Resilience//AdamsB module for surrogating","TS",comm,
					TSResilDestroy_AdamsB,TSResilView_AdamsB);CHKERRQ(ierr);	
  ierr                       = PetscLogObjectParent((PetscObject)estimate,(PetscObject)a);CHKERRQ(ierr);
  ierr                       = PetscObjectIncrementTabLevel((PetscObject)a,(PetscObject)estimate,1);CHKERRQ(ierr);
	estimate->data             = (void*)a;
	a->history 								 = 0;
  a->h1                      = 0.0;
  a->h2                      = 0.0;
  estimate->ops->compute        		 = TSResilCompute_AdamsB;
  estimate->ops->setfromoptions 		 = TSResilSetFromOptions_AdamsB;
  estimate->ops->destroy        		 = TSResilDestroy_AdamsB;
  estimate->ops->view           		 = TSResilView_AdamsB;
  estimate->ops->update              = TSResilUpdate_AdamsB;
  PetscFunctionReturn(0);
}
