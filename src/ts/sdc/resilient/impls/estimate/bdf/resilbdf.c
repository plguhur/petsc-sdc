#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/

typedef struct {
  PETSCHEADER(int);
	Vec 			X1,X2,X3;
  PetscReal h1,h2; /* time_step with which is computed history. might be different from ts->time_step */
	PetscInt  history;   /* length of history */
} TSResil_BDF;

static PetscClassId      TSRESIL_EST_BDF_CLASSID;
static PetscErrorCode TSResilBDF_1(TSResilEstimate, TS, Vec);
static PetscErrorCode TSResilBDF_2(TSResilEstimate, TS, Vec);
static PetscErrorCode TSResilBDF_3(TSResilEstimate, TS, Vec);
static PetscErrorCode TSResilBDF_4(TSResilEstimate, TS, Vec);



  
#undef __FUNCT__
#define __FUNCT__ "TSResilCompute_BDF"
static PetscErrorCode TSResilCompute_BDF(TSResilEstimate estimate, TS ts, Vec old_sol)
{
  
  PetscFunctionBegin;
  if(estimate->order == 1) {TSResilBDF_1(estimate, ts, old_sol);}
  if(estimate->order == 2) {TSResilBDF_2(estimate, ts, old_sol);}
  if(estimate->order == 3) {TSResilBDF_3(estimate, ts, old_sol);}
  if(estimate->order == 4) {TSResilBDF_4(estimate, ts, old_sol);}
 PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilBDF_1"
static PetscErrorCode TSResilBDF_1(TSResilEstimate estimate, TS ts, Vec old_sol)
{
  Vec 						*slopes, lte;
	PetscInt 				s;
  PetscReal       h = (PetscReal)ts->time_step;
  PetscErrorCode 	ierr;
	
	PetscFunctionBegin;
  ierr = VecDuplicate(ts->vec_sol,&lte);CHKERRQ(ierr);
	ierr = VecCopy(ts->vec_sol, lte);CHKERRQ(ierr);
	ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr);
	ierr = VecAXPBYPCZ(lte, -1.0,-h,1.0,old_sol,slopes[0]);CHKERRQ(ierr);
	if(estimate->vecForm) {
			ierr = VecCopy(lte,estimate->v_est);CHKERRQ(ierr);
	}
	else {
			ierr  = VecNorm(lte,estimate->norm, &estimate->r_est);CHKERRQ(ierr);
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilBDF_2"
static PetscErrorCode TSResilBDF_2(TSResilEstimate estimate, TS ts, Vec old_sol)
{
  TSResil_BDF	    *bdf = (TSResil_BDF*)estimate->data;
  Vec 						*slopes, lte, nextStage;
	PetscInt 				s;
  PetscReal       h = (PetscReal)ts->time_step, delta = h/bdf->h1, alpha, beta, gamma;
  PetscErrorCode 	ierr;
	
	PetscFunctionBegin;
  if (bdf->history > 0) {
    ierr = VecDuplicate(ts->vec_sol,&lte);CHKERRQ(ierr);
  	ierr = VecCopy(ts->vec_sol, lte);CHKERRQ(ierr);
  	ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr);
    alpha= -PetscPowScalar(1+delta, 2)/(1+2*delta);
    beta = PetscPowScalar(delta, 2)/(1+2*delta);
    gamma= -h*(1+delta)/(1+2*delta);
  	ierr = VecAXPBYPCZ(lte, alpha, beta, 1.0, old_sol, bdf->X1);CHKERRQ(ierr);
    ierr = VecDuplicate(ts->vec_sol,&nextStage);CHKERRQ(ierr);
    ierr = TSComputeRHSFunction(ts, ts->ptime, ts->vec_sol, nextStage);CHKERRQ(ierr);// we dont need to do that if FSAL. otherwise, use it for next step!
  	ierr = VecAXPBY(lte, gamma, 1.0, nextStage);CHKERRQ(ierr); 
  	if(estimate->vecForm) {
  			ierr = VecCopy(lte,estimate->v_est);CHKERRQ(ierr);
  	}
  	else {
  			ierr  = VecNorm(lte,estimate->norm, &estimate->r_est);CHKERRQ(ierr);
  	}
	}
	else {
		TSResilBDF_1(estimate, ts, old_sol);
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilBDF_3"
static PetscErrorCode TSResilBDF_3(TSResilEstimate estimate, TS ts, Vec old_sol)
{
  TSResil_BDF	    *bdf = (TSResil_BDF*)estimate->data;
  Vec 						nextStage, lte;
  PetscReal       h = (PetscReal)ts->time_step, delta1 = h/bdf->h1, delta2 = bdf->h2/bdf->h1, alpha, beta, gamma, eta;
  PetscErrorCode 	ierr;
	
	PetscFunctionBegin;
  if (bdf->history > 1) {
    ierr = VecDuplicate(ts->vec_sol,&lte);CHKERRQ(ierr);
  	ierr = VecCopy(ts->vec_sol, lte);CHKERRQ(ierr);
//  	ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr);
    alpha= -PetscPowScalar(1+delta1, 2)*PetscPowScalar(delta2*(1+delta1)+1, 2)/((1+delta2)*(2*delta1+delta2*(delta1+1)*(3*delta1+1)+1));
    beta = PetscPowScalar(delta1, 2)*PetscPowScalar(delta2*(1+delta1)+1, 2)/(2*delta1+delta2*(delta1+1)*(3*delta1+1)+1);
    gamma= -PetscPowScalar(delta1, 2)*PetscPowScalar(1+delta1, 2)*PetscPowScalar(delta2, 3)/((1+delta2)*(2*delta1+delta2*(delta1+1)*(3*delta1+1)+1));
    eta  = -h*(1+delta1)*(delta1*delta2+delta2+1)/(3*delta2*PetscPowScalar(delta1, 2)+4*delta2*delta1+2*delta1+delta2+1);
  	ierr = VecAXPBYPCZ(lte, alpha, beta, 1.0, old_sol, bdf->X1);CHKERRQ(ierr);
    ierr = VecDuplicate(ts->vec_sol,&nextStage);CHKERRQ(ierr);    
    ierr = TSComputeRHSFunction(ts, ts->ptime, ts->vec_sol, nextStage);CHKERRQ(ierr);// we dont need to do that if FSAL. otherwise, use it for next step!
  	ierr = VecAXPBYPCZ(lte, gamma, eta, 1.0, bdf->X2, nextStage);CHKERRQ(ierr);
  	if(estimate->vecForm) {
  			ierr = VecCopy(lte,estimate->v_est);CHKERRQ(ierr);
  	}
  	else {
  			ierr  = VecNorm(lte,estimate->norm, &estimate->r_est);CHKERRQ(ierr);
  	}
	}
	else {
		TSResilBDF_2(estimate, ts, old_sol);
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilBDF_4"
static PetscErrorCode TSResilBDF_4(TSResilEstimate estimate, TS ts, Vec old_sol)
{
  TSResil_BDF	    *bdf = (TSResil_BDF*)estimate->data;
  Vec 						nextStage, lte;
  PetscReal       h = (PetscReal)ts->time_step, alpha, beta, gamma, eta, nu;
  PetscErrorCode 	ierr;
	
	PetscFunctionBegin;
  if (bdf->history > 2) {
    ierr = VecDuplicate(ts->vec_sol,&lte);CHKERRQ(ierr);
  	ierr = VecCopy(ts->vec_sol, lte);CHKERRQ(ierr);
//  	ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr);
    alpha= -48.0/25.0;
    beta = 36.0/25.0;
    gamma= -16.0/25.0;
    eta  = 3.0/25.0;
    nu   = -h*12.0/25.0;
  	ierr = VecAXPBYPCZ(lte, alpha, beta, 1.0, old_sol, bdf->X1);CHKERRQ(ierr);
    ierr = VecDuplicate(ts->vec_sol,&nextStage);CHKERRQ(ierr);    
    ierr = TSComputeRHSFunction(ts, ts->ptime, ts->vec_sol, nextStage);CHKERRQ(ierr);// we dont need to do that if FSAL. otherwise, use it for next step!
  	ierr = VecAXPBYPCZ(lte, gamma, eta, 1.0, bdf->X2, bdf->X3);CHKERRQ(ierr);
    ierr = VecAXPBY(lte, nu, 1.0, nextStage);CHKERRQ(ierr); 
  	if(estimate->vecForm) {
  			ierr = VecCopy(lte,estimate->v_est);CHKERRQ(ierr);
  	}
  	else {
  			ierr  = VecNorm(lte,estimate->norm, &estimate->r_est);CHKERRQ(ierr);
  	}
	}
	else {
		TSResilBDF_2(estimate, ts, old_sol);
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilUpdate_BDF"
PetscErrorCode TSResilUpdate_BDF(TSResilEstimate estimate, TS ts, Vec old_sol)
{
  TSResil_BDF	        *bdf = (TSResil_BDF*)estimate->data;
  PetscErrorCode      ierr;
	PetscInt 						s;
	Vec									*slopes;

  PetscFunctionBegin;
	ierr = TSGetStageSlopes(ts, &s, &slopes);CHKERRQ(ierr);
	if(estimate->order == 2) {
    if(!bdf->X1) { ierr = VecDuplicate(old_sol, &bdf->X1);CHKERRQ(ierr);}
		ierr = VecCopy(old_sol,bdf->X1);CHKERRQ(ierr);
	}
	if(estimate->order == 3) {
    if(!bdf->X2) { ierr = VecDuplicate(old_sol, &bdf->X2);CHKERRQ(ierr);}
    if(!bdf->X1) { ierr = VecDuplicate(old_sol, &bdf->X1);CHKERRQ(ierr);}
    ierr = VecCopy(bdf->X1,bdf->X2);CHKERRQ(ierr);
		ierr = VecCopy(old_sol,bdf->X1);CHKERRQ(ierr);
    bdf->h2 = bdf->h1;
	}
	if(estimate->order == 4) {
    if(!bdf->X3) { ierr = VecDuplicate(old_sol, &bdf->X3);CHKERRQ(ierr);}
    if(!bdf->X2) { ierr = VecDuplicate(old_sol, &bdf->X2);CHKERRQ(ierr);}
    if(!bdf->X1) { ierr = VecDuplicate(old_sol, &bdf->X1);CHKERRQ(ierr);}
    ierr = VecCopy(bdf->X2,bdf->X3);CHKERRQ(ierr);
    ierr = VecCopy(bdf->X1,bdf->X2);CHKERRQ(ierr);
		ierr = VecCopy(old_sol,bdf->X1);CHKERRQ(ierr);
	}
  bdf->h1 = ts->time_step;
  bdf->history += 1;
  PetscFunctionReturn(0);
}


/*----------------------------------------------------------*/

#undef __FUNCT__
#define __FUNCT__ "TSResilReset_BDF"
static PetscErrorCode TSResilReset_BDF(TSResilEstimate estimate)
{
  TSResil_BDF	  *bdf = (TSResil_BDF*)estimate->data;
	PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = VecDestroy(&bdf->X3);CHKERRQ(ierr);
  ierr = VecDestroy(&bdf->X1);CHKERRQ(ierr);
  ierr = VecDestroy(&bdf->X2);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilDestroy_BDF"
static PetscErrorCode TSResilDestroy_BDF(TSResilEstimate estimate)
{
  TSResil_BDF	  *bdf = (TSResil_BDF*)estimate->data;
	PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = TSResilReset_BDF(estimate);CHKERRQ(ierr);
  ierr = PetscFree(bdf);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilSetFromOptions_BDF"
static PetscErrorCode TSResilSetFromOptions_BDF(PetscOptionItems *PetscOptionsObject,TSResilEstimate est)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscOptionsHead(PetscOptionsObject,"BDF estimate controller options");CHKERRQ(ierr);
  ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilView_BDF"
static PetscErrorCode TSResilView_BDF(TSResilEstimate estimate,PetscViewer viewer)
{
  TSResil_BDF	  *bdf = (TSResil_BDF*)estimate->data;
	PetscErrorCode ierr;
  PetscBool      iascii;

  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  if (iascii) {
    ierr = PetscViewerASCIIPrintf(viewer,"  BDF: history %d\n",bdf->history);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilCreate_BDF"
/*MC
   TSADAPTBASIC - BDF resilive controller for time stepping

   Level: intermediate

.seealso: TS, TSResil, TSSetResil()
M*/
PETSC_EXTERN PetscErrorCode TSResilCreate_BDF(MPI_Comm comm, TSResilEstimate estimate)
{
  PetscErrorCode ierr;
  TSResil_BDF  *a;

  PetscFunctionBegin;
  CHKMEMQ;
	ierr 											 = PetscClassIdRegister("TSResil_BDF",&TSRESIL_EST_BDF_CLASSID);CHKERRQ(ierr);
  ierr                       = PetscHeaderCreate(a,TSRESIL_EST_BDF_CLASSID,"TSResil_BDF","Resilience//BDF module for surrogating","TS",comm,
					TSResilDestroy_BDF,TSResilView_BDF);CHKERRQ(ierr);	
  ierr                       = PetscLogObjectParent((PetscObject)estimate,(PetscObject)a);CHKERRQ(ierr);
  ierr                       = PetscObjectIncrementTabLevel((PetscObject)a,(PetscObject)estimate,1);CHKERRQ(ierr);
	estimate->data             = (void*)a;
	a->history 								 = 0;
  a->h1                      = 0.0;
  a->h2                      = 0.0;
  estimate->ops->compute        		 = TSResilCompute_BDF;
  estimate->ops->setfromoptions 		 = TSResilSetFromOptions_BDF;
  estimate->ops->destroy        		 = TSResilDestroy_BDF;
  estimate->ops->view           		 = TSResilView_BDF;
  estimate->ops->update              = TSResilUpdate_BDF;
  PetscFunctionReturn(0);
}
