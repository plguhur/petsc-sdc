#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/
#include <petscmath.h>


typedef struct {
  PETSCHEADER(int);
  PetscReal h; /* time_step with which is computed history. might be different from ts->time_step */
} TSResil_Last;

static PetscClassId      TSRESIL_EST_LAST_CLASSID;





  
#undef __FUNCT__
#define __FUNCT__ "TSResilCompute_Last"
static PetscErrorCode TSResilCompute_Last(TSResilEstimate estimate, TS ts, Vec old_sol)
{
  TSResil_Last	  *last = (TSResil_Last*)estimate->data;
  PetscReal       delta = (PetscReal)ts->time_step/last->h, coeff;
  PetscInt        p = estimate->order;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  coeff = PetscPowReal(delta, p);
	if(estimate->vecForm) {
			ierr = VecScale(estimate->v_est,coeff);CHKERRQ(ierr);
	}
	else {
			estimate->r_est *= coeff;
	}
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilUpdate_Last"
PetscErrorCode TSResilUpdate_Last(TSResilEstimate estimate, TS ts, Vec old_sol)
{
  TSResil_Last	      *last = (TSResil_Last*)estimate->data;
 
  PetscFunctionBegin;
  last->h = ts->time_step;
  PetscFunctionReturn(0);
}


/*----------------------------------------------------------*/

#undef __FUNCT__
#define __FUNCT__ "TSResilDestroy_Last"
static PetscErrorCode TSResilDestroy_Last(TSResilEstimate estimate)
{
  TSResil_Last	  *last = (TSResil_Last*)estimate->data;
	PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscFree(last);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilCreate_Last"
/*MC
   TSADAPTBASIC - Last resilive controller for time stepping

   Level: intermediate

.seealso: TS, TSResil, TSSetResil()
M*/
PETSC_EXTERN PetscErrorCode TSResilCreate_Last(MPI_Comm comm, TSResilEstimate estimate)
{
  PetscErrorCode ierr;
  TSResil_Last  *a;

  PetscFunctionBegin;
  CHKMEMQ;
	ierr 											 = PetscClassIdRegister("TSResil_Last",&TSRESIL_EST_LAST_CLASSID);CHKERRQ(ierr);
  ierr                       = PetscHeaderCreate(a,TSRESIL_EST_LAST_CLASSID,"TSResil_Last","Resilience//Last module for surrogating","TS",comm,
					TSResilDestroy_Last,NULL);CHKERRQ(ierr);	
  ierr                       = PetscLogObjectParent((PetscObject)estimate,(PetscObject)a);CHKERRQ(ierr);
  ierr                       = PetscObjectIncrementTabLevel((PetscObject)a,(PetscObject)estimate,1);CHKERRQ(ierr);
	estimate->data             = (void*)a;
  a->h                       = 0.0;
  estimate->ops->compute 		 = TSResilCompute_Last;
  estimate->ops->update      = TSResilUpdate_Last;
  PetscFunctionReturn(0);
}
