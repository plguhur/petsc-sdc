#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/


#undef __FUNCT__
#define __FUNCT__ "TSResilConfidence_Epsilon"
static PetscErrorCode TSResilConfidence_Epsilon(TSResil resil,TS ts,PetscReal *conf)
{

  PetscFunctionBegin;
//  VecSet(*conf, PETSC_MACHINE_EPSILON);
  *conf = PETSC_MACHINE_EPSILON;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilReset_Epsilon"
static PetscErrorCode TSResilReset_Epsilon(TSResil resil)
{
  PetscFunctionBegin;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilDestroy_Epsilon"
static PetscErrorCode TSResilDestroy_Epsilon(TSResil resil)
{
  PetscFunctionBegin;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilSetFromOptions_Epsilon"
static PetscErrorCode TSResilSetFromOptions_Epsilon(PetscOptions *PetscOptionsObject,TSResil resil)
{
  
  PetscFunctionBegin;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilView_Epsilon"
static PetscErrorCode TSResilView_Epsilon(TSResil resil,PetscViewer viewer)
{
  TSResil_Basic  *basic = (TSResil_Basic*)resil->data;
  PetscErrorCode ierr;
  PetscBool      iascii;

  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  if (iascii) {
    ierr = PetscViewerASCIIPrintf(viewer,"  Epsilon: threshold %.15e",PETSC_MACHINE_EPSILON);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilCreate_Epsilon"
/*MC
   TSADAPTBASIC - Basic resilive controller for time stepping

   Level: intermediate

.seealso: TS, TSResil, TSSetResil()
M*/
PETSC_EXTERN PetscErrorCode TSResilCreate_Basic(TSResil resil)
{

  PetscFunctionBegin;
  PetscFunctionReturn(0);
}
