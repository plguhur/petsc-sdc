#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/

typedef struct {
  PETSCHEADER(struct _TSResilOps);
  PetscReal tau_v,tau_j,gamma,Gamma, old_sf[10];
  PetscInt  p; 
  PetscBool lastAccept;            
} TSResil_Feedback;

static PetscClassId      TSRESILFB_CLASSID;
static PetscClassId      TSRESIL_FB_MON_CLASSID;


static PetscReal ComputeRelDiff(PetscReal o, PetscReal s){
  return (s-o)/o;
}

static PetscReal ComputeChgVar(PetscReal o[10], PetscReal s){
  PetscReal a = o[9]*o[9], b = s;
  PetscInt  i = 0;
  for(; i<9; i++) {
    a += o[i]*o[i];
    b += o[i]*o[i];
  }
  return a/b;
}


#undef __FUNCT__
#define __FUNCT__ "TSResilCheck_Feedback"
static PetscErrorCode TSResilCheck_Feedback(TSResil resil,TS ts,PetscBool *check)
{
  TSResilCheck  chk = resil->check;
  TSResil_Feedback    *fb = (TSResil_Feedback*)chk->data;
	TSResilSurrogate	  surrogate = (TSResilSurrogate)resil->surrogate;  
  PetscReal relDiff, chgVar, sf = surrogate->r_sf;

  PetscFunctionBegin;
  relDiff = ComputeRelDiff(fb->old_sf[0], sf);
  chgVar = ComputeChgVar(fb->old_sf, sf);
  *check = (relDiff <= fb->tau_j ||  chgVar <= fb->tau_v) ? PETSC_TRUE : PETSC_FALSE;
  if(*check) {
  	if (relDiff > fb->tau_j) { 
      fb->tau_j *= fb->Gamma;
    } else { 
      fb->tau_j *= fb->gamma;
    }
  	if (chgVar > fb->tau_v) { 
      fb->tau_v *= fb->Gamma;
    } else { 
      fb->tau_v *= fb->gamma;
    }
  }
  //fb->lastAccept = *check;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilMonitor_Feedback"
static PetscErrorCode TSResilMonitor_Feedback(TSResil resil, PetscInt steps, PetscReal curtime,  Vec sol, PetscViewer viewer)
{
	TSResilCheck  check = resil->check;
  TSResil_Feedback  *fb = (TSResil_Feedback*)check->data;
	TSResilSurrogate	  surrogate = (TSResilSurrogate)resil->surrogate;  
  PetscReal relDiff, chgVar, sf = surrogate->r_sf;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  relDiff = ComputeRelDiff(fb->old_sf[0], sf);
  chgVar = ComputeChgVar(fb->old_sf, sf);
  ierr = PetscViewerASCIIPrintf(viewer,"%i, %g, %g, %g, %g, %g\n",steps,curtime, fb->tau_j, relDiff, fb->tau_v, chgVar);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilSetFromOptions_Feedback"
static PetscErrorCode TSResilSetFromOptions_Feedback(PetscOptionItems *PetscOptionsObject,TSResil resil)
{
  TSResilCheck  check = resil->check;
  TSResil_Feedback  *fb = (TSResil_Feedback*)check->data;
  PetscBool           flg = PETSC_FALSE;
  TSResilObjMonitor   rmon = NULL;
  PetscErrorCode ierr; 
  
  PetscFunctionBegin;
  ierr = PetscOptionsHead(PetscOptionsObject,"Feedback check for Resilience:: options");CHKERRQ(ierr);
  ierr = PetscOptionsReal("-ts_resil_fb_tau_j","Tau_j","",fb->tau_j,&fb->tau_j,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-ts_resil_fb_tau_v","Tau_v","",fb->tau_v,&fb->tau_v,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-ts_resil_fb_up","Gamma","",fb->Gamma,&fb->Gamma,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-ts_resil_fb_down","gamma","",fb->gamma,&fb->gamma,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-ts_resil_monitor_check","Monitor check","",PETSC_FALSE,&flg,NULL);CHKERRQ(ierr);
  if(flg) { 
    ierr                       = PetscClassIdRegister("TSResilObjMonitor",&TSRESIL_FB_MON_CLASSID);CHKERRQ(ierr);
    ierr                       = PetscHeaderCreate(rmon,TSRESIL_FB_MON_CLASSID,"TSResilObjMonitor","Resilience/Feedback monitor","TS",
                                  PetscObjectComm((PetscObject)resil), 0,0);CHKERRQ(ierr);	
    rmon->monitor = TSResilMonitor_Feedback;   
    ierr = PetscStrcpy(rmon->name, "check.txt");CHKERRQ(ierr);
    ierr = TSResilAddASCIIMonitor(resil, rmon);CHKERRQ(ierr);
  }    
	ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilUpdate_Feedback"
static PetscErrorCode TSResilUpdate_Feedback(TSResil resil, TS ts, Vec o)
{
  TSResilCheck  check = resil->check;
  TSResil_Feedback  *fb = (TSResil_Feedback*)check->data;
	TSResilSurrogate	  sf = (TSResilSurrogate)resil->surrogate;  
  
  PetscFunctionBegin;
  fb->old_sf[9] = fb->old_sf[8]; fb->old_sf[8] = fb->old_sf[7]; fb->old_sf[7] = fb->old_sf[6]; fb->old_sf[6] = fb->old_sf[5]; 
  fb->old_sf[5] = fb->old_sf[4]; fb->old_sf[4] = fb->old_sf[3];
  fb->old_sf[3] = fb->old_sf[2]; fb->old_sf[2] = fb->old_sf[1]; fb->old_sf[1] = fb->old_sf[0]; fb->old_sf[0] = sf->r_sf;
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilView_Feedback"
static PetscErrorCode TSResilView_Feedback(TSResil resil,PetscViewer viewer)
{
  TSResilCheck  check = resil->check;
  TSResil_Feedback  *fb = (TSResil_Feedback*)check->data;
  PetscErrorCode ierr;
  PetscBool      iascii;

  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  if (iascii) {
    ierr = PetscViewerASCIIPrintf(viewer,"  Feedback: tau_j %.15e, tau_v %.15e",fb->tau_j,fb->tau_v);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilDestroy_Feedback"
static PetscErrorCode TSResilDestroy_Feedback(TSResil resil)
{
  TSResilCheck          chk = resil->check;
  TSResil_Feedback       *  a = (TSResil_Feedback*)(chk->data);
  PetscErrorCode        ierr;

  PetscFunctionBegin;
  ierr = PetscHeaderDestroy(&a);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilCreate_Feedback"
/*MC
   TSADAPTBASIC - Feedback resilive controller for time stepping

   Level: intermediate

.seealso: TS, TSResil, TSSetResil()
M*/
PETSC_EXTERN PetscErrorCode TSResilCreate_Feedback(MPI_Comm comm,TSResil resil)
{
	PetscErrorCode ierr;
	TSResil_Feedback    *a;
  TSResilCheck  check = resil->check;

	PetscFunctionBegin;
  ierr                       = PetscClassIdRegister("TSResil_Feedback",&TSRESILFB_CLASSID);CHKERRQ(ierr);
  ierr                       = PetscHeaderCreate(a,TSRESILFB_CLASSID,"TSResil_Feedback","Resilience//Feedback module for surrogating","TS",comm,
                                  NULL,TSResilView_Feedback);CHKERRQ(ierr);	
  ierr                       = PetscLogObjectParent((PetscObject)check,(PetscObject)a);CHKERRQ(ierr); 
  ierr                       = PetscObjectIncrementTabLevel((PetscObject)a,(PetscObject)check,1);CHKERRQ(ierr);
	check->data 							 = (void*)a;
	a->tau_v = 0.0001;
	a->tau_j = 0.001;             
	a->gamma = 0.95;
	a->Gamma = 1.4;
	a->p     = 10;
  check->ops->check              = TSResilCheck_Feedback;
  check->ops->update             = TSResilUpdate_Feedback;
  check->ops->setfromoptions 		 = TSResilSetFromOptions_Feedback;
  check->ops->destroy        		 = TSResilDestroy_Feedback;
  check->ops->view           		 = TSResilView_Feedback;
	PetscFunctionReturn(0);
}
