#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/

PetscLogEvent  AID_EVENT;

typedef struct {
  PETSCHEADER(struct _TSResilOps);
  PetscReal alpha,maxErrorBound,maxErrorBound2, threshold, predError, h1,h2,h3;
	PetscInt K_resil, adapt_cmpt, adapt_pred, sampling_dist, history;
  Vec       X1,X2,X3,X4,Y;
  PetscBool       sampling, fixed;
  PetscErrorCode  (*predictors[3]) (TSResil, Vec X1, Vec X2, Vec X3, Vec Y) ;
  PetscErrorCode  (*predictor) (TSResil, Vec X1, Vec X2, Vec X3, Vec Y) ;
} TSResil_ImpactDriven;

static PetscClassId      TSRESILID_CLASSID;
static PetscClassId     TSRESIL_ID_MON_CLASSID;
//enum Predictors {Last, Linear, Quadratic}

#undef __FUNCT__
#define __FUNCT__ "TSImpactDriven_Sampling"
static PetscErrorCode TSImpactDriven_Sampling(TSResil resil,Vec input, Vec *output)
{
  TSResilCheck  chk = resil->check;
  TSResil_ImpactDriven    *id = (TSResil_ImpactDriven*)chk->data;
  PetscErrorCode ierr;
  PetscReal               *v1, *v2;
	PetscInt size, i,d;
	
  PetscFunctionBegin;
  d = id->sampling_dist;
  //if(!*output) {ierr = VecDuplicate(input, output);CHKERRQ(ierr);}
  ierr = VecGetSize(input, &size);CHKERRQ(ierr);
  size = (PetscInt)PetscFmodReal((PetscReal)size, (PetscReal)d) + PetscFloorReal((PetscReal)size/(PetscReal)d);
  ierr = VecSetSizes(*output, PETSC_DECIDE, size);CHKERRQ(ierr);
  ierr = VecGetArray(input, &v1);CHKERRQ(ierr);
  ierr = VecGetArray(*output, &v2);CHKERRQ(ierr);
  for (i = 0; i<size; i++) {
    v1[i] = v2[id->sampling_dist*i];
  }
  ierr = VecRestoreArray(input, &v1);CHKERRQ(ierr);
  ierr = VecRestoreArray(*output, &v2);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilCheck_ImpactDriven"
static PetscErrorCode TSResilCheck_ImpactDriven(TSResil resil,TS ts,PetscBool *check)
{
  TSResilCheck  chk = resil->check;
  TSResil_ImpactDriven    *id = (TSResil_ImpactDriven*)chk->data;
	Vec sf;
  PetscErrorCode ierr;
	PetscReal ci, enorm;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(AID_EVENT, 0,0,0,0);CHKERRQ(ierr);
  id->h1 = ts->time_step;
	if(id->adapt_pred > 0) {
		if(id->adapt_cmpt == resil->adapt_pred) {
			ierr = TSResilBestPredictor(resil,ts);CHKERRQ(ierr);
			id->adapt_cmpt = 0;
		}
		else {
			id->adapt_cmpt += 1;
		}
	}
	*check = PETSC_TRUE; 
  if(id->sampling) {  ierr = TSImpactDriven_Sampling(resil, ts->vec_sol, &sf);CHKERRQ(ierr); }
  else             {  sf = ts->vec_sol;}
  if(!id->Y) {
    ierr = VecDuplicate(sf, &id->Y);CHKERRQ(ierr);
  }  
  if(id->history < 4) {  PetscFunctionReturn(0);}
  ierr = (*id->predictor)(resil, id->X1,id->X2,id->X3, id->Y);CHKERRQ(ierr);
	ci = id->threshold*(id->predError + id->maxErrorBound);
  ierr = VecAXPY(id->Y, -1.0, sf);CHKERRQ(ierr);
  ierr = VecNorm(id->Y, NORM_INFINITY, &enorm);CHKERRQ(ierr);
	*check &= enorm < ci;
  if(*check) {   ierr = PetscInfo(resil, "Accepted step\n");CHKERRQ(ierr);}
  else       {   ierr = PetscInfo2(resil, "Rejected step: %g > %g\n", enorm, ci);CHKERRQ(ierr);}
  ierr = PetscLogEventEnd(AID_EVENT, 0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilCompute_Last"
static PetscErrorCode TSResilCompute_Last(TSResil resil, Vec X1, Vec X2, Vec X3, Vec Y) {
  PetscErrorCode ierr;
    
  PetscFunctionBegin;
  ierr = VecCopy(X1,Y);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilCompute_Linear"
static PetscErrorCode TSResilCompute_Linear(TSResil resil, Vec X1, Vec X2, Vec X3, Vec Y) {
  TSResilCheck  chk = resil->check;
  TSResil_ImpactDriven    *id = (TSResil_ImpactDriven*)chk->data;
  PetscReal      delta1, delta2;
  PetscErrorCode ierr;
    
  PetscFunctionBegin;
  if(!id->fixed) {
    delta1 = (id->h1+id->h2)/id->h2, delta2 = -id->h1/id->h2;
    ierr = VecWAXPY(Y, delta1/delta2, X1, X2);CHKERRQ(ierr);
    ierr = VecScale(Y, delta2);CHKERRQ(ierr);
  } else { /* 2X1 -  X2 */
    ierr = VecWAXPY(Y, -2.0, X1, X2);CHKERRQ(ierr);
    ierr = VecScale(Y, -1.0);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilCompute_Quadratic"
static PetscErrorCode TSResilCompute_Quadratic(TSResil resil, Vec X1, Vec X2, Vec X3, Vec Y) {
  TSResilCheck  chk = resil->check;
  TSResil_ImpactDriven    *id = (TSResil_ImpactDriven*)chk->data;
  PetscReal      delta1, delta2, delta3;
  PetscErrorCode ierr;
    
  PetscFunctionBegin;
  if(!id->fixed) {
    delta1 = (id->h1+id->h2)*(id->h1+id->h2+id->h3)/(id->h2*(id->h2+id->h3));
    delta2 = -(id->h1)*(id->h1+id->h2+id->h3)/(id->h2*id->h3);
    delta3 = (id->h1)*(id->h1+id->h2)/((id->h2+id->h3)*id->h3);
    ierr = VecWAXPY(Y, delta1/delta2, X1, X2);CHKERRQ(ierr);
    ierr = VecAXPBY(Y, delta3, delta2, X3);CHKERRQ(ierr);
  } else { /* 3X1 -  3X2 + X3*/
    ierr = VecWAXPY(Y, 3.0, X1, X3);CHKERRQ(ierr);
    ierr = VecAXPY(Y, -3.0, X2);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "TSResilUpdate_ImpactDriven"
PetscErrorCode TSResilUpdate_ImpactDriven(TSResil resil,TS ts, Vec old_sol)
{
  TSResilCheck  check = resil->check;
  TSResil_ImpactDriven    *id = (TSResil_ImpactDriven*)check->data;
  PetscErrorCode          ierr;
  
  PetscFunctionBegin;
  //sampling: might be optimized by using lower-level function
  if(id->sampling) {
    ierr = TSImpactDriven_Sampling(resil, ts->vec_sol, &id->Y);CHKERRQ(ierr);
  }
  else { ierr = VecCopy(ts->vec_sol, id->Y);CHKERRQ(ierr);}
  //update memory
  if(!id->X4) { 
    ierr = VecDuplicate(id->Y, &id->X1);CHKERRQ(ierr);
    ierr = VecDuplicate(id->Y, &id->X2);CHKERRQ(ierr);
    ierr = VecDuplicate(id->Y, &id->X3);CHKERRQ(ierr);
    ierr = VecDuplicate(id->Y, &id->X4);CHKERRQ(ierr);
  }
	ierr = VecCopy(id->X3,id->X4);CHKERRQ(ierr);
	ierr = VecCopy(id->X2,id->X3);CHKERRQ(ierr);
	ierr = VecCopy(id->X1,id->X2);CHKERRQ(ierr);
  ierr = VecCopy(id->Y,id->X1);CHKERRQ(ierr);

  id->h3 = id->h2;  
  id->h2 = id->h1;
  id->history++;
  id->threshold   = 1 + resil->Nfp;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilBestPredictor"
PetscErrorCode TSResilBestPredictor(TSResil resil,TS ts)
{
  PetscReal               locPredErr[3], mag, err, minErr = 0.0;
  PetscBool               gammaTest[3], gammaTest2[3], gammaTotal = PETSC_FALSE, gammaTotal2 = PETSC_FALSE, test = PETSC_TRUE;
  PetscInt                i;
  TSResilCheck  check = resil->check;
  TSResil_ImpactDriven    *id = (TSResil_ImpactDriven*)check->data;         
  PetscErrorCode          ierr;
  
  PetscFunctionBegin;
	for (i = 0; i < 3; i++ )
	{
		ierr = (*id->predictors[i])(resil, id->X2,id->X3,id->X4, id->Y);CHKERRQ(ierr);
		mag = 0.0;
		test = PETSC_FALSE;
    ierr = VecAYPX(id->Y, -1.0, id->X1);
    ierr = VecNorm(id->Y, NORM_INFINITY, &err);
    gammaTest[i] = (err < id->maxErrorBound);
    gammaTotal |= gammaTest[i];
		locPredErr[i] = err;
	}

	if(gammaTotal) {
		gammaTotal = PETSC_FALSE;
  	for (i = 0; i < 3; i++ )
  	{
	   		gammaTest2[i] = (err < id->maxErrorBound2);
				gammaTotal2 |= gammaTest2[i];
		}
		
		if(gammaTotal2) {
			for ( i = 0; i < 3; i++ ) {
				if(gammaTest[i]) {
					id->predictor = id->predictors[i];
          break;
        }
			}
		}
		else {
      minErr = id->maxErrorBound;
    	for (i = 0; i < 3; i++ )
    	{
  	   		if(locPredErr[i] < minErr) {
            id->predictor = id->predictors[i];
            minErr = locPredErr[i];
  	   		} 
  		}
		}
	}
	
  else {
    id->predictor = id->predictors[0];
    minErr = locPredErr[0];
  	for (i = 1; i < 3; i++ )
  	{
	   		if(locPredErr[i] < minErr) {
          id->predictor = id->predictors[i];
          minErr = locPredErr[i];
	   		} 
		}
    
  }

  PetscFunctionReturn(0);
}

/*--------------------------------------------------------------------*/

#undef __FUNCT__
#define __FUNCT__ "TSResilReset_ImpactDriven"
static PetscErrorCode TSResilReset_ImpactDriven(TSResil resil)
{
  PetscErrorCode ierr;
  TSResilCheck  check = resil->check;  
  TSResil_ImpactDriven    *a = (TSResil_ImpactDriven*)check->data;
  
	PetscFunctionBegin;
  ierr = VecDestroy(&a->Y);CHKERRQ(ierr);
  ierr = VecDestroy(&a->X1);CHKERRQ(ierr);
  ierr = VecDestroy(&a->X2);CHKERRQ(ierr);
  ierr = VecDestroy(&a->X3);CHKERRQ(ierr);
  ierr = VecDestroy(&a->X4);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilDestroy_ImpactDriven"
static PetscErrorCode TSResilDestroy_ImpactDriven(TSResil resil)
{
  TSResilCheck  check = resil->check;
  TSResil_ImpactDriven    *id = (TSResil_ImpactDriven*)check->data;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = TSResilReset_ImpactDriven(resil);CHKERRQ(ierr);
  ierr = PetscHeaderDestroy(&id);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilMonitor_ImpactDriven"
static PetscErrorCode TSResilMonitor_ImpactDriven(TSResil resil, PetscInt steps, PetscReal curtime,  Vec sol, PetscViewer viewer)
{
	TSResilCheck  check = resil->check;
  TSResil_ImpactDriven    *id = (TSResil_ImpactDriven*)check->data;
  PetscErrorCode 			ierr;
  PetscReal           n1, n2, ci;

  PetscFunctionBegin;
  if(id->history < 4) {  
    ierr = PetscViewerASCIIPrintf(viewer,"%i, %g, %g, %g, %g\n",steps,curtime, 0.0, 0.0,0.0);CHKERRQ(ierr);
    PetscFunctionReturn(0);
  }
    
  //ierr = VecDuplicate(id->X1, &extrapol);CHKERRQ(ierr);
  ci = id->threshold*(id->predError + id->maxErrorBound);
  ierr = (*id->predictor)(resil, id->X1,id->X2,id->X3, id->Y);CHKERRQ(ierr);
  ierr = VecNorm(id->Y, NORM_2, &n1);
  ierr = VecNorm(sol, NORM_2, &n2);
  ierr = PetscViewerASCIIPrintf(viewer,"%i, %g, %g, %g, %g\n",steps,curtime, ci, n1, n2);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilSetFromOptions_ImpactDriven"
static PetscErrorCode TSResilSetFromOptions_ImpactDriven(PetscOptionItems *PetscOptionsObject,TSResil resil)
{
  TSResilCheck  check = resil->check;
  TSResil_ImpactDriven    *id = (TSResil_ImpactDriven*)check->data;
  PetscBool           flg = PETSC_FALSE;
  TSResilObjMonitor   rmon = NULL;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscOptionsHead(PetscOptionsObject,"ImpactDriven resilience controller options");CHKERRQ(ierr);
  ierr = PetscOptionsReal("-ts_resil_id_alpha","Learning rate alpha","",id->alpha,&id->alpha,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-ts_resil_id_maxerrorbound","Max error bound","",id->maxErrorBound,&id->maxErrorBound,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-ts_resil_id_sampling","Sampling (not implemented)","",id->sampling,&id->sampling,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-ts_resil_id_fixed","Fixed step size","",id->fixed,&id->fixed,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-ts_resil_monitor_check","Monitor check","",PETSC_FALSE,&flg,NULL);CHKERRQ(ierr);
  if(flg) { 
    ierr                       = PetscClassIdRegister("TSResilObjMonitor",&TSRESIL_ID_MON_CLASSID);CHKERRQ(ierr);
    ierr                       = PetscHeaderCreate(rmon,TSRESIL_ID_MON_CLASSID,"TSResilObjMonitor","Resilience//Online monitor","TS",
                                  PetscObjectComm((PetscObject)resil), 0,0);CHKERRQ(ierr);	
    rmon->monitor = TSResilMonitor_ImpactDriven;   
    ierr = PetscStrcpy(rmon->name, "check.txt");CHKERRQ(ierr);
    ierr = TSResilAddASCIIMonitor(resil, rmon);CHKERRQ(ierr);
  }    
  ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilView_ImpactDriven"
static PetscErrorCode TSResilView_ImpactDriven(TSResil resil,PetscViewer viewer)
{
  TSResilCheck  check = resil->check;
  TSResil_ImpactDriven  *id = (TSResil_ImpactDriven*)check->data;
  PetscErrorCode ierr;
  PetscBool      iascii;

  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  if (iascii) {
    ierr = PetscViewerASCIIPrintf(viewer,"  ImpactDriven: K_resil %d,  threshold %.15e",id->K_resil,id->threshold);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilCreate_ImpactDriven"
/*MC
   TSADAPTBASIC - ImpactDriven resilive controller for time stepping

   Level: intermediate

.seealso: TS, TSResil, TSSetResil()
M*/
PETSC_EXTERN PetscErrorCode TSResilCreate_ImpactDriven(MPI_Comm comm,TSResil resil)
{
	PetscErrorCode ierr;
	TSResil_ImpactDriven    *a;
  TSResilCheck  check = resil->check;
  
	PetscFunctionBegin;
  ierr                       = PetscClassIdRegister("TSResil_ImpactDriven",&TSRESILID_CLASSID);CHKERRQ(ierr);
  ierr                       = PetscHeaderCreate(a,TSRESILID_CLASSID,"TSResil_ImpactDriven","Resilience//AID module for surrogating","TS",comm,
                                  TSResilDestroy_ImpactDriven,TSResilView_ImpactDriven);CHKERRQ(ierr);	
  ierr                       = PetscLogObjectParent((PetscObject)check,(PetscObject)a);CHKERRQ(ierr); 
  ierr                       = PetscObjectIncrementTabLevel((PetscObject)a,(PetscObject)check,1);CHKERRQ(ierr);
  check->data 							 = (void*)a;
	a->K_resil = 0;
	a->threshold = 1;
  a->history = 0;             
	a->maxErrorBound = 3e-5;
  a->predictor = TSResilCompute_Last;
	a->alpha     = 0.01;
	a->adapt_cmpt     = 0;
  a->adapt_pred     = 10;
  a->sampling_dist  = 3;
  a->h1 = 0.0,  a->h2 = 0.0,  a->h3 = 0.0;
  a->sampling       = PETSC_FALSE; a->fixed = PETSC_FALSE;
  a->predictors[0]  = TSResilCompute_Last;
  a->predictors[1]  = TSResilCompute_Linear;
  a->predictors[2]  = TSResilCompute_Quadratic;
  check->ops->update    = TSResilUpdate_ImpactDriven;
  check->ops->check              = TSResilCheck_ImpactDriven;
  check->ops->setfromoptions 		 = TSResilSetFromOptions_ImpactDriven;
  check->ops->destroy        		 = TSResilDestroy_ImpactDriven;
  check->ops->view           		 = TSResilView_ImpactDriven;
  PetscLogEventRegister("AID",PETSC_VIEWER_CLASSID,&AID_EVENT);
  PetscLogEventGetId("AID",&AID_EVENT);
	PetscFunctionReturn(0);
}
