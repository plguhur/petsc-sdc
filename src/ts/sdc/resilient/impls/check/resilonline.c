#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/

typedef struct _p_sf_node* sf_node;
struct _p_sf_node {
  PetscReal   sf;
  sf_node    next;
} ;


struct _p_sf_list{
  sf_node     seed;
  sf_node     last;
  PetscInt    len;
} ;
typedef struct _p_sf_list* sf_list;
    
struct _p_TSResilOnline{
  PETSCHEADER(int);
  sf_list     list;
  PetscReal   alpha, threshold;
  PetscInt    K_adapt, p;     
} ;

typedef struct _p_TSResilOnline *TSResilOnline;
PetscClassId      TSRESIL_ONLINE_CLASSID;
PetscClassId      TSRESIL_MONITOR_ONLINE_CLASSID;
static PetscErrorCode TSResilComputeThreshold_Online(TSResil resil);
static PetscErrorCode TSResilAdaptCtrl_Online(TSResil resil) ;

#undef __FUNCT__
#define __FUNCT__ "TSResilCheck_Online"
static PetscErrorCode TSResilCheck_Online(TSResil resil,TS ts,PetscBool *check)
{
  TSResilCheck          chk = resil->check;
  TSResilOnline         o  = (TSResilOnline)(chk->data);
  TSResilSurrogate      s   = resil->surrogate;
	PetscReal             sf = s->r_sf;
  sf_list               l  = o->list;
  Vec                   SF = s->v_sf;
  PetscBool             form = s->vecForm;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if(o->p > l->len) {
    *check = PETSC_TRUE;
    PetscFunctionReturn(0);
  }
  if(form) {
    ierr = VecNorm(SF, s->norm, &sf);CHKERRQ(ierr);
  }
  ierr = TSResilComputeThreshold_Online(resil);CHKERRQ(ierr);
  *check = (o->threshold < PetscAbs(sf)) ? PETSC_FALSE : PETSC_TRUE;
  if(*check) { 
    ierr    = PetscInfo2(resil,"Surrogate function %g, threshold is %g: accepted step\n",(double)sf,(double)o->threshold);CHKERRQ(ierr);
  } else {
    ierr    = PetscInfo2(resil,"Surrogate function %g, threshold is %g: rejected step\n",(double)sf,(double)o->threshold);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilUpdate_Online"
PetscErrorCode TSResilUpdate_Online(TSResil resil, TS ts, Vec old_sol)
{
  TSResilSurrogate	    s = resil->surrogate;
  TSResilCheck          chk = resil->check;
  TSResilOnline         o  = (TSResilOnline)(chk->data);
  PetscBool             form = s->vecForm;
	PetscReal             sf = s->r_sf;
  Vec                   SF = s->v_sf;
  sf_list               list = o->list;
  sf_node               l = list->last, new; 
  PetscErrorCode        ierr;

  PetscFunctionBegin;
  if(form) {
    ierr = VecNorm(SF, s->norm, &sf);CHKERRQ(ierr);
  }
  // add element to linked list
  new             = (sf_node) malloc(sizeof(struct _p_sf_node));
  //ierr = PetscMalloc(sizeof(sf_node), &);CHKERRQ(ierr);
  new->sf = sf;
  new->next = 0;
  l->next = new;
  list->last = new;
  // rm
  if(list->len < o->p) { 
    list->len++;
    //resil->Nfp = 0;//we can make false positive while we don't know much
  }  else {
    sf_node f = list->seed;
    list->seed = f->next;
    //ierr = PetscFree(f);CHKERRQ(ierr);
    free(f);
  }
  // update K_adapt
  if(resil->FP) { ierr = TSResilAdaptCtrl_Online(resil);CHKERRQ(ierr); }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilComputeThreshold_Online"
static PetscErrorCode TSResilComputeThreshold_Online(TSResil resil) {
  TSResilCheck          chk = resil->check;
  TSResilOnline         o  = (TSResilOnline)(chk->data);
  sf_list               list = o->list;
  sf_node               curr = list->seed; 
  PetscReal             max = 0.0; 
	
	PetscFunctionBegin;
  while(curr) {
    if(curr->sf > max) { max = curr->sf;}
    curr = curr->next;
  }
  o->threshold = max*2*PetscPowScalar(1+o->alpha,o->K_adapt);
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilAdaptCtrl_Online"
static PetscErrorCode TSResilAdaptCtrl_Online(TSResil resil) {
  TSResilCheck          chk = resil->check;
  TSResilOnline         o  = (TSResilOnline)(chk->data);
	PetscReal FPR;
	
	PetscFunctionBegin;
	FPR = (resil->Nnegative != 0) ? (PetscReal)(resil->Nfp)/(PetscReal)(resil->Nnegative)  : 1;
	if (FPR > 0.03) {
		o->K_adapt += 1;
	}
	PetscFunctionReturn(0);
}

/*-----------------------------------------------------------------*/
#undef __FUNCT__
#define __FUNCT__ "TSResilReset_Online"
static PetscErrorCode TSResilReset_Online(TSResil resil)
{
  TSResilCheck          chk = resil->check;
  TSResilOnline         o = (TSResilOnline)(chk->data);
  sf_list               list = o->list;
  sf_node               curr = list->seed, next; 
  
	PetscFunctionBegin;
  while(curr) {
    next = curr->next;
    //ierr = PetscFree(curr);CHKERRQ(ierr);
    free(curr);
    curr = next;
  }
//  ierr = PetscFree(list);CHKERRQ(ierr);
  free(list);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilDestroy_Online"
static PetscErrorCode TSResilDestroy_Online(TSResil resil)
{
  TSResilCheck          chk = resil->check;
  TSResilOnline         a = (TSResilOnline)(chk->data);
  PetscErrorCode        ierr;

  PetscFunctionBegin;
  ierr = TSResilReset_Online(resil);CHKERRQ(ierr);
  ierr = PetscHeaderDestroy(&a);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilMonitor_Online"
static PetscErrorCode TSResilMonitor_Online(TSResil resil, PetscInt steps, PetscReal curtime,  Vec sol, PetscViewer viewer)
{
  TSResilCheck        chk = resil->check;
	TSResilOnline	      o = (TSResilOnline)chk->data;
  PetscErrorCode 			ierr;

  PetscFunctionBegin;
  ierr = PetscViewerASCIIPrintf(viewer,"%i, %g, %g\n",steps,curtime, o->threshold);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilSetFromOptions_Online"
static PetscErrorCode TSResilSetFromOptions_Online(PetscOptionItems *PetscOptionsObject,TSResil resil)
{
  TSResilCheck          chk = resil->check;
  TSResilOnline         o = (TSResilOnline)(chk->data);
  PetscBool             flg = PETSC_FALSE;
  TSResilObjMonitor     rmon = NULL;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscOptionsHead(PetscOptionsObject,"Online's checking resilience options");CHKERRQ(ierr);
  ierr = PetscOptionsReal("-ts_resil_online_alpha","Learning rate alpha","",o->alpha,&o->alpha,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-ts_resil_online_length","Size learning set","",o->p,&o->p,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-ts_resil_monitor_check","Monitor checking","",PETSC_FALSE,&flg,NULL);CHKERRQ(ierr);
  if(flg) { 
    ierr                       = PetscClassIdRegister("TSResilObjMonitor",&TSRESIL_ONLINE_CLASSID);CHKERRQ(ierr);
    ierr                       = PetscHeaderCreate(rmon,TSRESIL_ONLINE_CLASSID,"TSResilObjMonitor","Resilience//Online monitor","TS",
                                  PetscObjectComm((PetscObject)resil), 0,0);CHKERRQ(ierr);	
    rmon->monitor = TSResilMonitor_Online;   
    ierr = PetscStrcpy(rmon->name, "check.txt");CHKERRQ(ierr);
    ierr = TSResilAddASCIIMonitor(resil, rmon);CHKERRQ(ierr);
  }    
  ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilView_Online"
static PetscErrorCode TSResilView_Online(TSResil resil,PetscViewer viewer)
{
  TSResilCheck          chk = resil->check;
  TSResilOnline         o = (TSResilOnline)(chk->data);
  PetscErrorCode        ierr;
  PetscBool             iascii;

  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  if (iascii) {
    ierr = PetscViewerASCIIPrintf(viewer,"  Online: K_adapt %d, threshold %.15e",o->K_adapt,
					o->threshold);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}





#undef __FUNCT__
#define __FUNCT__ "TSResilCreate_Online"
/*MC
   TSADAPTBASIC - Online resilive controller for time stepping

   Level: intermediate

.seealso: TS, TSResil, TSSetResil()
M*/
PETSC_EXTERN PetscErrorCode TSResilCreate_Online(MPI_Comm comm,TSResil resil)
{
	TSResilCheck  check = resil->check;
	PetscErrorCode ierr;
  sf_node          n, m;
  sf_list          l;
	TSResilOnline    a;

	PetscFunctionBegin;
  CHKMEMQ;
  ierr                       = PetscClassIdRegister("TSResilOnline",&TSRESIL_ONLINE_CLASSID);CHKERRQ(ierr);
  ierr                       = PetscHeaderCreate(a,TSRESIL_ONLINE_CLASSID,"TSResilOnline","Resilience//Online module for checking","TS",comm,
					TSResilDestroy_Online,TSResilView_Online);CHKERRQ(ierr);		 
  ierr                       = PetscLogObjectParent((PetscObject)check,(PetscObject)a);CHKERRQ(ierr); 
  ierr                       = PetscObjectIncrementTabLevel((PetscObject)a,(PetscObject)check,1);CHKERRQ(ierr);
	check->data 							 = (void*)a;
  check->ops->check          = TSResilCheck_Online;
  check->ops->setfromoptions = TSResilSetFromOptions_Online;
  check->ops->destroy        = TSResilDestroy_Online;
  check->ops->view           = TSResilView_Online;
	check->ops->update				 = TSResilUpdate_Online;
	a->alpha 									 = 0.01;
	a->threshold 							 = 0.0;
	a->K_adapt 								 = 1;
	a->p                       = 3;
  //ierr                       = PetscMalloc(sizeof(struct _p_sf_node), &n);CHKERRQ(ierr);
  //ierr                       = PetscMalloc(sizeof(struct _p_sf_node), m);CHKERRQ(ierr);
  n                          = (sf_node) malloc(sizeof(struct _p_sf_node));
  m                          = (sf_node) malloc(sizeof(struct _p_sf_node));
  CHKMEMQ;
  m->sf                      = 0;
  m->next                    = 0;
  n->sf                      = 0; //0 will not impact the max
  n->next                    = m;
  l                          = (sf_list) malloc(sizeof(struct _p_sf_list));
//  ierr                       = PetscMalloc(sizeof(struct _p_sf_list), l);CHKERRQ(ierr);
  l->seed                    = n;
  l->last                    = m;
  l->len                     = 2;
  a->list                    = l;
  CHKMEMQ;
	PetscFunctionReturn(0);
}
