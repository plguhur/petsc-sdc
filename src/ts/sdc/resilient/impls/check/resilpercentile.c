#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/
#include "petscsys.h"   
#include <petscmath.h>
  
typedef struct {
  PETSCHEADER(int);
  PetscReal alpha, threshold, theta, *learning_set;
  PetscInt  K_adapt, p, learning_size, percentile;     
	PetscBool learning;
} TSResil_Percentile;

PetscClassId      TSRESILPERCENTILE_CLASSID;
PetscClassId      TSRESILMONITORPERC_CLASSID;

#undef __FUNCT__
#define __FUNCT__ "TSResilCheck_Percentile"
static PetscErrorCode TSResilCheck_Percentile(TSResil resil,TS ts,PetscBool *check)
{
  TSResilCheck          chk = resil->check;
  TSResil_Percentile    *pc = (TSResil_Percentile*)(chk->data);
  TSResilSurrogate      s   = resil->surrogate;
	PetscReal sf = s->r_sf;
  Vec SF = s->v_sf;
  PetscBool form = s->vecForm;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if(form) {
    ierr = VecNorm(SF, s->norm, &sf);CHKERRQ(ierr);
  }
  *check = (pc->threshold < PetscAbs(sf)) ? PETSC_FALSE : PETSC_TRUE;
  if(*check) { 
    ierr    = PetscInfo2(resil,"Surrogate function %g, threshold is %g: accepted step\n",(double)sf,(double)pc->threshold);CHKERRQ(ierr);
  } else {
    ierr    = PetscInfo2(resil,"Surrogate function %g, threshold is %g: rejected step\n",(double)sf,(double)pc->threshold);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilLearning_Percentile"
static PetscErrorCode TSResilLearning_Percentile(TSResil resil)
{
  TSResilCheck          chk = resil->check;
  TSResil_Percentile    *pc = (TSResil_Percentile*)(chk->data);
  TSResilSurrogate      s   = resil->surrogate;
	PetscReal sf = s->r_sf, sum_weights = 0.0, *w_set, *set=pc->learning_set;
  Vec SF = s->v_sf;
  PetscBool form = s->vecForm;
	PetscInt *size = &pc->learning_size, i;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
  if(form) {
    ierr = VecNorm(SF, s->norm, &sf);CHKERRQ(ierr);
  }
  set[*size] = sf;
	if (*size == pc->p) 	{
	    ierr = PetscMalloc1(*size, &w_set);CHKERRQ(ierr);
			for(i = 0; i < *size; i++) {
				PetscReal coeff = exp(-set[i]);
				sum_weights += coeff;
				w_set[i] = set[i] * coeff;
			}
			ierr = PetscSortReal(*size, w_set);CHKERRQ(ierr);
      PetscReal fsize = *size * 1.0;
			pc->theta = fsize*w_set[pc->percentile]/sum_weights;
			pc->threshold = 10.0*pc->theta;
      ierr = PetscFree(w_set);CHKERRQ(ierr);
      CHKMEMQ;
			pc->learning = PETSC_FALSE;
	}	else {
		*size += 1;
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilReset_Percentile"
static PetscErrorCode TSResilReset_Percentile(TSResil resil)
{
  TSResilCheck          chk = resil->check;
  TSResil_Percentile    *a = (TSResil_Percentile*)(chk->data);
	PetscErrorCode ierr;
  
	PetscFunctionBegin;
  ierr = PetscFree(a->learning_set);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilDestroy_Percentile"
static PetscErrorCode TSResilDestroy_Percentile(TSResil resil)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = TSResilReset_Percentile(resil);CHKERRQ(ierr);
  ierr = PetscFree(resil->check);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilMonitor_Percentile"
static PetscErrorCode TSResilMonitor_Percentile(TSResil resil, PetscInt steps, PetscReal curtime,  Vec sol, PetscViewer viewer)
{
  TSResilCheck        chk = resil->check;
	TSResil_Percentile	*pc = (TSResil_Percentile*)chk->data;
  PetscErrorCode 			ierr;

  PetscFunctionBegin;
  ierr = PetscViewerASCIIPrintf(viewer,"%i, %g, %g\n",steps,curtime, pc->threshold);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilSetFromOptions_Percentile"
static PetscErrorCode TSResilSetFromOptions_Percentile(PetscOptionItems *PetscOptionsObject,TSResil resil)
{
  TSResilCheck          chk = resil->check;
  TSResil_Percentile    *pc = (TSResil_Percentile*)(chk->data);
  PetscBool             flg = PETSC_FALSE;
  TSResilObjMonitor     rmon = NULL;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscOptionsHead(PetscOptionsObject,"Percentile's checking resilience options");CHKERRQ(ierr);
  ierr = PetscOptionsReal("-ts_resil_pc_alpha","Learning rate alpha","",pc->alpha,&pc->alpha,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-ts_resil_pc_length","Size learning set","",pc->p,&pc->p,&flg);CHKERRQ(ierr);
  if(flg) {
      ierr  = PetscFree(pc->learning_set);CHKERRQ(ierr);
      ierr  = PetscMalloc1(pc->p, &pc->learning_set);CHKERRQ(ierr);
  }
  ierr = PetscOptionsInt("-ts_resil_pc_percentile","Percentile of learning set","",pc->percentile,&pc->percentile,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-ts_resil_monitor_check","Monitor checking","",PETSC_FALSE,&flg,NULL);CHKERRQ(ierr);
  if(flg) { 
    ierr                       = PetscClassIdRegister("TSResilObjMonitor",&TSRESILMONITORPERC_CLASSID);CHKERRQ(ierr);
    ierr                       = PetscHeaderCreate(rmon,TSRESILMONITORPERC_CLASSID,"TSResilObjMonitor","Resilience//Percentile monitor","TS",
                                  PetscObjectComm((PetscObject)resil), 0,0);CHKERRQ(ierr);	
    rmon->monitor = TSResilMonitor_Percentile;   
    ierr = PetscStrcpy(rmon->name, "check.txt");CHKERRQ(ierr);
    ierr = TSResilAddASCIIMonitor(resil, rmon);CHKERRQ(ierr);
  }    
  ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilView_Percentile"
static PetscErrorCode TSResilView_Percentile(TSResil resil,PetscViewer viewer)
{
  TSResilCheck          chk = resil->check;
  TSResil_Percentile    *pc = (TSResil_Percentile*)(chk->data);
  PetscErrorCode ierr;
  PetscBool      iascii;

  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  if (iascii) {
    ierr = PetscViewerASCIIPrintf(viewer,"  Percentile: K_adapt %d, threshold %.15e, percentile %.15e, learning %d",pc->K_adapt,
					pc->threshold, pc->percentile, pc->learning);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "TSResilAdaptCtrl_Percentile"
static PetscErrorCode TSResilAdaptCtrl_Percentile(TSResil resil) {
  TSResilCheck          chk = resil->check;
  TSResil_Percentile    *pc = (TSResil_Percentile*)(chk->data);
	PetscReal FPR;
	
	PetscFunctionBegin;
	FPR = (resil->Nnegative != 0) ? (PetscReal)(resil->Nfp)/(PetscReal)(resil->Nnegative)  : 1;
	if (FPR > 0.03) {
		pc->K_adapt += 1;
		pc->threshold = PetscPowReal(1+pc->alpha,pc->K_adapt)*10.0*pc->theta; 
	}
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSResilUpdate_Percentile"
PetscErrorCode TSResilUpdate_Percentile(TSResil resil, TS ts, Vec old_sol)
{
  TSResilCheck          chk = resil->check;
  TSResil_Percentile    *pc = (TSResil_Percentile*)(chk->data);
  PetscErrorCode      ierr;

  PetscFunctionBegin;
  if(pc->learning) {  ierr = TSResilLearning_Percentile(resil);CHKERRQ(ierr); }
	if(resil->FP) { ierr = TSResilAdaptCtrl_Percentile(resil);CHKERRQ(ierr); }
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSResilCreate_Percentile"
/*MC
   TSADAPTBASIC - Percentile resilive controller for time stepping

   Level: intermediate

.seealso: TS, TSResil, TSSetResil()
M*/
PETSC_EXTERN PetscErrorCode TSResilCreate_Percentile(MPI_Comm comm,TSResil resil)
{
	TSResilCheck  check = resil->check;
	PetscErrorCode ierr;
	TSResil_Percentile    *a;

	PetscFunctionBegin;
  ierr                       = PetscClassIdRegister("TSResil_Percentile",&TSRESILPERCENTILE_CLASSID);CHKERRQ(ierr); 
  ierr                       = PetscHeaderCreate(a,TSRESILPERCENTILE_CLASSID,"TSResil_Percentile","Resilience//Percentile module for checking","TS",comm,
					TSResilDestroy_Percentile,TSResilView_Percentile);CHKERRQ(ierr);		 
  ierr                       = PetscLogObjectParent((PetscObject)check,(PetscObject)a);CHKERRQ(ierr); 
  ierr                       = PetscObjectIncrementTabLevel((PetscObject)a,(PetscObject)check,1);CHKERRQ(ierr);
	check->data 							 = (void*)a;
  check->ops->check          = TSResilCheck_Percentile;
  check->ops->setfromoptions = TSResilSetFromOptions_Percentile;
  check->ops->destroy        = TSResilDestroy_Percentile;
  check->ops->view           = TSResilView_Percentile;
	check->ops->update				 = TSResilUpdate_Percentile;
	a->alpha 									 = 0.01;
	a->threshold 							 = 0.0;
	a->K_adapt 								 = 1;
	a->theta                   = 0.0;  
	a->learning                = PETSC_TRUE;
	a->p                       = 5;
	a->percentile              = 4; // 99th percentile of the distribution
  a->learning_size              = 0;
	ierr                       = PetscMalloc1(a->p, &a->learning_set);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}
