
#include <petsc/private/tsimpl.h> /*I "petscts.h" I*/


typedef struct {
  PetscInt lastReject; // last number step that was rejected
  PetscReal threshold; 
} TSAdapt_CFL;



typedef struct {
  PetscBool always_accept;
  PetscReal clip[2];            /* admissible decrease/increase factors */
  PetscReal safety;             /* safety factor relative to target error */
  PetscReal reject_safety;      /* extra safety factor if the last step was rejected */
  PetscReal enorm;
  Vec       Y;
  void*     estimate;
  void*     double_estimate;
  AdaptEstType      mode, mode2;
  TSAdapt_CFL *cfl;
  PetscErrorCode (*estimator)(TSAdapt adapt,TS ts,PetscReal *enorm, void*);
  PetscErrorCode (*double_estimator)(TSAdapt adapt,TS ts,PetscReal *enorm, void*);
} TSAdapt_Basic;




#define TSADAPT_EMBEDDED "embedded"
#define TSADAPT_BDF "bdf"
#define TSADAPT_RICHARDSON "richardson"
#define TSADAPT_ORICHARDSON "orichardson"

struct _p_TSAdapt_VecList{
  Vec              vec;
  struct _p_TSAdapt_VecList* next;
};
  
typedef struct _p_TSAdapt_VecList * TSAdapt_VecList;  

typedef struct {
  PetscInt         stepno, history, fpr, fp[3], steps[3], order, q_max, cmpt, cmpt_max;
  PetscReal        lasth, h1,h2, lastEnorm, gamma, Gamma;
  TSAdapt_VecList list;
} TSAdapt_BDF;

typedef struct {
  PetscInt         stepno, fp;
  Vec              old_sol, last, last2;
  PetscReal        lastEnorm, lasth;
} TSAdapt_Richardson;

typedef struct {
  PetscInt         stepno, fp, ns;
  Vec              oldSol, lastSol, oldSlope, lastSlope, *Ydot;
  PetscReal        lastEnorm, h, lasth;
} TSAdapt_ORichardson;

typedef struct {
  PetscReal lasth, h1,h2,h3, lastEnorm, gamma, Gamma;
	PetscInt  cmpt, cmpt_max, stepno, fp[3], steps[3], order, history;
  TSAdapt_VecList list;
} TSAdapt_AID;

static PetscErrorCode TSAdaptEmbedded(TSAdapt adapt,TS ts,PetscReal *enorm, void*);
static PetscErrorCode TSAdaptBDF(TSAdapt adapt,TS ts,PetscReal *enorm, void*);
static PetscErrorCode TSAdaptRichardson(TSAdapt adapt,TS ts,PetscReal *enorm, void*) ;
static PetscErrorCode TSAdaptORichardson(TSAdapt adapt,TS ts,PetscReal *enorm, void*) ;
static PetscErrorCode TSAdaptCFL(TSAdapt adapt,TS ts,PetscReal h,PetscInt *next_sc,PetscReal *next_h,PetscBool *accept,PetscReal *wlte);

PetscLogEvent  EMBD_EVENT, ADAPT_EVENT, BDF_EVENT, BDF_VEC_EVENT,BDF_NORM_EVENT, BDF_MV_EVENT, BDF_INI_EVENT, AD_AID_EVENT;

#undef __FUNCT__
#define __FUNCT__ "TSAdaptChoose_Basic"
static PetscErrorCode TSAdaptChoose_Basic(TSAdapt adapt,TS ts,PetscReal h,PetscInt *next_sc,PetscReal *next_h,PetscBool *accept,PetscReal *wlte)
{
  TSAdapt_Basic  *basic = (TSAdapt_Basic*)adapt->data;
  PetscErrorCode ierr;
  Vec            X;
  PetscReal      enorm2=0.0,hfac_lte,h_lte,safety, norm;
  PetscInt       order,stepno;
  PetscBool      infOrNan ;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(ADAPT_EVENT, 0,0,0,0);CHKERRQ(ierr);
  ierr = TSGetTimeStepNumber(ts,&stepno);CHKERRQ(ierr);
  ierr = TSGetSolution(ts,&X);CHKERRQ(ierr);
  order = adapt->candidates.order[0];
  if (!basic->Y) {ierr = VecDuplicate(X,&basic->Y);CHKERRQ(ierr);}
  
  // embedded estimate
  ierr = (*basic->estimator)(adapt, ts, &basic->enorm, (void*)basic->estimate);CHKERRQ(ierr);
  if(ts->inject && ts->inject->log_lte) {
    ierr = VecAYPX(basic->Y, -1.0, ts->vec_sol);CHKERRQ(ierr);
    ierr = VecNorm(basic->Y,adapt->wnormtype,&norm);CHKERRQ(ierr);
    ierr = TSInjectLogNormNew(ts->inject, ts->steps, ts->time_step);CHKERRQ(ierr);
    ierr = TSInjectLogNorm(ts->inject, basic->enorm);CHKERRQ(ierr);
    ierr = TSInjectLogNorm(ts->inject, norm);CHKERRQ(ierr);
  }  
  
  // LTE-based double-check
  if (basic->enorm < 1. && basic->double_estimator)  {
      ierr = (*basic->double_estimator)(adapt, ts, &enorm2, (void*)basic->double_estimate);CHKERRQ(ierr);
      ierr = PetscInfo1(adapt, "Double-checking (based LTE) estimates scaled LTE %g\n", (double)enorm2);CHKERRQ(ierr);
      if(ts->inject && ts->inject->log_lte) {
        ierr = VecAYPX(basic->Y, -1.0, ts->vec_sol);CHKERRQ(ierr);
        ierr = VecNorm(basic->Y,adapt->wnormtype, &norm);CHKERRQ(ierr);
        ierr = TSInjectLogNorm(ts->inject, enorm2);CHKERRQ(ierr);
        ierr = TSInjectLogNorm(ts->inject, norm);CHKERRQ(ierr);
      }
  }
  else if(ts->inject && ts->inject->log_lte) {
    ierr = TSInjectLogNorm(ts->inject, 0.0);CHKERRQ(ierr);
    ierr = TSInjectLogNorm(ts->inject, 0.0);CHKERRQ(ierr);
  }
  
  // Select new step size
  safety = basic->safety;
  infOrNan = (PetscIsInfOrNanScalar(basic->enorm) || PetscIsInfOrNanScalar(enorm2));
  if (basic->enorm > 1. )  {
    if (!*accept) safety *= basic->reject_safety; /* The last attempt also failed, shorten more aggressively */
    if (h < (1 + PETSC_SQRT_MACHINE_EPSILON)*adapt->dt_min) {
      ierr    = PetscInfo2(adapt,"Estimated scaled local truncation error %g, accepting because step size %g is at minimum\n",(double)basic->enorm,(double)h);CHKERRQ(ierr);
      *accept = PETSC_TRUE && !infOrNan ;
    } else if (basic->always_accept) {
      ierr    = PetscInfo2(adapt,"Estimated scaled local truncation error %g, accepting step of size %g because always_accept is set\n",(double)basic->enorm,(double)h);CHKERRQ(ierr);
      *accept = PETSC_TRUE && !infOrNan ;
    } else {
      ierr    = PetscInfo2(adapt,"Estimated scaled local truncation error %g, rejecting step of size %g\n",(double)basic->enorm,(double)h);CHKERRQ(ierr);
      *accept = PETSC_FALSE;
    }
  } else {
    ierr    = PetscInfo2(adapt,"Estimated scaled local truncation error %g, accepting step of size %g\n",(double)basic->enorm,(double)h);CHKERRQ(ierr);
    *accept = PETSC_TRUE;
  }

  if(enorm2 > 1.0) { //double-check rejects the stepsize
    *accept = PETSC_FALSE;
    *next_h = h;
    ierr    = PetscInfo(adapt,"Double-checking (based LTE) rejects the step so the computed step-size is not kept\n");CHKERRQ(ierr);
  }
  else if (infOrNan) {
    *accept = PETSC_FALSE;
    hfac_lte = safety * PETSC_SMALL;
    h_lte    = h * PetscClipInterval(hfac_lte,basic->clip[0],basic->clip[1]);
    *next_h  = PetscClipInterval(h_lte,adapt->dt_min,adapt->dt_max);
  }
  else {
    /* The optimal new step based purely on local truncation error for this step. */
    if (basic->enorm == 0.0) {
      hfac_lte = safety * PETSC_INFINITY;
    } else {
      hfac_lte = safety * PetscPowReal(basic->enorm,-1./order);
    }
    h_lte    = h * PetscClipInterval(hfac_lte,basic->clip[0],basic->clip[1]);
    *next_h  = PetscClipInterval(h_lte,adapt->dt_min,adapt->dt_max);
    
  }
  *next_sc = 0;
  *wlte    = basic->enorm;
  ierr = PetscLogEventEnd(ADAPT_EVENT, 0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSAdaptEmbedded"
static PetscErrorCode TSAdaptEmbedded(TSAdapt adapt,TS ts,PetscReal *enorm, void * estimate) 
{
  PetscErrorCode ierr;
  TSAdapt_Basic  *basic = (TSAdapt_Basic*)adapt->data;
  Vec            X;
  PetscInt       order;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(EMBD_EVENT, 0,0,0,0);CHKERRQ(ierr);
  ierr = TSGetSolution(ts,&X);CHKERRQ(ierr);
  order = adapt->candidates.order[0] - 1;
  ierr  = TSEvaluateStep(ts,order,basic->Y,NULL);CHKERRQ(ierr);
  ierr  = TSErrorWeightedNorm(ts,X,basic->Y,adapt->wnormtype,enorm);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(EMBD_EVENT, 0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);  
}

#undef __FUNCT__
#define __FUNCT__ "TSAdaptRichardson"
static PetscErrorCode TSAdaptRichardson(TSAdapt adapt,TS ts,PetscReal *enorm, void * estimate) 
{ //assuming RK
  TSAdapt_Basic*      basic = (TSAdapt_Basic*)adapt->data;
  TSAdapt_Richardson* rich = (TSAdapt_Richardson*)estimate;
  PetscInt            order;
  PetscErrorCode      ierr;
  
  PetscFunctionBegin;
  order = adapt->candidates.order[0];
  // check whether an update has to be done
  if(ts->steps != rich->stepno) {
    rich->stepno = ts->steps;
    ierr = VecCopy(rich->last, rich->old_sol);CHKERRQ(ierr);
  }
  //computing the estimate
  ts->time_step /= 2;
  ierr = VecCopy(rich->old_sol,basic->Y);CHKERRQ(ierr); 
  ierr = TSMinStep(ts, basic->Y);CHKERRQ(ierr);
  ts->ptime +=   ts->time_step;
  ierr = TSMinStep(ts, basic->Y);CHKERRQ(ierr);
  ts->ptime -=  ts->time_step;
  ts->time_step *= 2;
  ierr = TSErrorWeightedNorm(ts,ts->vec_sol,basic->Y,adapt->wnormtype,enorm);CHKERRQ(ierr);
  *enorm /= (1-PetscPowScalar(.5, order));//(PetscPowScalar(2, order) - 1);//

  //prepare next update
  if(*enorm == rich->lastEnorm) {
    ierr = PetscInfo2(adapt, "Double-checking (based LTE) computed %g. It seems to be a false positive (the %ith) so enorm2 is 0.0\n", *enorm, rich->fp);CHKERRQ(ierr);
    *enorm = 0.0;
    rich->fp += 1;
  } 
  else {
    rich->lastEnorm = *enorm;
  }
  //prepare next update
  if(*enorm < 1.0) {
    ierr = VecCopy(ts->vec_sol, rich->last);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);  
}

#undef __FUNCT__
#define __FUNCT__ "TSAdaptORichardson"
static PetscErrorCode TSAdaptORichardson(TSAdapt adapt,TS ts,PetscReal *enorm, void * estimate) 
{ //assuming RK
  TSAdapt_Basic*      basic = (TSAdapt_Basic*)adapt->data;
  TSAdapt_ORichardson* rich = (TSAdapt_ORichardson*)estimate;
  PetscInt            order, i, s;
  PetscReal           t, dt;
  Vec                 *Ydot, old_sol;
  PetscErrorCode      ierr;
  
  PetscFunctionBegin;
  order = adapt->candidates.order[0] ;
  // check whether an update has to be done
  if(ts->steps != rich->stepno) {
    rich->stepno = ts->steps;
    rich->h      = rich->lasth;
    ierr = VecCopy(rich->lastSlope, rich->oldSlope);CHKERRQ(ierr);
    ierr = VecCopy(rich->lastSol, rich->oldSol);CHKERRQ(ierr);
  }
  // compute missing stages
  ierr = VecCopy(rich->oldSlope, rich->Ydot[0]);CHKERRQ(ierr);
  t    = ts->ptime - rich->h; 
  dt   = ts->time_step + rich->h;
  for(i = 1; i < rich->ns - 1; i++) {
      ierr = VecCopy(rich->oldSol, basic->Y);CHKERRQ(ierr);
      ierr = TSGetStageSlope(ts, i, t, dt, rich->Ydot, basic->Y);CHKERRQ(ierr);
  }
  //ierr = TSGetPrevSolution(ts, &old_sol); CHKERRQ(ierr);
  //ierr = TSGetNextStageSlope(ts, old_sol, rich->Ydot[rich->ns-1]);CHKERRQ(ierr); 
  
  // evaluate step
  ierr = VecCopy(rich->oldSol, basic->Y);CHKERRQ(ierr);
  ierr = TSMinEvaluateStep(ts, dt, rich->Ydot, basic->Y); CHKERRQ(ierr);
  ierr = TSErrorWeightedNorm(ts,ts->vec_sol,basic->Y,adapt->wnormtype,enorm);CHKERRQ(ierr);
  *enorm /= (-1+PetscPowScalar(2.0, order));

  //prepare next update
  if(*enorm == rich->lastEnorm) {
    ierr = PetscInfo2(adapt, "Double-checking (based LTE) computed %g. It seems to be a false positive (the %ith) so enorm2 is 0.0\n", *enorm, rich->fp);CHKERRQ(ierr);
    *enorm = 0.0;
    rich->fp += 1;
  } else {
    rich->lastEnorm = *enorm;
  }
  //prepare next update
  if(*enorm < 1.0) {
    ierr = TSGetStageSlopes(ts, &s, &Ydot); CHKERRQ(ierr); 
    ierr = VecCopy(Ydot[0], rich->lastSlope);CHKERRQ(ierr);
    ierr = TSGetPrevSolution(ts, &old_sol); CHKERRQ(ierr); 
    ierr = VecCopy(old_sol, rich->lastSol);CHKERRQ(ierr);
    rich->lasth = ts->time_step;
  }
  PetscFunctionReturn(0);  
}


#undef __FUNCT__
#define __FUNCT__ "TSAdaptBDF"
static PetscErrorCode TSAdaptBDF(TSAdapt adapt,TS ts,PetscReal *enorm, void * estimate) 
{
  TSAdapt_Basic*  basic = (TSAdapt_Basic*)adapt->data;
  TSAdapt_BDF* bdf = (TSAdapt_BDF*)estimate;
  TSAdapt_VecList l = bdf->list, m, n;
  PetscErrorCode ierr;
  Vec            X, oldSol, nextSlope;
  PetscReal      h = ts->time_step, delta, delta2, alpha, beta, gamma, eta, s;
  PetscInt       order,i;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(BDF_EVENT, 0,0,0,0);
  ierr = TSGetSolution(ts,&X);CHKERRQ(ierr);
  ierr = TSGetOldSolution(ts,&oldSol);CHKERRQ(ierr);
  
  // check whether an update has to be done
//  ierr = PetscLogEventBegin(BDF_MV_EVENT, 0,0,0,0);
  if(ts->steps != bdf->stepno) {
    // get a Vec for new node
    if(bdf->history == 3) { //limit of 2 in the history!
      for(i = 0; i < bdf->history-2; i++){
        l = l->next;
      }
      m = l->next;
      l->next = 0;
    }
    else {
      ierr = PetscNew(&m);CHKERRQ(ierr);
      ierr = VecDuplicate(X, &m->vec);CHKERRQ(ierr);        
      bdf->history++;
    }
    // m->vec = old solution of the solver
    ierr = VecCopy(oldSol, m->vec);CHKERRQ(ierr); 
    // add m to the linked list
    m->next = bdf->list;
    bdf->stepno = ts->steps;
    bdf->list = m;
    bdf->h2 = bdf->h1;
    bdf->h1 = bdf->lasth;
  }
  //ierr = PetscLogEventEnd(BDF_MV_EVENT, 0,0,0,0);CHKERRQ(ierr);
    
  // last time was a False Positive ?
  if(basic->enorm == bdf->lastEnorm) {
    bdf->fp[bdf->order] += 1;
    ierr = PetscInfo2(adapt, "Double-checking (based LTE) computed %g. It seems to be a false positive (the %ith) so enorm2 is 0.0\n", *enorm, bdf->fp[0] + bdf->fp[1] + bdf->fp[2]);CHKERRQ(ierr);
    *enorm = 0.0;
    bdf->cmpt = bdf->cmpt_max;
  } 
  else {
    bdf->steps[bdf->order] += 1;
    // select order: 
    if(bdf->cmpt++ == bdf->cmpt_max) {
      bdf->cmpt = 0;
      s = (PetscReal)(bdf->fp[bdf->order])/((PetscReal)bdf->steps[bdf->order]);
      if (s < bdf->gamma) {
        bdf->order = PetscMax(0, bdf->order-1);
      } else if (s > bdf->Gamma) {
        bdf->order = PetscMin(2, bdf->order+1);
      }
    }
    s = (PetscReal)(bdf->fp[bdf->order])/((PetscReal)bdf->steps[bdf->order]);
    order = PetscMin(adapt->candidates.order[0] - 1 + bdf->order, bdf->history);
    ierr = PetscInfo2(adapt, "Double-checking (based LTE) has order %i and FPR %g.\n", order, s);CHKERRQ(ierr);
    
    // compute x_bdf
    l = bdf->list;
    //ierr = PetscLogEventBegin(BDF_INI_EVENT, 0,0,0,0);CHKERRQ(ierr);
    ierr = TSGetNextStageSlope(ts, X, &nextSlope);CHKERRQ(ierr); // consider the case FSAL
    //ierr = PetscLogEventEnd(BDF_INI_EVENT, 0,0,0,0);CHKERRQ(ierr);
    if(order == 3) { 
      //  ierr = PetscLogEventBegin(BDF_VEC_EVENT, 0,0,0,0);CHKERRQ(ierr);
        delta = h/bdf->h1, delta2 = bdf->h2/bdf->h1;
        n    = l->next;
        alpha= PetscPowScalar(1+delta, 2)*PetscPowScalar(delta2*(1+delta)+1, 2)/((1+delta2)*(2*delta+delta2*(delta+1)*(3*delta+1)+1));
        beta = -PetscPowScalar(delta, 2)*PetscPowScalar(delta2*(1+delta)+1, 2)/(2*delta+delta2*(delta+1)*(3*delta+1)+1);
        gamma= PetscPowScalar(delta, 2)*PetscPowScalar(1+delta, 2)*PetscPowScalar(delta2, 3)/((1+delta2)*(2*delta+delta2*(delta+1)*(3*delta+1)+1));
        eta  = h*(1+delta)*(delta*delta2+delta2+1)/(3*delta2*PetscPowScalar(delta, 2)+4*delta2*delta+2*delta+delta2+1);
      	ierr = VecWAXPY(basic->Y, beta/alpha, n->vec, l->vec);CHKERRQ(ierr);
        n    = n->next;
      	ierr = VecAXPBYPCZ(basic->Y, eta, gamma, alpha, nextSlope, n->vec);CHKERRQ(ierr);
       // ierr = PetscLogEventEnd(BDF_VEC_EVENT, 0,0,0,0);CHKERRQ(ierr);
    }
    else if(order == 2) { 
	 // PetscLogEventBegin(BDF_VEC_EVENT,0,0,0,0);
          delta = h/bdf->h1;
          alpha= PetscPowScalar(1+delta, 2)/(1+2*delta);
          beta = -PetscPowScalar(delta, 2)/(1+2*delta);
          gamma= h*(1+delta)/(1+2*delta);
          n    = l->next;
         ierr = VecWAXPY(basic->Y, gamma/alpha, nextSlope, l->vec);CHKERRQ(ierr);
         ierr = VecAXPBY(basic->Y, beta, alpha, n->vec);CHKERRQ(ierr);
        // ierr = PetscLogEventEnd(BDF_VEC_EVENT, 0,0,0,0);CHKERRQ(ierr);
        } 
    else if(order == 1) {
       // ierr = PetscLogEventBegin(BDF_VEC_EVENT, 0,0,0,0);CHKERRQ(ierr);
        ierr = VecCopy(l->vec, basic->Y);CHKERRQ(ierr);
    	ierr = VecAXPY(basic->Y,  h, nextSlope);CHKERRQ(ierr);
         // ierr = PetscLogEventEnd(BDF_VEC_EVENT, 0,0,0,0);CHKERRQ(ierr);
    } 
   // ierr = PetscLogEventBegin(BDF_NORM_EVENT, 0,0,0,0);CHKERRQ(ierr);
    ierr  = TSErrorWeightedNorm(ts,X,basic->Y,adapt->wnormtype,enorm);CHKERRQ(ierr);
   // ierr = PetscLogEventEnd(BDF_NORM_EVENT, 0,0,0,0);CHKERRQ(ierr);

    bdf->lastEnorm = basic->enorm;
  
  }
  
  if(*enorm < 1.0) {
    bdf->lasth = ts->time_step;
  }
  ierr = PetscLogEventEnd(BDF_EVENT, 0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);  
}

#undef __FUNCT__
#define __FUNCT__ "TSAdaptAID"
static PetscErrorCode TSAdaptAID(TSAdapt adapt,TS ts,PetscReal *enorm, void * estimate) 
{
  TSAdapt_Basic*  basic = (TSAdapt_Basic*)adapt->data;
  TSAdapt_AID*    aid = (TSAdapt_AID*)estimate;
  TSAdapt_VecList l = aid->list, m, n;
  PetscErrorCode ierr;
  Vec            X, oldSol;
  PetscReal      h = ts->time_step, alpha, beta, gamma, s;
  PetscInt       order,i;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(AD_AID_EVENT, 0,0,0,0);
  ierr = TSGetSolution(ts,&X);CHKERRQ(ierr);
  ierr = TSGetOldSolution(ts,&oldSol);CHKERRQ(ierr);
  
  // check whether an update has to be done
  if(ts->steps != aid->stepno) {
    // get a Vec for new node
    if(aid->history == 3) { //limit of 2 in the history!
      for(i = 0; i < aid->history-2; i++){
        l = l->next;
      }
      m = l->next;
      l->next = 0;
    }
    else {
      ierr = PetscNew(&m);CHKERRQ(ierr);
      ierr = VecDuplicate(X, &m->vec);CHKERRQ(ierr);        
      aid->history++;
    }
    // m->vec = old solution of the solver
    ierr = VecCopy(oldSol, m->vec);CHKERRQ(ierr); 
    // add m to the linked list
    m->next = aid->list;
    aid->stepno = ts->steps;
    aid->list = m;
    aid->h2 = aid->h1;
    aid->h1 = aid->lasth;
  }
  //ierr = PetscLogEventEnd(BDF_MV_EVENT, 0,0,0,0);CHKERRQ(ierr);
    
  // last time was a False Positive ?
  if(basic->enorm == aid->lastEnorm) {
    aid->fp[aid->order] += 1;
    ierr = PetscInfo2(adapt, "Double-checking (based LTE) computed %g. It seems to be a false positive (the %ith) so enorm2 is 0.0\n", *enorm, aid->fp[0] + aid->fp[1] + aid->fp[2]);CHKERRQ(ierr);
    *enorm = 0.0;
  } 
  else {
    aid->steps[aid->order] += 1;
    // select order: 
    if(aid->cmpt++ == aid->cmpt_max) {
      aid->cmpt = 0;
      s = (PetscReal)(aid->fp[aid->order])/((PetscReal)aid->steps[aid->order]);
      if (s < aid->gamma) {
        aid->order = PetscMax(0, aid->order-1);
      } else if (s > aid->Gamma) {
        aid->order = PetscMin(2, aid->order+1);
      }
    }
    s = (PetscReal)(aid->fp[aid->order])/((PetscReal)aid->steps[aid->order]);
    order = PetscMin(1+aid->order, aid->history);
    ierr = PetscInfo2(adapt, "Double-checking (based LTE) has order %i and FPR %g.\n", order, s);CHKERRQ(ierr);
    
    // compute extrapolation
    l = aid->list;
    if(order == 3) { 
        n    = l->next;
        alpha= (h+aid->h1)*(h+aid->h1+aid->h2)/(aid->h1*(aid->h1+aid->h2));
        beta = -h*(h+aid->h1+aid->h2)/(aid->h1*aid->h2);
        gamma= h*(h+aid->h1)/((aid->h1+aid->h2)*aid->h2);
        ierr = VecWAXPY(basic->Y, alpha/beta, oldSol, n->vec);CHKERRQ(ierr);
        n    = n->next;
        ierr = VecAXPBY(basic->Y, gamma, beta, n->vec);CHKERRQ(ierr);
    }
    else if(order == 2) { 
        n    = l->next;
        alpha = (h+aid->h1)/aid->h1, beta = -h/aid->h1;
        ierr = VecWAXPY(basic->Y, alpha/beta, oldSol, n->vec);CHKERRQ(ierr);
        ierr = VecScale(basic->Y, beta);CHKERRQ(ierr);
    } 
    else if(order == 1) {
        ierr = VecCopy(l->vec, basic->Y);CHKERRQ(ierr);
    } 
    ierr  = TSErrorWeightedNorm(ts,X,basic->Y,adapt->wnormtype,enorm);CHKERRQ(ierr);
    aid->lastEnorm = basic->enorm;
  }
  
  if(*enorm < 1.0) {
    aid->lasth = ts->time_step;
  }
  ierr = PetscLogEventEnd(AD_AID_EVENT, 0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);  
}

/*
#undef __FUNCT__
#define __FUNCT__ "TSAdaptBestPredictor"
PetscErrorCode TSAdaptBestPredictor(TSAdapt adapt, TSAdapt_VecList l)
{
  PetscReal               locPredErr[3], mag, err, minErr = 0.0;
  PetscBool               gammaTest[3], gammaTest2[3], gammaTotal = PETSC_FALSE, gammaTotal2 = PETSC_FALSE, test = PETSC_TRUE;
  PetscInt                i;
  TSResilCheck  check = resil->check;
  TSResil_ImpactDriven    *id = (TSResil_ImpactDriven*)check->data;         
  PetscErrorCode          ierr;
  
  PetscFunctionBegin;
	for (i = 0; i < 3; i++ )
	{
		ierr = (*id->predictors[i])(resil, id->X2,id->X3,id->X4, id->Y);CHKERRQ(ierr);
		mag = 0.0;
		test = PETSC_FALSE;
    ierr = VecAYPX(id->Y, -1.0, id->X1);
    ierr = VecNorm(id->Y, NORM_INFINITY, &err);
    gammaTest[i] = (err < id->maxErrorBound);
    gammaTotal |= gammaTest[i];
		locPredErr[i] = err;
	}

	if(gammaTotal) {
		gammaTotal = PETSC_FALSE;
  	for (i = 0; i < 3; i++ )
  	{
	   		gammaTest2[i] = (err < id->maxErrorBound2);
				gammaTotal2 |= gammaTest2[i];
		}
		
		if(gammaTotal2) {
			for ( i = 0; i < 3; i++ ) {
				if(gammaTest[i]) {
					id->predictor = id->predictors[i];
          break;
        }
			}
		}
		else {
      minErr = id->maxErrorBound;
    	for (i = 0; i < 3; i++ )
    	{
  	   		if(locPredErr[i] < minErr) {
            id->predictor = id->predictors[i];
            minErr = locPredErr[i];
  	   		} 
  		}
		}
	}
	
  else {
    id->predictor = id->predictors[0];
    minErr = locPredErr[0];
  	for (i = 1; i < 3; i++ )
  	{
	   		if(locPredErr[i] < minErr) {
          id->predictor = id->predictors[i];
          minErr = locPredErr[i];
	   		} 
		}
    
  }

  PetscFunctionReturn(0);
}
*/

#undef __FUNCT__
#define __FUNCT__ "TSAdaptCFL"
static PetscErrorCode TSAdaptCFL(TSAdapt adapt,TS ts,PetscReal h,PetscInt *next_sc,PetscReal *next_h,PetscBool *accept,PetscReal *wlte)
{
  TSAdapt_Basic*  basic = (TSAdapt_Basic*)adapt->data;
  TSAdapt_CFL*    cfl = basic->cfl;
  PetscErrorCode  ierr;
  TSCFLFunction   fCFL;
  PetscReal       rCFL;

  PetscFunctionBegin;
  if(cfl->lastReject == ts->steps) {
    ierr = PetscInfo(adapt,"Last step was already rejected\n");CHKERRQ(ierr);
    *accept  = PETSC_TRUE;
    PetscFunctionReturn(0);
  }
  ierr = TSGetCFLFunction(ts,&fCFL);CHKERRQ(ierr);
  if (!fCFL) {
    SETERRQ(PetscObjectComm((PetscObject)adapt),PETSC_ERROR_INITIAL,"Must provide a CFL function");
  }
  ierr = (*fCFL)(ts,h, ts->ptime, &rCFL);CHKERRQ(ierr);
  if (rCFL > cfl->threshold) {
    ierr = PetscInfo2(adapt,"Cannot satisfy CFL constraint %g (maxCFL: %g)\n",(double)cfl->threshold,(double)rCFL);CHKERRQ(ierr);
    if(next_sc) *next_sc = 0;
    if(next_h)  *next_h  = -1;//PetscClipInterval(rCFL,adapt->dt_min,adapt->dt_max);
    *accept  = PETSC_FALSE;
    cfl->lastReject = ts->steps;
  }
  if(wlte)       *wlte    = -1;                /* Weighted local truncation error was not evaluated */
  PetscFunctionReturn(0);
}


  /*------------------------------------------------*/
#undef __FUNCT__
#define __FUNCT__ "TSAdaptReset_Basic"
static PetscErrorCode TSAdaptReset_Basic(TSAdapt adapt)
{
  TSAdapt_Basic  *basic = (TSAdapt_Basic*)adapt->data;
  TSAdapt_BDF    *bdf;
  TSAdapt_AID    *aid;
  TSAdapt_ORichardson    *orich;
  TSAdapt_VecList l, m;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = VecDestroy(&basic->Y);CHKERRQ(ierr);
  if(basic->mode2 == TSADAPT_EST_BDF) {
    bdf = (TSAdapt_BDF*)basic->double_estimate;
    l = bdf->list;
    while(l){
      m = l->next;
      ierr = VecDestroy(&l->vec);CHKERRQ(ierr);
      ierr = PetscFree(l);CHKERRQ(ierr);
      l = m;
    }
    ierr = PetscFree(basic->double_estimate);CHKERRQ(ierr);
  }
  else if(basic->mode2 == TSADAPT_EST_AID) {
    aid = (TSAdapt_AID*)basic->double_estimate;
    l = aid->list;
    while(l){
      m = l->next;
      ierr = VecDestroy(&l->vec);CHKERRQ(ierr);
      ierr = PetscFree(l);CHKERRQ(ierr);
      l = m;
    }
    ierr = PetscFree(basic->double_estimate);CHKERRQ(ierr);
  }
  else if(basic->mode2 == TSADAPT_EST_ORICHARDSON) {
    orich = (TSAdapt_ORichardson*)basic->double_estimate;
    ierr = VecDestroyVecs(orich->ns,&orich->Ydot);CHKERRQ(ierr);
    ierr = VecDestroy(&orich->lastSol);CHKERRQ(ierr);
    ierr = VecDestroy(&orich->oldSol);CHKERRQ(ierr);
    ierr = VecDestroy(&orich->lastSlope);CHKERRQ(ierr);
    ierr = VecDestroy(&orich->oldSlope);CHKERRQ(ierr);
    ierr = PetscFree(basic->double_estimate);CHKERRQ(ierr);
  }
  
  
  if(basic->mode == TSADAPT_EST_BDF) {
    bdf = (TSAdapt_BDF*)basic->estimate;
    l = bdf->list;
    while(l){
      m = l->next;
      ierr = VecDestroy(&l->vec);CHKERRQ(ierr);
      l = m;
    }
    ierr = PetscFree(basic->estimate);CHKERRQ(ierr);
  }
  if(basic->cfl) {
    ierr = PetscFree(basic->cfl);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSAdaptDestroy_Basic"
static PetscErrorCode TSAdaptDestroy_Basic(TSAdapt adapt)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = TSAdaptReset_Basic(adapt);CHKERRQ(ierr);
  ierr = PetscFree(adapt->data);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSAdaptSetUp_Basic"
static PetscErrorCode TSAdaptSetUp_Basic(TS ts) {
  TSAdapt adapt         = ts->adapt;
  TSAdapt_Basic  *basic = (TSAdapt_Basic*)adapt->data;
  TSAdapt_BDF    *bdf;
  TSAdapt_AID    *aid;
  TSAdapt_Richardson    *rich;
  TSAdapt_ORichardson    *orich;    
  TSAdapt_VecList l;
  Vec             u = ts->vec_sol;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if(basic->mode == TSADAPT_EST_BDF) {
    bdf = (TSAdapt_BDF*)basic->estimate;
    l = bdf->list;
    ierr = VecDuplicate(u,&l->vec);CHKERRQ(ierr);
    ierr = VecCopy(u,l->vec);CHKERRQ(ierr);
    bdf->history = 1;
  }
  else if(basic->mode == TSADAPT_EST_RICHARDSON) {  
    rich = (TSAdapt_Richardson*)basic->estimate;
    ierr = VecDuplicate(u,&rich->last);CHKERRQ(ierr);
    ierr = VecCopy(u,rich->last);CHKERRQ(ierr);
    ierr = VecDuplicate(u,&rich->old_sol);CHKERRQ(ierr);
    ierr = VecCopy(u,rich->old_sol);CHKERRQ(ierr);
  }
  if(basic->mode2 == TSADAPT_EST_BDF) {
    bdf = (TSAdapt_BDF*)basic->double_estimate;
    l = bdf->list;
    ierr = VecDuplicate(u,&l->vec);CHKERRQ(ierr);
    ierr = VecCopy(u,l->vec);CHKERRQ(ierr);
    bdf->history = 1;
    bdf->h1 = ts->time_step;
    bdf->h2 = ts->time_step;
    bdf->lasth = ts->time_step;
  } else if(basic->mode2 == TSADAPT_EST_AID) {
    aid = (TSAdapt_AID*)basic->double_estimate;
    l = aid->list;
    ierr = VecDuplicate(u,&l->vec);CHKERRQ(ierr);
    ierr = VecCopy(u,l->vec);CHKERRQ(ierr);
    aid->history = 1;
    aid->h1 = ts->time_step;
    aid->h2 = ts->time_step;
    aid->lasth = ts->time_step;
  }
  else if(basic->mode2 == TSADAPT_EST_RICHARDSON) {  
    rich = (TSAdapt_Richardson*)basic->double_estimate;
    ierr = VecDuplicate(u,&rich->last);CHKERRQ(ierr);
    ierr = VecCopy(u,rich->last);CHKERRQ(ierr);
    ierr = VecDuplicate(u,&rich->old_sol);CHKERRQ(ierr);
    ierr = VecCopy(u,rich->old_sol);CHKERRQ(ierr);
  }   
  else if(basic->mode2 == TSADAPT_EST_ORICHARDSON) {  
    orich = (TSAdapt_ORichardson*)basic->double_estimate;
    ierr = VecDuplicate(u,&orich->lastSol);CHKERRQ(ierr);
    ierr = VecCopy(u,orich->lastSol);CHKERRQ(ierr);
    ierr = VecDuplicate(u,&orich->oldSol);CHKERRQ(ierr);
    ierr = VecCopy(u,orich->oldSol);CHKERRQ(ierr);
    ierr = VecDuplicate(u,&orich->lastSlope);CHKERRQ(ierr);
    ierr = VecDuplicate(u,&orich->oldSlope);CHKERRQ(ierr);
    orich->h = ts->time_step;
    orich->lasth = ts->time_step;
    ierr = TSGetNumberStages(ts, &orich->ns);CHKERRQ(ierr);
    ierr = VecDuplicateVecs(ts->vec_sol,orich->ns,&orich->Ydot);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}
  
#undef __FUNCT__
#define __FUNCT__ "TSAdaptSetFromOptions_Basic"
static PetscErrorCode TSAdaptSetFromOptions_Basic(PetscOptionItems *PetscOptionsObject,TSAdapt adapt)
{
  TSAdapt_Basic  *basic = (TSAdapt_Basic*)adapt->data;
  TSAdapt_BDF    *bdf;
  TSAdapt_AID    *aid;
  TSAdapt_Richardson    *rich;
  TSAdapt_ORichardson    *orich;
  TSAdapt_CFL    *cfl;
  PetscErrorCode ierr;
  PetscInt       two;
  PetscBool      set, flg;

  PetscFunctionBegin;
  ierr = PetscOptionsHead(PetscOptionsObject,"Basic adaptive controller options");CHKERRQ(ierr);
  two  = 2;
  ierr = PetscOptionsRealArray("-ts_adapt_basic_clip","Admissible decrease/increase in step size","",basic->clip,&two,&set);CHKERRQ(ierr);
  if (set && (two != 2 || basic->clip[0] > basic->clip[1])) SETERRQ(PetscObjectComm((PetscObject)adapt),PETSC_ERR_ARG_OUTOFRANGE,"Must give exactly two values to -ts_adapt_basic_clip");
  ierr = PetscOptionsReal("-ts_adapt_basic_safety","Safety factor relative to target error","",basic->safety,&basic->safety,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-ts_adapt_basic_reject_safety","Extra safety factor to apply if the last step was rejected","",basic->reject_safety,&basic->reject_safety,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-ts_adapt_basic_always_accept","Always accept the step regardless of whether local truncation error meets goal","",basic->always_accept,&basic->always_accept,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnum("-ts_adapt_basic_double_lte","LTE-based double check","",AdaptEstTypes,(PetscEnum)basic->mode2,(PetscEnum*)&basic->mode2,NULL);CHKERRQ(ierr);

  if(basic->mode2 == TSADAPT_EST_RICHARDSON) {  
    ierr                       = PetscNew(&rich);CHKERRQ(ierr);
    basic->double_estimate     = (void*)rich;
    basic->double_estimator    = TSAdaptRichardson;
    rich->stepno               = 0;
    rich->fp                    = 0;
    rich->lastEnorm             = 0.0;
    adapt->ops->setup    = TSAdaptSetUp_Basic;
  } else if(basic->mode2 == TSADAPT_EST_BDF) {
    ierr                       = PetscNew(&bdf);CHKERRQ(ierr);
    basic->double_estimate     = (void*)bdf;
    bdf->history               = 0;
    bdf->stepno                = 0;
    bdf->order                 = 0;
    bdf->steps[0]                 = 0;
    bdf->steps[1]                 = 0;
    bdf->steps[2]                 = 0;
    bdf->fp[0]                 = 0;
    bdf->fp[1]                 = 0;
    bdf->fp[2]                 = 0;
    bdf->fpr		       = 0;
    bdf->gamma                 = 0.01;
    bdf->Gamma                 = 0.05;
    bdf->lastEnorm             = 0.0;
    bdf->cmpt                  = 0;
    bdf->cmpt_max              = 10;
    basic->double_estimator    = TSAdaptBDF;
    ierr = PetscNew(&bdf->list);CHKERRQ(ierr);
    adapt->ops->setup    = TSAdaptSetUp_Basic;
  } else if(basic->mode2 == TSADAPT_EST_AID) {
    ierr                       = PetscNew(&aid);CHKERRQ(ierr);
    basic->double_estimate     = (void*)aid;
    aid->history               = 0;
    aid->stepno                = 0;
    aid->order                 = 0;
    aid->steps[0]              = 0;
    aid->steps[1]              = 0;
    aid->steps[2]              = 0;
    aid->fp[0]                 = 0;
    aid->fp[1]                 = 0;
    aid->fp[2]                 = 0;
    aid->gamma                 = 0.01;
    aid->Gamma                 = 0.05;
    aid->lastEnorm             = 0.0;
    aid->cmpt                  = 0;
    aid->cmpt_max              = 10;
    basic->double_estimator    = TSAdaptAID;
    ierr = PetscNew(&aid->list);CHKERRQ(ierr);
    adapt->ops->setup    = TSAdaptSetUp_Basic;
  }  else if(basic->mode2 == TSADAPT_EST_ORICHARDSON) {  
    ierr                       = PetscNew(&orich);CHKERRQ(ierr);
    basic->double_estimate     = (void*)orich;
    basic->double_estimator    = TSAdaptORichardson;
    orich->stepno              = 0;
    orich->fp                  = 0;
    orich->lastEnorm           = 0.0;
    adapt->ops->setup    = TSAdaptSetUp_Basic;
  }
  else if(basic->mode2 == TSADAPT_EST_EMBEDDED) {   basic->double_estimator           = TSAdaptEmbedded;}
  
  ierr = PetscOptionsEnum("-ts_adapt_basic_mode","Method for computing an LTE","", AdaptEstTypes,(PetscEnum) basic->mode,(PetscEnum*)&basic->mode,NULL);CHKERRQ(ierr);
  if(basic->mode == TSADAPT_EST_BDF) {
    ierr                       = PetscNew(&bdf);CHKERRQ(ierr);
    basic->estimate            = (void*)bdf;
    bdf->history               = 0;
    bdf->stepno                = 0;
    bdf->lastEnorm             = 0.0;
    bdf->order                 = 0;
    bdf->fp[0]                 = 0;
    bdf->fp[1]                 = 0;
    bdf->fp[2]                 = 0;
    bdf->gamma                 = 0.01;
    bdf->Gamma                 = 0.05;
    bdf->cmpt                  = 0;
    bdf->cmpt_max              = 5;
    basic->estimator           = TSAdaptBDF;
    ierr = PetscNew(&bdf->list);CHKERRQ(ierr);
    adapt->ops->setup    = TSAdaptSetUp_Basic;
  } 
  else if(basic->mode == TSADAPT_EST_EMBEDDED) {   basic->estimator           = TSAdaptEmbedded;}
  else if(basic->mode == TSADAPT_EST_RICHARDSON) { 
    ierr                       = PetscNew(&rich);CHKERRQ(ierr);
    rich->stepno               = 0;
    basic->estimate            = (void*)rich;
    basic->estimator           = TSAdaptRichardson;
    adapt->ops->setup          = TSAdaptSetUp_Basic;
    rich->fp                   = 0;
    rich->lastEnorm            = 0.0;
  }
  ierr = PetscOptionsBool("-ts_adapt_basic_cfl","Double-checking with CFL criteria","",PETSC_FALSE,&flg,NULL);CHKERRQ(ierr);  
  /*if(flg) {
    ierr       = PetscNew(&cfl);CHKERRQ(ierr);
    basic->cfl = cfl;
    ierr = PetscOptionsReal("-ts_adapt_basic_cfl_threshold","CFL threshold","",1.0,&cfl->threshold,NULL);CHKERRQ(ierr);
    cfl->lastReject = -1;
  }*/
  ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSAdaptIsStageComputed_Basic"
static PetscErrorCode  TSAdaptIsStageComputed_Basic(TS ts, PetscBool * success){
  TSAdapt        adapt = ts->adapt;
  TSAdapt_Basic  *basic = (TSAdapt_Basic*)adapt->data;
  TSAdapt_BDF* bdf;

  PetscFunctionBegin;
  if(basic->mode2 == TSADAPT_EST_BDF ) {
    bdf = (TSAdapt_BDF*)basic->double_estimate;
    *success =  (ts->steps != bdf->stepno) \
                && (!ts->inject || (ts->inject && !ts->inject->log_lte));
  } else {
    *success = PETSC_FALSE;
  }
  PetscFunctionReturn(0);
  
}
  
  


#undef __FUNCT__
#define __FUNCT__ "TSAdaptView_Basic"
static PetscErrorCode TSAdaptView_Basic(TSAdapt adapt,PetscViewer viewer)
{
  TSAdapt_Basic  *basic = (TSAdapt_Basic*)adapt->data;
  PetscErrorCode ierr;
  PetscBool      iascii;

  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  if (iascii) {
    if (basic->always_accept) {ierr = PetscViewerASCIIPrintf(viewer,"  Basic: always accepting steps\n");CHKERRQ(ierr);}
    ierr = PetscViewerASCIIPrintf(viewer,"  Basic: clip fastest decrease %g, fastest increase %g\n",(double)basic->clip[0],(double)basic->clip[1]);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer,"  Basic: safety factor %g, extra factor after step rejection %g\n",(double)basic->safety,(double)basic->reject_safety);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer,"  Basic: lastLTE %g\n",(double)basic->enorm);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSAdaptCreate_Basic"
/*MC
   TSADAPTBASIC - Basic adaptive controller for time stepping

   Level: intermediate

.seealso: TS, TSAdapt, TSSetAdapt()
M*/
PETSC_EXTERN PetscErrorCode TSAdaptCreate_Basic(TSAdapt adapt)
{
  PetscErrorCode ierr;
  TSAdapt_Basic  *a;

  PetscFunctionBegin;
  ierr                       = PetscNewLog(adapt,&a);CHKERRQ(ierr);
  adapt->data                = (void*)a;
  adapt->ops->choose         = TSAdaptChoose_Basic;
  adapt->ops->setfromoptions = TSAdaptSetFromOptions_Basic;
  adapt->ops->destroy        = TSAdaptDestroy_Basic;
  adapt->ops->view           = TSAdaptView_Basic;
  adapt->ops->isstagecomputed= TSAdaptIsStageComputed_Basic;
  
  a->mode = TSADAPT_EST_EMBEDDED;
  a->clip[0]       = 0.1;
  a->clip[1]       = 10.;
  a->safety        = 0.9;
  a->reject_safety = 0.5;
  a->always_accept = PETSC_FALSE;
  a->cfl           = NULL;
  PetscLogEventRegister("TSAdapt",PETSC_VIEWER_CLASSID,&ADAPT_EVENT);
  PetscLogEventGetId("TSAdapt",&ADAPT_EVENT);
  PetscLogEventRegister("TSAdaptEmbd",PETSC_VIEWER_CLASSID,&EMBD_EVENT);
  PetscLogEventGetId("TSAdaptEmbd",&EMBD_EVENT);
  PetscLogEventRegister("TSAdaptBDF",PETSC_VIEWER_CLASSID,&BDF_EVENT);
  PetscLogEventGetId("TSAdaptBDF",&BDF_EVENT);
  PetscLogEventRegister("TSAdaptAID",PETSC_VIEWER_CLASSID,&AD_AID_EVENT);
  PetscLogEventGetId("TSAdaptAID",&AD_AID_EVENT);
  PetscFunctionReturn(0);
}
