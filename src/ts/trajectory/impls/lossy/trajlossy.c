
#include <petsc/private/tsimpl.h>        /*I "petscts.h"  I*/

#undef __FUNCT__
#define __FUNCT__ "OutputDAT"
static PetscErrorCode OutputDAT(const char *filename,PetscViewer *viewer)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(*viewer,PETSCVIEWERASCII);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(*viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(*viewer,filename);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "InputDAT"
static PetscErrorCode InputDAT(const char *filename,PetscViewer *viewer)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(*viewer,PETSCVIEWERASCII);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(*viewer,FILE_MODE_READ);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(*viewer,filename);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TSTrajectorySet_Lossy"
static PetscErrorCode TSTrajectorySet_Lossy(TSTrajectory tj,TS ts,PetscInt stepnum,PetscReal time,Vec X)
{
  PetscViewer    viewer;
  PetscInt       ns,i;
  Vec            *Y;
  char           filename[PETSC_MAX_PATH_LEN];
  PetscReal      tprev, *val;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = TSGetTotalSteps(ts,&stepnum);CHKERRQ(ierr);
  if (stepnum == tj->firststep) {
    PetscMPIInt rank;
    ierr = MPI_Comm_rank(PetscObjectComm((PetscObject)ts),&rank);CHKERRQ(ierr);
    if (!rank) {
      ierr = PetscRMTree("SA-data");CHKERRQ(ierr);
      ierr = PetscMkdir("SA-data");CHKERRQ(ierr);
    }
    if(tj->firststep > 0){    tj->counter--;}
  }
  if (++tj->counter > tj->counterMax) {
    tj->counter = 0;
  } else {
    PetscFunctionReturn(0);
  }
  /* save vector */
  ierr = PetscSNPrintf(filename,sizeof(filename),"SA-data/SA-%06d.dat",stepnum);CHKERRQ(ierr);
  ierr = OutputDAT(filename,&viewer);CHKERRQ(ierr);
  //ierr = VecLockPush(X);CHKERRQ(ierr);
  ierr = VecGetLocalSize(X, &ns);CHKERRQ(ierr);
  ierr = VecGetArray(X, &val);CHKERRQ(ierr);
  for(i = 0; i < ns; i++ ) {
    // should we use PetscViewerASCIIPrintf ?
    PetscViewerASCIIPrintf(viewer, "%.15f\n", val[i]);CHKERRQ(ierr);
  }
  //ierr = VecLockPop(X);CHKERRQ(ierr);
  ierr = VecRestoreArray(X, &val);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  
  /* save meta data */
  ierr = PetscSNPrintf(filename,sizeof(filename),"SA-data/SA-%06d.dat.info",stepnum);CHKERRQ(ierr);
  ierr = OutputDAT(filename,&viewer);CHKERRQ(ierr);
  ierr = TSView(ts,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  tj->diskwrites++;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TSTrajectoryGet_Lossy"
static PetscErrorCode TSTrajectoryGet_Lossy(TSTrajectory tj,TS ts,PetscInt stepnum,PetscReal *t)
{
  Vec            Sol;
  PetscInt       i, ns;
  PetscViewer    viewer;
  PetscReal      *val1, *val2;
  char           filename[PETSC_MAX_PATH_LEN];
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscInfo2(ts, "Restart from step %i with step size %g\n", stepnum, *t);CHKERRQ(ierr);
  tj->firststep = stepnum;
  ts->steps = tj->firststep; ts->total_steps = tj->firststep;
  ts->ptime = *t;
  ierr = PetscSNPrintf(filename,sizeof filename,"SA-data/SA-%06d.dat",stepnum);CHKERRQ(ierr);
  ierr = InputDAT(filename,&viewer);CHKERRQ(ierr);
  ierr = TSGetSolution(ts,&Sol);CHKERRQ(ierr);

  if (stepnum != 0) {
    ierr = VecGetLocalSize(Sol, &ns);CHKERRQ(ierr);
    ierr = PetscMalloc1(ns,&val2);CHKERRQ(ierr);
    ierr = PetscViewerASCIIRead(viewer,val2,ns,NULL,PETSC_REAL);CHKERRQ(ierr);
    ierr = VecGetArray(Sol, &val1);CHKERRQ(ierr);
    for(i = 0; i < ns; i++ ) {
      val1[i] = val2[i];
    }
    ierr = VecRestoreArray(Sol, &val1);CHKERRQ(ierr);
    ierr = PetscFree(val2);CHKERRQ(ierr);
  }
  
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*MC
      TSTRAJECTORYLOSSY - Stores each solution of the ODE/ADE in a file

  Level: intermediate

.seealso:  TSTrajectoryCreate(), TS, TSTrajectorySetType()

M*/
#undef __FUNCT__
#define __FUNCT__ "TSTrajectoryCreate_Lossy"
PETSC_EXTERN PetscErrorCode TSTrajectoryCreate_Lossy(TSTrajectory tj,TS ts)
{
  PetscFunctionBegin;
  tj->ops->set  = TSTrajectorySet_Lossy;
  tj->ops->get  = TSTrajectoryGet_Lossy;
  PetscFunctionReturn(0);
}
